/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class MatchesMapModel extends DbConfig {

    private int f24_mid;
    private int o7m_mid;
    private String hn;
    private String gn;
    private String showdate;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT INTO `matches_map` (`f24_mid`, `o7m_mid`, `hn`, `gn`,`showdate`) VALUES (?, ?, ?, ?, ?) "
                + "ON DUPLICATE KEY UPDATE `showdate`=?";
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getF24_mid());
            prestmt.setInt(2, this.getO7m_mid());
            prestmt.setString(3, this.getHn());
            prestmt.setString(4, this.getGn());
            prestmt.setString(5, this.getShowdate());
            prestmt.setString(6, this.getShowdate());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public List<MatchesMapModel> getTodayMatch() {
        List<MatchesMapModel> mmlist = new ArrayList<>();

        String sql = "SELECT mm.* FROM live_match lm\n"
                + "RIGHT JOIN matches_map mm ON mm.f24_mid=lm.mid\n"
                + "WHERE lm.showDate=CURRENT_DATE";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                MatchesMapModel mm = new MatchesMapModel();
                mm.setF24_mid(this.rs.getInt("f24_mid"));
                mm.setO7m_mid(this.rs.getInt("o7m_mid"));
                mm.setHn(this.rs.getString("hn"));
                mm.setGn(this.rs.getString("gn"));
                mm.setShowdate(this.rs.getString("showdate"));
                mmlist.add(mm);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return mmlist;
    }

    public List<MatchesMapModel> getTodayWaitMatch() {
        List<MatchesMapModel> mmlist = new ArrayList<>();

        String sql = "SELECT * FROM matches_map mm\n"
                + "RIGHT JOIN live_match lm ON lm.mid=mm.f24_mid\n"
                + "WHERE mm.showdate=CURRENT_DATE\n"
                + "AND NOW() < DATE_SUB(lm.date,INTERVAL 3 hour)\n"
                + "GROUP BY lm.mid";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                MatchesMapModel mm = new MatchesMapModel();
                mm.setF24_mid(this.rs.getInt("f24_mid"));
                mm.setO7m_mid(this.rs.getInt("o7m_mid"));
                mm.setHn(this.rs.getString("hn"));
                mm.setGn(this.rs.getString("gn"));
                mm.setShowdate(this.rs.getString("showdate"));
                mmlist.add(mm);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return mmlist;
    }

    public List<MatchesMapModel> getTodayLiveMatch() {
        List<MatchesMapModel> mmlist = new ArrayList<>();

        String sql = "SELECT mm.* FROM matches_map mm\n"
                + "RIGHT JOIN live_match lm ON lm.mid=mm.f24_mid\n"
                + "WHERE mm.showdate=CURRENT_DATE\n"
                + "AND NOW() BETWEEN lm.date AND DATE_ADD(lm.date,INTERVAL 3 hour)\n"
                + "GROUP BY lm.mid";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                MatchesMapModel mm = new MatchesMapModel();
                mm.setF24_mid(this.rs.getInt("f24_mid"));
                mm.setO7m_mid(this.rs.getInt("o7m_mid"));
                mm.setHn(this.rs.getString("hn"));
                mm.setGn(this.rs.getString("gn"));
                mm.setShowdate(this.rs.getString("showdate"));
                mmlist.add(mm);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return mmlist;
    }

    public List<MatchesMapModel> getMatchMap(int mid) {
        List<MatchesMapModel> mmlist = new ArrayList<>();

        this.connectDB();
        PreparedStatement prestmt = null;

        try {
            if (mid != 0) {
                String sql = "SELECT * FROM matches_map mm\n"
                        + "WHERE mm.f24_mid = ?";
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, mid);
                this.rs = prestmt.executeQuery();
                while (this.rs.next()) {
                    MatchesMapModel mm = new MatchesMapModel();
                    mm.setF24_mid(this.rs.getInt("f24_mid"));
                    mm.setO7m_mid(this.rs.getInt("o7m_mid"));
                    mm.setHn(this.rs.getString("hn"));
                    mm.setGn(this.rs.getString("gn"));
                    mm.setShowdate(this.rs.getString("showdate"));
                    mmlist.add(mm);
                }
                prestmt.close();
            } else {
                String sql = "SELECT * FROM matches_map mm ORDER BY showdate DESC";                      
                prestmt = this.conn.prepareStatement(sql);        
                this.rs = prestmt.executeQuery();
                while (this.rs.next()) {
                    MatchesMapModel mm = new MatchesMapModel();
                    mm.setF24_mid(this.rs.getInt("f24_mid"));
                    mm.setO7m_mid(this.rs.getInt("o7m_mid"));
                    mm.setHn(this.rs.getString("hn"));
                    mm.setGn(this.rs.getString("gn"));
                    mm.setShowdate(this.rs.getString("showdate"));
                    mmlist.add(mm);
                }
                prestmt.close();
            }
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return mmlist;
    }

    /**
     * @return the f24_mid
     */
    public int getF24_mid() {
        return f24_mid;
    }

    /**
     * @param f24_mid the f24_mid to set
     */
    public void setF24_mid(int f24_mid) {
        this.f24_mid = f24_mid;
    }

    /**
     * @return the o7m_mid
     */
    public int getO7m_mid() {
        return o7m_mid;
    }

    /**
     * @param o7m_mid the o7m_mid to set
     */
    public void setO7m_mid(int o7m_mid) {
        this.o7m_mid = o7m_mid;
    }

    /**
     * @return the hn
     */
    public String getHn() {
        return hn;
    }

    /**
     * @param hn the hn to set
     */
    public void setHn(String hn) {
        this.hn = hn;
    }

    /**
     * @return the gn
     */
    public String getGn() {
        return gn;
    }

    /**
     * @param gn the gn to set
     */
    public void setGn(String gn) {
        this.gn = gn;
    }

    /**
     * @return the showdate
     */
    public String getShowdate() {
        return showdate;
    }

    /**
     * @param showdate the showdate to set
     */
    public void setShowdate(String showdate) {
        this.showdate = showdate;
    }

}
