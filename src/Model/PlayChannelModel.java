
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ngola.bot.controller.DbConfig;
/**
 *
 * @author NanoK
 */
public class PlayChannelModel extends DbConfig{
    private int id;
    private String channel_name;
    private String channel_path;
    private String created_at;
    private String updated_at;
    
    public PlayChannelModel getCheckChannel(String channel_name) {
        PlayChannelModel stattable = null;
        this.connectDB();
        PreparedStatement prestmt = null;
        boolean success = false;
        try {
            String lsql = "select * from channel where channel_name=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setString(1, channel_name);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                stattable = new PlayChannelModel();
                stattable.setId(rs.getInt("id"));
                stattable.setChannelName(rs.getString("channel_name"));
                stattable.setChannelPath(rs.getString("channel_path"));
                stattable.setCreated_at(rs.getString("created_at"));
                stattable.setUpdated_at(rs.getString("updated_at"));
                success = true;
            }
        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return stattable;
    }
    
    public List<PlayChannelModel> getAllChannel(int limit) {
        List<PlayChannelModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM `channel` ORDER BY `channel`.id ASC LIMIT "+limit;

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayChannelModel ll = new PlayChannelModel();
                ll.setId(this.rs.getInt("id"));
                ll.setChannelName(this.rs.getString("channel_name"));
                ll.setChannelPath(this.rs.getString("channel_path"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return lllist;
    }
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `channel` (`channel_name`, `channel_path`, `created_at`,`updated_at`) VALUES (?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getChannelName());
            prestmt.setString(2, this.getChannelPath());
            prestmt.setString(3, this.getCreated_at());
            prestmt.setString(4, this.getUpdated_at());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    
    public String getChannelName() {return channel_name;}
    public void setChannelName(String channel_name) {this.channel_name = channel_name;}
    
    public String getChannelPath() {return channel_path;}
    public void setChannelPath(String channel_path) {this.channel_path = channel_path;}
    
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}
}
