/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import controller.FileManager;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class TeamModel extends DbConfig {

    public boolean getLive7m(FixtureModel fixture, int id7m, String side, String teamname) {
        boolean success = false;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            String lsql = "SELECT lt.teamName,lt.tnPk,t.comPk FROM lang_team lt\n"
                    + "LEFT JOIN team t ON t.tnPk=lt.tnPk\n"
                    + "WHERE \n"
                    + "lt.id7m=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setInt(1, id7m);
            this.rs = prestmt.executeQuery();

            while (this.rs.next()) {
                if (side == "home") {
                    fixture.setHn(rs.getString("teamName"));
                    fixture.setTeamHomeNamePk(rs.getString("tnPk"));
                    fixture.setHcomPk(rs.getString("comPk"));
                } else {
                    fixture.setAn(rs.getString("teamName"));
                    fixture.setTeamAwayNamePk(rs.getString("tnPk"));
                    fixture.setAcomPk(rs.getString("comPk"));
                }
                success = true;
            }

            if (!success) {
                FileManager fileman = new FileManager();
                String dir = "fixture";
                fileman.setPath(dir);
                fileman.setFilename("newteams.csv");
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                String content = fixture.getCompetitionNamePk() + "," + fixture.getLeagueName() + "," + teamname + "," + id7m + "," + dateFormat.format(date) + "\n";
                fileman.append(content);
            }
        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return success;
    }

    public boolean getTeamByid7m(StatTableModel stattable, int id7m) {
        boolean success = false;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            String lsql = "SELECT * FROM lang_team lt\n"
                    + "WHERE \n"
                    + "lt.id7m=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setInt(1, id7m);
            this.rs = prestmt.executeQuery();

            while (this.rs.next()) {
                stattable.setTnpk(rs.getString("tnPk"));
                stattable.setTid(rs.getInt("tid"));
                success = true;
            }

        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return success;
    }

    public List<Integer> getAllTeamId7m() {
        boolean success = false;
        this.connectDB();
        List<Integer> id7mlist = new ArrayList<>();
        PreparedStatement prestmt = null;
        try {
            String lsql = "SELECT * FROM lang_team lt ORDER BY id7m\n";

            prestmt = this.conn.prepareStatement(lsql);
            this.rs = prestmt.executeQuery();

            while (this.rs.next()) {              
                id7mlist.add(rs.getInt("id7m"));
            }

        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return id7mlist;
    }
}
