/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author NanoK
 */
public class PlayTeamModel extends DbConfig{
    
    private int id;
    private int league_id;
    private String name_th;
    private String name_en;
    private String path;
    private String created_at;
    private String updated_at;
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `team` (`league_id`,`name_th`,`name_en`, `created_at`,`updated_at`) VALUES (?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getLeague_id());
            prestmt.setString(2, this.getName_th());
            prestmt.setString(3, this.getName_en());
            prestmt.setString(4, this.getCreated_at());
            prestmt.setString(5, this.getUpdated_at());
           
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public PlayTeamModel getCheckNameTeam(String name_en) {
        PlayTeamModel stattable = null;
        this.connectDB();
        PreparedStatement prestmt = null;
        boolean success = false;
        try {
            String lsql = "select * from team where name_en=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setString(1, name_en);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                stattable = new PlayTeamModel();
                stattable.setId(rs.getInt("id"));
                stattable.setLeague_id(rs.getInt("league_id"));
                stattable.setName_th(rs.getString("name_th"));
                stattable.setName_en(rs.getString("name_en"));
                stattable.setPath(rs.getString("path"));
                stattable.setCreated_at(rs.getString("created_at"));
                stattable.setUpdated_at(rs.getString("updated_at"));
                success = true;
            }

        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return stattable;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getLeague_id() {
        return league_id;
    }

    public void setLeague_id(int league_id) {
        this.league_id = league_id;
    }
    
    public String getName_th() {
        return name_th;
    }

    public void setName_th(String name_th) {
        this.name_th = name_th;
    }
    
    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }
    
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
    
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    
    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
