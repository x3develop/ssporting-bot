/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class FixtureModel extends DbConfig {

    private String date;
    private String teamHomeNamePk;
    private String teamAwayNamePk;
    private String competitionType;
    private String competitionNamePk;
    private String leagueSeason;
    private String subLeagueNamePk;
    private String leagueNamePk;
    private String hn;
    private String an;
    private String hcomPk;
    private String acomPk;
    private String leagueName;
    private String lnk;
    private String teamNamePk;
    private String show_time;

    public boolean save() {
        boolean success = false;
        String sql = "REPLACE INTO `fixture` (`date`, "
                + "`teamHomeNamePk`, `teamAwayNamePk`, `competitionType`, "
                + "`competitionNamePk`, `leagueSeason`, `subLeagueNamePk`, "
                + "`leagueNamePk`, `hn`, `an`, `hcomPk`, `acomPk`, "
                + "`leagueName`, `lnk`, `teamNamePk`,`show_time`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getDate());
            prestmt.setString(2, this.getTeamHomeNamePk());
            prestmt.setString(3, this.getTeamAwayNamePk());
            prestmt.setString(4, this.getCompetitionType());
            prestmt.setString(5, this.getCompetitionNamePk());
            prestmt.setString(6, this.getLeagueSeason());
            prestmt.setString(7, this.getSubLeagueNamePk());
            prestmt.setString(8, this.getLeagueNamePk());
            prestmt.setString(9, this.getHn());
            prestmt.setString(10, this.getAn());
            prestmt.setString(11, this.getHcomPk());
            prestmt.setString(12, this.getAcomPk());
            prestmt.setString(13, this.getLeagueName());
            prestmt.setString(14, this.getLnk());
            prestmt.setString(15, this.getTeamNamePk());
            prestmt.setString(16, this.getShow_time());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the teamHomeNamePk
     */
    public String getTeamHomeNamePk() {
        return teamHomeNamePk;
    }

    /**
     * @param teamHomeNamePk the teamHomeNamePk to set
     */
    public void setTeamHomeNamePk(String teamHomeNamePk) {
        this.teamHomeNamePk = teamHomeNamePk;
    }

    /**
     * @return the teamAwayNamePk
     */
    public String getTeamAwayNamePk() {
        return teamAwayNamePk;
    }

    /**
     * @param teamAwayNamePk the teamAwayNamePk to set
     */
    public void setTeamAwayNamePk(String teamAwayNamePk) {
        this.teamAwayNamePk = teamAwayNamePk;
    }

    /**
     * @return the competitionType
     */
    public String getCompetitionType() {
        return competitionType;
    }

    /**
     * @param competitionType the competitionType to set
     */
    public void setCompetitionType(String competitionType) {
        this.competitionType = competitionType;
    }

    /**
     * @return the competitionNamePk
     */
    public String getCompetitionNamePk() {
        return competitionNamePk;
    }

    /**
     * @param competitionNamePk the competitionNamePk to set
     */
    public void setCompetitionNamePk(String competitionNamePk) {
        this.competitionNamePk = competitionNamePk;
    }

    /**
     * @return the leagueSeason
     */
    public String getLeagueSeason() {
        return leagueSeason;
    }

    /**
     * @param leagueSeason the leagueSeason to set
     */
    public void setLeagueSeason(String leagueSeason) {
        this.leagueSeason = leagueSeason;
    }

    /**
     * @return the subLeagueNamePk
     */
    public String getSubLeagueNamePk() {
        return subLeagueNamePk;
    }

    /**
     * @param subLeagueNamePk the subLeagueNamePk to set
     */
    public void setSubLeagueNamePk(String subLeagueNamePk) {
        this.subLeagueNamePk = subLeagueNamePk;
    }

    /**
     * @return the leagueNamePk
     */
    public String getLeagueNamePk() {
        return leagueNamePk;
    }

    /**
     * @param leagueNamePk the leagueNamePk to set
     */
    public void setLeagueNamePk(String leagueNamePk) {
        this.leagueNamePk = leagueNamePk;
    }

    /**
     * @return the hn
     */
    public String getHn() {
        return hn;
    }

    /**
     * @param hn the hn to set
     */
    public void setHn(String hn) {
        this.hn = hn;
    }

    /**
     * @return the an
     */
    public String getAn() {
        return an;
    }

    /**
     * @param an the an to set
     */
    public void setAn(String an) {
        this.an = an;
    }

    /**
     * @return the hcomPk
     */
    public String getHcomPk() {
        return hcomPk;
    }

    /**
     * @param hcomPk the hcomPk to set
     */
    public void setHcomPk(String hcomPk) {
        this.hcomPk = hcomPk;
    }

    /**
     * @return the acomPk
     */
    public String getAcomPk() {
        return acomPk;
    }

    /**
     * @param acomPk the acomPk to set
     */
    public void setAcomPk(String acomPk) {
        this.acomPk = acomPk;
    }

    /**
     * @return the leagueName
     */
    public String getLeagueName() {
        return leagueName;
    }

    /**
     * @param leagueName the leagueName to set
     */
    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }

    /**
     * @return the lnk
     */
    public String getLnk() {
        return lnk;
    }

    /**
     * @param lnk the lnk to set
     */
    public void setLnk(String lnk) {
        this.lnk = lnk;
    }

    /**
     * @return the teamNamePk
     */
    public String getTeamNamePk() {
        return teamNamePk;
    }

    /**
     * @param teamNamePk the teamNamePk to set
     */
    public void setTeamNamePk(String teamNamePk) {
        this.teamNamePk = teamNamePk;
    }

    /**
     * @return the show_time
     */
    public String getShow_time() {
        return show_time;
    }

    /**
     * @param show_time the show_time to set
     */
    public void setShow_time(String show_time) {
        this.show_time = show_time;
    }

}
