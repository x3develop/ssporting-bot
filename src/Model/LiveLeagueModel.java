/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LiveLeagueModel extends DbConfig {

    private int leagueid;
    private int competitionid;
    private String date;
    private int subleagueid;
    private String subleaguename;
    private int oid;
    private String ln;
    private String lnk;
    private String fg;
    private String bg;
    private String kn;
    private String kkod;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `live_league` "
                + "(`leagueId`,`competitionId`,`date`,`subleagueId`,`subleagueName`,`oid`,`ln`,`lnk`,`fg`,`bg`,`kn`,`kkod`)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getLeagueid());
            prestmt.setInt(2, this.getCompetitionid());
            prestmt.setString(3, this.getDate());
            prestmt.setInt(4, this.getSubleagueid());
            prestmt.setString(5, this.getSubleaguename());
            prestmt.setInt(6, this.getOid());
            prestmt.setString(7, this.getLn());
            prestmt.setString(8, this.getLnk());
            prestmt.setString(9, this.getFg());
            prestmt.setString(10, this.getBg());
            prestmt.setString(11, this.getKn());
            prestmt.setString(12, this.getKkod());
//            System.out.println(prestmt.toString());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<LiveLeagueModel> lllist) {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `live_league` "
                + "(`leagueId`,`competitionId`,`date`,`subleagueId`,`subleagueName`,`oid`,`ln`,`lnk`,`fg`,`bg`,`kn`,`kkod`)"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (LiveLeagueModel league : lllist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, league.getLeagueid());
                prestmt.setInt(2, league.getCompetitionid());
                prestmt.setString(3, league.getDate());
                prestmt.setInt(4, league.getSubleagueid());
                prestmt.setString(5, league.getSubleaguename());
                prestmt.setInt(6, league.getOid());
                prestmt.setString(7, league.getLn());
                prestmt.setString(8, league.getLnk());
                prestmt.setString(9, league.getFg());
                prestmt.setString(10, league.getBg());
                prestmt.setString(11, league.getKn());
                prestmt.setString(12, league.getKkod());
//            System.out.println(prestmt.toString());
                success = prestmt.execute();

//                if (league.getLeagueid() == 43893) {
//                    System.out.println(league.getLeagueid() + "|" + league.getSubleagueid() + "|" + league.getLn());
//                    System.out.println(prestmt.toString());
//                }
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the leagueid
     */
    public int getLeagueid() {
        return leagueid;
    }

    /**
     * @param leagueid the leagueid to set
     */
    public void setLeagueid(int leagueid) {
        this.leagueid = leagueid;
    }

    /**
     * @return the competitionid
     */
    public int getCompetitionid() {
        return competitionid;
    }

    /**
     * @param competitionid the competitionid to set
     */
    public void setCompetitionid(int competitionid) {
        this.competitionid = competitionid;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the subleagueid
     */
    public int getSubleagueid() {
        return subleagueid;
    }

    /**
     * @param subleagueid the subleagueid to set
     */
    public void setSubleagueid(int subleagueid) {
        this.subleagueid = subleagueid;
    }

    /**
     * @return the subleaguename
     */
    public String getSubleaguename() {
        return subleaguename;
    }

    /**
     * @param subleaguename the subleaguename to set
     */
    public void setSubleaguename(String subleaguename) {
        this.subleaguename = subleaguename;
    }

    /**
     * @return the oid
     */
    public int getOid() {
        return oid;
    }

    /**
     * @param oid the oid to set
     */
    public void setOid(int oid) {
        this.oid = oid;
    }

    /**
     * @return the ln
     */
    public String getLn() {
        return ln;
    }

    /**
     * @param ln the ln to set
     */
    public void setLn(String ln) {
        this.ln = ln;
    }

    /**
     * @return the lnk
     */
    public String getLnk() {
        return lnk;
    }

    /**
     * @param lnk the lnk to set
     */
    public void setLnk(String lnk) {
        this.lnk = lnk;
    }

    /**
     * @return the fg
     */
    public String getFg() {
        return fg;
    }

    /**
     * @param fg the fg to set
     */
    public void setFg(String fg) {
        this.fg = fg;
    }

    /**
     * @return the bg
     */
    public String getBg() {
        return bg;
    }

    /**
     * @param bg the bg to set
     */
    public void setBg(String bg) {
        this.bg = bg;
    }

    /**
     * @return the kn
     */
    public String getKn() {
        return kn;
    }

    /**
     * @param kn the kn to set
     */
    public void setKn(String kn) {
        this.kn = kn;
    }

    /**
     * @return the kkod
     */
    public String getKkod() {
        return kkod;
    }

    /**
     * @param kkod the kkod to set
     */
    public void setKkod(String kkod) {
        this.kkod = kkod;
    }

}
