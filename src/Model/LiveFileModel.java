/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LiveFileModel extends DbConfig {

    private String date;
    private String time;
    private String plinkXML;
    private int ml00;
    private int ml01;
    private int ml02;
    private int ml11;
    private int ml10;
    private int ml12;
    private int ma;
    private int zid;

    public boolean save() {
        boolean success = false;
        String sql = "REPLACE INTO `live_file` "
                + "(`date`, `time`, `plinkXML`, `ml00`, `ml01`, `ml02`, `ml10`, `ml11`, `ml12`, `ma`,`zid`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getDate());
            prestmt.setString(2, this.getTime());
            prestmt.setString(3, this.getPlinkXML());
            prestmt.setInt(4, this.getMl00());
            prestmt.setInt(5, this.getMl01());
            prestmt.setInt(6, this.getMl02());
            prestmt.setInt(7, this.getMl10());
            prestmt.setInt(8, this.getMl11());
            prestmt.setInt(9, this.getMl12());
            prestmt.setInt(10, this.getMa());
            prestmt.setInt(11, this.getZid());
//            System.out.println(prestmt.toString());
            success = prestmt.execute();
//            System.out.println(success);
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public LiveFileModel getByCurrentdate() {
        boolean success = false;
        String sql = "SELECT * FROM live_file lf\n"
                + "WHERE lf.date=CURRENT_DATE";

        this.connectDB();
        LiveFileModel lfm = new LiveFileModel();

        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                lfm.setDate(this.rs.getString("date"));
                lfm.setTime(this.rs.getString("time"));
                lfm.setPlinkXML(this.rs.getString("plinkxml"));
                lfm.setMl00(this.rs.getInt("ml00"));
                lfm.setMl01(this.rs.getInt("ml01"));
                lfm.setMl02(this.rs.getInt("ml02"));
                lfm.setMl10(this.rs.getInt("ml10"));
                lfm.setMl11(this.rs.getInt("ml11"));
                lfm.setMl12(this.rs.getInt("ml12"));
                lfm.setMa(this.rs.getInt("ma"));

            }

//            System.out.println(prestmt.toString());
            success = prestmt.execute();
//            System.out.println(success);
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return lfm;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public String getTime() {
        return time;
    }

    /**
     * @param time the time to set
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * @return the plinkXML
     */
    public String getPlinkXML() {
        return plinkXML;
    }

    /**
     * @param plinkXML the plinkXML to set
     */
    public void setPlinkXML(String plinkXML) {
        this.plinkXML = plinkXML;
    }

    /**
     * @return the ml00
     */
    public int getMl00() {
        return ml00;
    }

    /**
     * @param ml00 the ml00 to set
     */
    public void setMl00(int ml00) {
        this.ml00 = ml00;
    }

    /**
     * @return the ml01
     */
    public int getMl01() {
        return ml01;
    }

    /**
     * @param ml01 the ml01 to set
     */
    public void setMl01(int ml01) {
        this.ml01 = ml01;
    }

    /**
     * @return the ml02
     */
    public int getMl02() {
        return ml02;
    }

    /**
     * @param ml02 the ml02 to set
     */
    public void setMl02(int ml02) {
        this.ml02 = ml02;
    }

    /**
     * @return the ml11
     */
    public int getMl11() {
        return ml11;
    }

    /**
     * @param ml11 the ml11 to set
     */
    public void setMl11(int ml11) {
        this.ml11 = ml11;
    }

    /**
     * @return the ml10
     */
    public int getMl10() {
        return ml10;
    }

    /**
     * @param ml10 the ml10 to set
     */
    public void setMl10(int ml10) {
        this.ml10 = ml10;
    }

    /**
     * @return the ml12
     */
    public int getMl12() {
        return ml12;
    }

    /**
     * @param ml12 the ml12 to set
     */
    public void setMl12(int ml12) {
        this.ml12 = ml12;
    }

    /**
     * @return the ma
     */
    public int getMa() {
        return ma;
    }

    /**
     * @param ma the ma to set
     */
    public void setMa(int ma) {
        this.ma = ma;
    }

    /**
     * @return the zid
     */
    public int getZid() {
        return zid;
    }

    /**
     * @param zid the zid to set
     */
    public void setZid(int zid) {
        this.zid = zid;
    }
}
