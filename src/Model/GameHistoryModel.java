/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class GameHistoryModel extends DbConfig {

    private int mid;
    private int history_mid;
    private int lid;
    private int hid;
    private int gid;
    private int home_score;
    private int away_score;
    private String ht_score;
    private String odds;
    private int result;
    private int odds_result;
    private String show_date;
    private String type;

    public GameHistoryModel() {
        this.type = "game";
    }

    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `game_history` (`game_mid`, `history_mid`, `id7m_lid`, `id7m_hid`, `id7m_gid`, `home_score`, `away_score`, `ht_score`, `odds`, `result`, `odds_result`, `show_date`,`type`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setInt(2, this.getHistory_mid());
            prestmt.setInt(3, this.getLid());
            prestmt.setInt(4, this.getHid());
            prestmt.setInt(5, this.getGid());
            prestmt.setInt(6, this.getHome_score());
            prestmt.setInt(7, this.getAway_score());
            prestmt.setString(8, this.getHt_score());
            prestmt.setString(9, this.getOdds());
            prestmt.setInt(10, this.getResult());
            prestmt.setInt(11, this.getOdds_result());
            prestmt.setString(12, this.getShow_date());
            prestmt.setString(13, this.getType());
//            System.out.println(prestmt.toString());
            success = prestmt.execute();
//            System.out.println(success);
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<GameHistoryModel> ghlist) {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `game_history` (`game_mid`, `history_mid`, `id7m_lid`, `id7m_hid`, `id7m_gid`, `home_score`, `away_score`, `ht_score`, `odds`, `result`, `odds_result`, `show_date`,`type`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (GameHistoryModel gh : ghlist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, gh.getMid());
                prestmt.setInt(2, gh.getHistory_mid());
                prestmt.setInt(3, gh.getLid());
                prestmt.setInt(4, gh.getHid());
                prestmt.setInt(5, gh.getGid());
                prestmt.setInt(6, gh.getHome_score());
                prestmt.setInt(7, gh.getAway_score());
                prestmt.setString(8, gh.getHt_score());
                prestmt.setString(9, gh.getOdds());
                prestmt.setInt(10, gh.getResult());
                prestmt.setInt(11, gh.getOdds_result());
                prestmt.setString(12, gh.getShow_date());
                prestmt.setString(13, gh.getType());
//            System.out.println(prestmt.toString());
                success = prestmt.execute();
//            System.out.println(success);
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the history_mid
     */
    public int getHistory_mid() {
        return history_mid;
    }

    /**
     * @param history_mid the history_mid to set
     */
    public void setHistory_mid(int history_mid) {
        this.history_mid = history_mid;
    }

    /**
     * @return the hid
     */
    public int getHid() {
        return hid;
    }

    /**
     * @param hid the hid to set
     */
    public void setHid(int hid) {
        this.hid = hid;
    }

    /**
     * @return the gid
     */
    public int getGid() {
        return gid;
    }

    /**
     * @param gid the gid to set
     */
    public void setGid(int gid) {
        this.gid = gid;
    }

    /**
     * @return the home_score
     */
    public int getHome_score() {
        return home_score;
    }

    /**
     * @param home_score the home_score to set
     */
    public void setHome_score(int home_score) {
        this.home_score = home_score;
    }

    /**
     * @return the away_score
     */
    public int getAway_score() {
        return away_score;
    }

    /**
     * @param away_score the away_score to set
     */
    public void setAway_score(int away_score) {
        this.away_score = away_score;
    }

    /**
     * @return the ht_score
     */
    public String getHt_score() {
        return ht_score;
    }

    /**
     * @param ht_score the ht_score to set
     */
    public void setHt_score(String ht_score) {
        this.ht_score = ht_score;
    }

    /**
     * @return the odds
     */
    public String getOdds() {
        return odds;
    }

    /**
     * @param odds the odds to set
     */
    public void setOdds(String odds) {
        this.odds = odds;
    }

    /**
     * @return the result
     */
    public int getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(int result) {
        this.result = result;
    }

    /**
     * @return the odds_result
     */
    public int getOdds_result() {
        return odds_result;
    }

    /**
     * @param odds_result the odds_result to set
     */
    public void setOdds_result(int odds_result) {
        this.odds_result = odds_result;
    }

    /**
     * @return the show_date
     */
    public String getShow_date() {
        return show_date;
    }

    /**
     * @param show_date the show_date to set
     */
    public void setShow_date(String show_date) {
        this.show_date = show_date;
    }

    /**
     * @return the lid
     */
    public int getLid() {
        return lid;
    }

    /**
     * @param lid the lid to set
     */
    public void setLid(int lid) {
        this.lid = lid;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

}
