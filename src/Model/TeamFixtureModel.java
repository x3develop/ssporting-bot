/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class TeamFixtureModel extends DbConfig {

    private int mid;
    private int lid;
    private int hid;
    private int gid;
    private Long datetime;
    private String side;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `team_fixtures` (`odds7m_mid`, `odds7m_lid`, `odds7m_hid`, `odds7m_gid`, `show_date`, `side`) VALUES (?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setInt(2, this.getLid());
            prestmt.setInt(3, this.getHid());
            prestmt.setInt(4, this.getGid());
            prestmt.setLong(5, this.getDatetime());
            prestmt.setString(6, this.getSide());

            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<TeamFixtureModel> tflist) {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `team_fixtures` (`odds7m_mid`, `odds7m_lid`, `odds7m_hid`, `odds7m_gid`, `show_date`, `side`) VALUES (?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (TeamFixtureModel tf : tflist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, tf.getMid());
                prestmt.setInt(2, tf.getLid());
                prestmt.setInt(3, tf.getHid());
                prestmt.setInt(4, tf.getGid());
                prestmt.setLong(5, tf.getDatetime());
                prestmt.setString(6, tf.getSide());

                success = prestmt.execute();
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the lid
     */
    public int getLid() {
        return lid;
    }

    /**
     * @param lid the lid to set
     */
    public void setLid(int lid) {
        this.lid = lid;
    }

    /**
     * @return the hid
     */
    public int getHid() {
        return hid;
    }

    /**
     * @param hid the hid to set
     */
    public void setHid(int hid) {
        this.hid = hid;
    }

    /**
     * @return the gid
     */
    public int getGid() {
        return gid;
    }

    /**
     * @param gid the gid to set
     */
    public void setGid(int gid) {
        this.gid = gid;
    }

    /**
     * @return the datetime
     */
    public Long getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(Long datetime) {
        this.datetime = datetime;
    }

    /**
     * @return the side
     */
    public String getSide() {
        return side;
    }

    /**
     * @param side the side to set
     */
    public void setSide(String side) {
        this.side = side;
    }

}
