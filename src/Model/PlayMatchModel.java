/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ngola.bot.controller.DbConfig;

/**
 *
 * @author NanoK
 */
public class PlayMatchModel extends DbConfig {
    private int id;
    private int match_7m_id;
    private String groups;
    private int league_id;
    private int team_home;
    private int home_end_time_score;
    private int home_over_time_score;
    private int team_away;
    private int away_end_time_score;
    private int away_over_time_score;
    private String status;
    private String time_match;
    private String created_at;
    private String updated_at;
    
    public int lastMatchByleague(int league_id){
        int match_7m_id = 0;
        String sql = "select * from `match` where `match`.league_id="+league_id+" order by `match`.id DESC limit 1";
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while(rs.next()){
                match_7m_id = rs.getInt("match_7m_id");
            }
            prestmt.close();
        } catch (Exception e) {
//            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return match_7m_id;
    }
    
    public int checkMatchByleague(int match_7m){
        int count = 0;
        String sql = "SELECT count(*) as count  FROM `match` WHERE league_id="+match_7m;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while(rs.next()){
                count = rs.getInt("count");
            }
            prestmt.close();
        } catch (Exception e) {
//            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return count;
    }
    
    public List<PlayMatchModel> getAllFullTimeMatch7mId(int limit) {
        List<PlayMatchModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM `match` left join gamble on gamble.match_id=`match`.id WHERE `match`.`status`='fullTime' and  ISNULL(gamble.handicap) ORDER BY `match`.id DESC LIMIT "+limit;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayMatchModel ll = new PlayMatchModel();
                ll.setId(this.rs.getInt("id"));
                ll.setMatch_7m_id(this.rs.getInt("match_7m_id"));
                ll.setLeague_id(this.rs.getInt("league_id"));
                ll.setTime_match(this.rs.getString("time_match"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return lllist;
    }
    
    public List<PlayMatchModel> getAllMatch7mId(int limit) {
        List<PlayMatchModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM `match` WHERE (`match`.`status`='create' or `match`.`status`='bet') "
                + "ORDER BY CASE "
                + "WHEN `match`.league_id=1 THEN 8 "
                + "WHEN `match`.league_id=590 THEN 7 "
                + "WHEN `match`.league_id=18 THEN 6 "
                + "WHEN `match`.league_id=44 THEN 5 "
                + "WHEN `match`.league_id=27 THEN 4 "
                + "WHEN `match`.league_id=36 THEN 3 "
                + "WHEN `match`.league_id=105 THEN 2 "
                + "WHEN `match`.league_id=608 THEN 1 END DESC, `match`.`status` ASC LIMIT "+limit;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayMatchModel ll = new PlayMatchModel();
                ll.setId(this.rs.getInt("id"));
                ll.setMatch_7m_id(this.rs.getInt("match_7m_id"));
                ll.setLeague_id(this.rs.getInt("league_id"));
                if(this.rs.getString("time_match").contains(".0")){
                    ll.setTime_match(this.rs.getString("time_match").replace(".0", ""));
                }
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return lllist;
    }
    
    public boolean updateTimeMatch() {
        boolean success = false;
        String sql = "UPDATE `match` SET `time_match`=?,`updated_at`=? WHERE `id` = ?";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getTime_match());
            prestmt.setString(2, this.getUpdated_at());
            prestmt.setInt(3, this.getId());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public boolean updateMatch() {
        boolean success = false;
        String sql = "UPDATE `match` SET `status` = ?,`home_end_time_score`=?,`away_end_time_score`=?,`updated_at`=? WHERE `id` = ?";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getStatus());
            prestmt.setInt(2, this.getHome_end_time_score());
            prestmt.setInt(3, this.getAway_end_time_score());
            prestmt.setString(4, this.getUpdated_at());
            prestmt.setInt(5, this.getId());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public boolean saveNewMatch() {
        boolean success = false;
        String sql = "INSERT IGNORE `match` (`match_7m_id`,`groups`, `league_id`, `team_home`, `team_away`, `status`, `time_match`, `created_at`,`updated_at`) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMatch_7m_id());
            prestmt.setString(2, this.getGroups());
            prestmt.setInt(3, this.getLeague_id());
            prestmt.setInt(4, this.getTeam_home());
            prestmt.setInt(5, this.getTeam_away());
            prestmt.setString(6, this.getStatus());
            prestmt.setString(7, this.getTime_match());
            prestmt.setString(8, this.getCreated_at());
            prestmt.setString(9, this.getUpdated_at());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    
    public int getMatch_7m_id() {return match_7m_id;}
    public void setMatch_7m_id(int match_7m_id) {this.match_7m_id = match_7m_id;}
    
    public String getGroups() {return groups;}
    public void setGroups(String groups) {this.groups = groups;}
    
    public int getLeague_id() {return league_id;}
    public void setLeague_id(int league_id) {this.league_id = league_id;}
    
    public int getTeam_home() {return team_home;}
    public void setTeam_home(int team_home) {this.team_home = team_home;}
    
    public int getHome_end_time_score() {return home_end_time_score;}
    public void setHome_end_time_score(int home_end_time_score) {this.home_end_time_score = home_end_time_score;}
    
    public int getHome_over_time_score() {return home_over_time_score;}
    public void setHome_over_time_score(int home_over_time_score) {this.home_over_time_score = home_over_time_score;}
    
    public int getTeam_away() {return team_away;}
    public void setTeam_away(int team_away) {this.team_away = team_away;}
    
    public int getAway_end_time_score() {return away_end_time_score;}
    public void setAway_end_time_score(int away_end_time_score) {this.away_end_time_score = away_end_time_score;}
    
    public int getAway_over_time_score() {return away_over_time_score;}
    public void setAway_over_time_score(int away_over_time_score) {this.away_over_time_score = away_over_time_score;}
    
    public String getStatus() {return status;}
    public void setStatus(String status) {this.status = status;}
    
    public String getTime_match() {return time_match;}
    public void setTime_match(String time_match) {this.time_match = time_match;}
    
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}
}
