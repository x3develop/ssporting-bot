/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class StatTableModel extends DbConfig {

    private String tnpk;
    private int cid;
    private int leagueid;
    private String subleaguenamepk;
    private int no;
    private int gp;
    private int pts;
    private int w;
    private int d;
    private int l;
    private int gf;
    private int ga;
    private int subleagueid;
    private int tid;
    private String group;

    public StatTableModel() {
        group = "none";

    }

    public boolean save() {
        boolean success = false;
        String sql = "REPLACE INTO `stat_table` (`tnPk`, `cid`, `leagueId`, `subLeagueNamePk`, `no`, `gp`, `pts`, `w`, `d`, `l`, `gf`, `ga`, `subLeagueId`, `tid`,`group`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getTnpk());
            prestmt.setInt(2, this.getCid());
            prestmt.setInt(3, this.getLeagueid());
            prestmt.setString(4, this.getSubleaguenamepk());
            prestmt.setInt(5, this.getNo());
            prestmt.setInt(6, this.getGp());
            prestmt.setInt(7, this.getPts());
            prestmt.setInt(8, this.getW());
            prestmt.setInt(9, this.getD());
            prestmt.setInt(10, this.getL());
            prestmt.setInt(11, this.getGf());
            prestmt.setInt(12, this.getGa());
            prestmt.setInt(13, this.getSubleagueid());
            prestmt.setInt(14, this.getTid());
            prestmt.setString(15, this.getGroup());
            success = prestmt.execute();          
            prestmt.close();
        } catch (Exception e) {            
            success = false;
            e.printStackTrace();
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the tnpk
     */
    public String getTnpk() {
        return tnpk;
    }

    /**
     * @param tnpk the tnpk to set
     */
    public void setTnpk(String tnpk) {
        this.tnpk = tnpk;
    }

    /**
     * @return the cid
     */
    public int getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     * @return the leagueid
     */
    public int getLeagueid() {
        return leagueid;
    }

    /**
     * @param leagueid the leagueid to set
     */
    public void setLeagueid(int leagueid) {
        this.leagueid = leagueid;
    }

    /**
     * @return the subleaguenamepk
     */
    public String getSubleaguenamepk() {
        return subleaguenamepk;
    }

    /**
     * @param subleaguenamepk the subleaguenamepk to set
     */
    public void setSubleaguenamepk(String subleaguenamepk) {
        this.subleaguenamepk = subleaguenamepk;
    }

    /**
     * @return the no
     */
    public int getNo() {
        return no;
    }

    /**
     * @param no the no to set
     */
    public void setNo(int no) {
        this.no = no;
    }

    /**
     * @return the gp
     */
    public int getGp() {
        return gp;
    }

    /**
     * @param gp the gp to set
     */
    public void setGp(int gp) {
        this.gp = gp;
    }

    /**
     * @return the pts
     */
    public int getPts() {
        return pts;
    }

    /**
     * @param pts the pts to set
     */
    public void setPts(int pts) {
        this.pts = pts;
    }

    /**
     * @return the w
     */
    public int getW() {
        return w;
    }

    /**
     * @param w the w to set
     */
    public void setW(int w) {
        this.w = w;
    }

    /**
     * @return the d
     */
    public int getD() {
        return d;
    }

    /**
     * @param d the d to set
     */
    public void setD(int d) {
        this.d = d;
    }

    /**
     * @return the l
     */
    public int getL() {
        return l;
    }

    /**
     * @param l the l to set
     */
    public void setL(int l) {
        this.l = l;
    }

    /**
     * @return the gf
     */
    public int getGf() {
        return gf;
    }

    /**
     * @param gf the gf to set
     */
    public void setGf(int gf) {
        this.gf = gf;
    }

    /**
     * @return the ga
     */
    public int getGa() {
        return ga;
    }

    /**
     * @param ga the ga to set
     */
    public void setGa(int ga) {
        this.ga = ga;
    }

    /**
     * @return the subleagueid
     */
    public int getSubleagueid() {
        return subleagueid;
    }

    /**
     * @param subleagueid the subleagueid to set
     */
    public void setSubleagueid(int subleagueid) {
        this.subleagueid = subleagueid;
    }

    /**
     * @return the tid
     */
    public int getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(int tid) {
        this.tid = tid;
    }

    /**
     * @return the group
     */
    public String getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(String group) {
        this.group = group;
    }
}
