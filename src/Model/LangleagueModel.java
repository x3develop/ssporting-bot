/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LangleagueModel extends DbConfig {

    private String name;
    private int cid;
    private int lid;
    private int id7m;
    private String inused;

    public List<LangleagueModel> getAll() {
        List<LangleagueModel> lllist = new ArrayList<>();
        String sql = "SELECT ll.leagueName,ll.competitionId,ll.leagueId,ll.id7m,ll.`new` FROM lang_league ll\n"
                + "ORDER BY ll.id7m";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                LangleagueModel ll = new LangleagueModel();
                ll.setName(this.rs.getString("leagueName"));
                ll.setCid(this.rs.getInt("competitionId"));
                ll.setLid(this.rs.getInt("leagueId"));
                ll.setId7m(this.rs.getInt("id7m"));
                ll.setInused(this.rs.getString("new"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }

        return lllist;
    }

    public boolean usedThisLeague() {
        boolean success = false;
        String sqlupdate = "UPDATE `lang_league` SET `new`='Y' WHERE  `leagueId`=?;";
        this.connectDB();
        PreparedStatement prestmt = null;

        try {
            prestmt = this.conn.prepareStatement(sqlupdate);           
            prestmt.setInt(1, this.getLid());
            prestmt.executeUpdate();
            System.out.println(prestmt.toString());

            prestmt.close();
//            prestmt1.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the cid
     */
    public int getCid() {
        return cid;
    }

    /**
     * @param cid the cid to set
     */
    public void setCid(int cid) {
        this.cid = cid;
    }

    /**
     * @return the lid
     */
    public int getLid() {
        return lid;
    }

    /**
     * @param lid the lid to set
     */
    public void setLid(int lid) {
        this.lid = lid;
    }

    /**
     * @return the id7m
     */
    public int getId7m() {
        return id7m;
    }

    /**
     * @param id7m the id7m to set
     */
    public void setId7m(int id7m) {
        this.id7m = id7m;
    }

    /**
     * @return the inused
     */
    public String getInused() {
        return inused;
    }

    /**
     * @param inused the inused to set
     */
    public void setInused(String inused) {
        this.inused = inused;
    }
}
