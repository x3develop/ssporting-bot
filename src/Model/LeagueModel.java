/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import controller.FileManager;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LeagueModel extends DbConfig {

    public List<int[]> getLive7m() {
        List<int[]> id7mlist = new ArrayList<int[]>();
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            String lsql = "select ll.id7m,ll.leagueId from lang_league ll ";
            prestmt = this.conn.prepareStatement(lsql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                int[] arr = {rs.getInt("id7m"), rs.getInt("leagueId")};
                id7mlist.add(arr);
            }
        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return id7mlist;
    }

    public List<Integer> getAllid7m() {
        List<Integer> id7mlist = new ArrayList<>();
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            String lsql = "SELECT * FROM lang_league ll "
                    + "ORDER BY `id7m`";
            prestmt = this.conn.prepareStatement(lsql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                id7mlist.add(rs.getInt("id7m"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            this.closeDB();
        }
        return id7mlist;
    }

    public FixtureModel getLeagueById7m(int id7m, String lnk) {
        FixtureModel fixture = null;
        this.connectDB();
        PreparedStatement prestmt = null;
        boolean success = false;
        try {

            String lsql = "SELECT \n"
                    + "ll.leagueName,\n"
                    + "ll.leagueNameTh,\n"
                    + "ll.competitionId,\n"
                    + "ll.id7m,\n"
                    + "c.competitionType,\n"
                    + "c.competitionNamePk,\n"
                    + "l.leagueNamePk,\n"
                    + "l.leagueSeason,\n"
                    + "l.leagueId\n"
                    + "\n"
                    + " FROM lang_league ll\n"
                    + "LEFT JOIN  competitions c ON c.competitionId=ll.competitionId\n"
                    + "LEFT JOIN league l ON l.competitionId=c.competitionId AND l.leagueNamePk=c.leagueNamePk\n"
                    + "WHERE ll.id7m=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setInt(1, id7m);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                fixture = new FixtureModel();
                fixture.setLeagueName(rs.getString("leagueName"));
                fixture.setCompetitionType(rs.getString("competitionType"));
                fixture.setCompetitionNamePk(rs.getString("competitionNamePk"));
                fixture.setLeagueNamePk(rs.getString("leagueNamePk"));
                fixture.setLeagueSeason(rs.getString("leagueSeason"));
                fixture.setLnk(rs.getString("leagueName"));
                fixture.setSubLeagueNamePk(rs.getString("leagueNamePk"));
                success = true;
            }
            if (!success) {
                FileManager fileman = new FileManager();
                String dir = "fixture";
                fileman.setPath(dir);
                fileman.setFilename("newleagues.csv");
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
                String content = id7m + "," + lnk + "," + dateFormat.format(date) + "\n";
                fileman.append(content);
            }
        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return fixture;
    }

    public StatTableModel getLeagueById7m(int id7m) {
        StatTableModel stattable = null;
        this.connectDB();
        PreparedStatement prestmt = null;
        boolean success = false;
        try {

            String lsql = "SELECT ll.*,l.leagueNamePk FROM lang_league ll\n"
                    + "RIGHT JOIN league l ON l.leagueId=ll.leagueId\n"
                    + "WHERE \n"
                    + "ll.id7m=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setInt(1, id7m);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                stattable = new StatTableModel();
                stattable.setCid(rs.getInt("competitionId"));
                stattable.setLeagueid(rs.getInt("leagueId"));
                stattable.setSubleaguenamepk(rs.getString("leagueNamePk"));
                stattable.setSubleagueid(rs.getInt("leagueId"));
                success = true;
            }

        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return stattable;
    }

    public boolean hasLeague(int lid) {
        boolean found = false;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            String lsql = "SELECT * FROM league l\n"
                    + "WHERE l.leagueId=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setInt(1, lid);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                found = true;
            }
        } catch (Exception e) {

        } finally {
            this.closeDB();
        }
        return found;
    }
}
