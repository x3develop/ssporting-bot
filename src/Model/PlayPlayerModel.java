/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author NanoK
 */
public class PlayPlayerModel extends DbConfig{
    private int id;
    private String name;
    private String path;
    private String created_at;
    private String updated_at;
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `player` (`name`, `created_at`,`updated_at`) VALUES (?, ?, ?)";
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getName());
//            prestmt.setString(2, this.getPath());
            prestmt.setString(2, this.getCreated_at());
            prestmt.setString(3, this.getUpdated_at());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public PlayPlayerModel getCheckNamePlayer(String name) {
        PlayPlayerModel stattable = null;
        this.connectDB();
        PreparedStatement prestmt = null;
        boolean success = false;
        try {
            String lsql = "select * from `player` where name=?";
            prestmt = this.conn.prepareStatement(lsql);
            prestmt.setString(1, name);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                stattable = new PlayPlayerModel();
                stattable.setId(rs.getInt("id"));
                stattable.setName(rs.getString("name"));
                stattable.setPath(rs.getString("path"));
                stattable.setCreated_at(rs.getString("created_at"));
                stattable.setUpdated_at(rs.getString("updated_at"));
                success = true;
            }
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
        }
        return stattable;
    }
    
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public String getPath() {return path;}
    public void setPath(String path) {this.path = path;}
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}    
}
