/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LineupModel extends DbConfig {

    private int mid;
    private int tid;
    private int pid;
    private String name_th;
    private String name_en;
    private int position;
    private int status;
    private String side;

    public LineupModel() {
        this.name_en = "";
        this.name_th = "";
    }

    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `lineup` (`id7m_mid`, `id7m_tid`, `id7m_pid`, `name_th`, `name_en`, `position`, `status`, `side`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setInt(2, this.getTid());
            prestmt.setInt(3, this.getPid());
            prestmt.setString(4, this.getName_th());
            prestmt.setString(5, this.getName_en());
            prestmt.setInt(6, this.getPosition());
            prestmt.setInt(7, this.getStatus());
            prestmt.setString(8, this.getSide());
//            System.out.println(prestmt.toString());
            success = prestmt.execute();
//            System.out.println(success);
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<LineupModel> linelist) {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `lineup` (`id7m_mid`, `id7m_tid`, `id7m_pid`, `name_th`, `name_en`, `position`, `status`, `side`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (LineupModel line : linelist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, line.getMid());
                prestmt.setInt(2, line.getTid());
                prestmt.setInt(3, line.getPid());
                prestmt.setString(4, line.getName_th());
                prestmt.setString(5, line.getName_en());
                prestmt.setInt(6, line.getPosition());
                prestmt.setInt(7, line.getStatus());
                prestmt.setString(8, line.getSide());
//            System.out.println(prestmt.toString());
                success = prestmt.execute();
//            System.out.println(success);
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the tid
     */
    public int getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(int tid) {
        this.tid = tid;
    }

    /**
     * @return the pid
     */
    public int getPid() {
        return pid;
    }

    /**
     * @param pid the pid to set
     */
    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     * @return the name_th
     */
    public String getName_th() {
        return name_th;
    }

    /**
     * @param name_th the name_th to set
     */
    public void setName_th(String name_th) {
        this.name_th = name_th;
    }

    /**
     * @return the name_en
     */
    public String getName_en() {
        return name_en;
    }

    /**
     * @param name_en the name_en to set
     */
    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    /**
     * @return the position
     */
    public int getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(int position) {
        this.position = position;
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the side
     */
    public String getSide() {
        return side;
    }

    /**
     * @param side the side to set
     */
    public void setSide(String side) {
        this.side = side;
    }

}
