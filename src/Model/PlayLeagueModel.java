/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ngola.bot.controller.DbConfig;

/**
 *
 * @author NanoK
 */
public class PlayLeagueModel extends DbConfig {
    
    private int id;
    private int country_id;
    private String id_7m;
    private String name;
    private String path;
    private String name_7m;
    private String created_at;
    private String updated_at;
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `league` (`country_id`,`id_7m`,`name`, `name_7m`, `created_at`,`updated_at`) VALUES (?, ?, ?, ?, ?, ?)";
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getCountry_id());
            prestmt.setString(2, this.getId_7m());
            prestmt.setString(3, this.getName());
            prestmt.setString(4, this.getName_7m());
            prestmt.setString(5, this.getCreated_at());
            prestmt.setString(6, this.getUpdated_at());
           
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public List<PlayLeagueModel> getAllGroup(int limit) {
        List<PlayLeagueModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM league where `bot`='Y' and `group`='Y' ORDER BY `id` ASC LIMIT "+limit;

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayLeagueModel ll = new PlayLeagueModel();
                ll.setId(this.rs.getInt("id"));
                ll.setId_7m(this.rs.getString("id_7m"));
                ll.setName(this.rs.getString("name"));
                ll.setName_7m(this.rs.getString("name_7m"));
                ll.setCreated_at(this.rs.getString("created_at"));
                ll.setUpdated_at(this.rs.getString("updated_at"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }

        return lllist;
    }
    
    public List<PlayLeagueModel> getAll(int limit) {
        List<PlayLeagueModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM league where `bot`='Y' ORDER BY `id` ASC LIMIT "+limit;

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayLeagueModel ll = new PlayLeagueModel();
                ll.setId(this.rs.getInt("id"));
                ll.setId_7m(this.rs.getString("id_7m"));
                ll.setName(this.rs.getString("name"));
                ll.setName_7m(this.rs.getString("name_7m"));
                ll.setCreated_at(this.rs.getString("created_at"));
                ll.setUpdated_at(this.rs.getString("updated_at"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }

        return lllist;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getCountry_id() {return country_id;}
    public void setCountry_id(int country_id) {this.country_id = country_id;}
    
    public String getId_7m() {
        return id_7m;
    }

    public void setId_7m(String id_7m) {
        this.id_7m = id_7m;
    }
    
    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
    
    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getName_7m() {
        return name_7m;
    }

    public void setName_7m(String name_7m) {
        this.name_7m = name_7m;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
