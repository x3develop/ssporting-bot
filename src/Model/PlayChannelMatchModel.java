/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;
/**
 *
 * @author NanoK
 */
public class PlayChannelMatchModel extends DbConfig{
    private int id;
    private int match_id;
    private int channel_id;
    private String link;
    private String created_at;
    private String updated_at;
    
    public List<PlayChannelMatchModel> getAllChannelMatch(int limit) {
        List<PlayChannelMatchModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM `channel_match` ORDER BY `channel_match`.id ASC LIMIT "+limit;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayChannelMatchModel ll = new PlayChannelMatchModel();
                ll.setId(this.rs.getInt("id"));
                ll.setMatch_id(this.rs.getInt("match_id"));
                ll.setChannel_id(this.rs.getInt("channel_id"));
                ll.setLink(this.rs.getString("link"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return lllist;
    }
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `channel_match` (`match_id`, `channel_id`, `link`, `created_at`,`updated_at`) VALUES (?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMatch_id());
            prestmt.setInt(2, this.getChannel_id());
            prestmt.setString(3, this.getLink());
            prestmt.setString(4, this.getCreated_at());
            prestmt.setString(5, this.getUpdated_at());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    
    public int getMatch_id() {return match_id;}
    public void setMatch_id(int match_id) {this.match_id = match_id;}
    
    public int getChannel_id() {return channel_id;}
    public void setChannel_id(int channel_id) {this.channel_id = channel_id;}
    
    public String getLink() {return link;}
    public void setLink(String link) {this.link = link;}
    
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}
}
