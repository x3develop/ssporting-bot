
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ngola.bot.controller.DbConfig;
/**
 *
 * @author NanoK
 */
public class PlayCountryModel extends DbConfig{
    private int id;
    private String id_7m;
    private String name;
    private String created_at;
    private String updated_at;
    
    public List<PlayCountryModel> getAllCountry(int limit) {
        List<PlayCountryModel> lllist = new ArrayList<>();
        String sql = "SELECT * FROM `country` ORDER BY `country`.id ASC LIMIT "+limit;

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                PlayCountryModel ll = new PlayCountryModel();
                ll.setId(this.rs.getInt("id"));
                ll.setId_7m(this.rs.getString("id_7m"));
                ll.setName(this.rs.getString("name"));
                lllist.add(ll);
            }
            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return lllist;
    }
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `country` (`id_7m`, `name`, `created_at`,`updated_at`) VALUES (?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setString(1, this.getId_7m());
            prestmt.setString(2, this.getName());
            prestmt.setString(3, this.getCreated_at());
            prestmt.setString(4, this.getUpdated_at());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    
    public String getId_7m() {return id_7m;}
    public void setId_7m(String id_7m) {this.id_7m = id_7m;}
    
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}
}
