/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class PlayerModel extends DbConfig {

    private int no;
    private int playerid;
    private int tid;
    private int id7m;
    private int leagueid;
    private String totalscore;
    private String penscoe;
    private String teamname_en;
    private String teamname_th;
    private String playername_en;
    private String playername_th;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT INTO `top_player_score` (`no`, `playerId`, `tid`, `id7m`, `leagueId`, `totalScore`, `penScore`, `teamNameEn`, `teamNameTh`, `playerNameEn`, `playerNameTh`) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getNo());
            prestmt.setInt(2, this.getPlayerid());
            prestmt.setInt(3, this.getTid());
            prestmt.setInt(4, this.getId7m());
            prestmt.setInt(5, this.getLeagueid());
            prestmt.setString(6, this.getTotalscore());
            prestmt.setString(7, this.getPenscoe());
            prestmt.setString(8, this.getTeamname_en());
            prestmt.setString(9, this.getTeamname_th());
            prestmt.setString(10, this.getPlayername_en());
            prestmt.setString(11, this.getPlayername_th());
//            System.out.println(prestmt.toString());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean removeOldTopplayer(int lid) {
        boolean success = false;
        String sql = "delete from top_player_score where leagueId=?";
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, lid);
//            System.out.println(prestmt.toString());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the no
     */
    public int getNo() {
        return no;
    }

    /**
     * @param no the no to set
     */
    public void setNo(int no) {
        this.no = no;
    }

    /**
     * @return the playerid
     */
    public int getPlayerid() {
        return playerid;
    }

    /**
     * @param playerid the playerid to set
     */
    public void setPlayerid(int playerid) {
        this.playerid = playerid;
    }

    /**
     * @return the tid
     */
    public int getTid() {
        return tid;
    }

    /**
     * @param tid the tid to set
     */
    public void setTid(int tid) {
        this.tid = tid;
    }

    /**
     * @return the id7m
     */
    public int getId7m() {
        return id7m;
    }

    /**
     * @param id7m the id7m to set
     */
    public void setId7m(int id7m) {
        this.id7m = id7m;
    }

    /**
     * @return the leagueid
     */
    public int getLeagueid() {
        return leagueid;
    }

    /**
     * @param leagueid the leagueid to set
     */
    public void setLeagueid(int leagueid) {
        this.leagueid = leagueid;
    }

    /**
     * @return the totalscore
     */
    public String getTotalscore() {
        return totalscore;
    }

    /**
     * @param totalscore the totalscore to set
     */
    public void setTotalscore(String totalscore) {
        this.totalscore = totalscore;
    }

    /**
     * @return the penscoe
     */
    public String getPenscoe() {
        return penscoe;
    }

    /**
     * @param penscoe the penscoe to set
     */
    public void setPenscoe(String penscoe) {
        this.penscoe = penscoe;
    }

    /**
     * @return the teamname_en
     */
    public String getTeamname_en() {
        return teamname_en;
    }

    /**
     * @param teamname_en the teamname_en to set
     */
    public void setTeamname_en(String teamname_en) {
        this.teamname_en = teamname_en;
    }

    /**
     * @return the teamname_th
     */
    public String getTeamname_th() {
        return teamname_th;
    }

    /**
     * @param teamname_th the teamname_th to set
     */
    public void setTeamname_th(String teamname_th) {
        this.teamname_th = teamname_th;
    }

    /**
     * @return the playername_en
     */
    public String getPlayername_en() {
        return playername_en;
    }

    /**
     * @param playername_en the playername_en to set
     */
    public void setPlayername_en(String playername_en) {
        this.playername_en = playername_en;
    }

    /**
     * @return the playername_th
     */
    public String getPlayername_th() {
        return playername_th;
    }

    /**
     * @param playername_th the playername_th to set
     */
    public void setPlayername_th(String playername_th) {
        this.playername_th = playername_th;
    }

}
