/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class SubstitutesModel extends DbConfig {

    private int mid;
    private int pid_in;
    private int pid_out;
    private String minute;
    private int side;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `substitutes` (`odds7m_mid`, `pid_in`, `pid_out`, `minute`, `side`) VALUES (?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setInt(2, this.getPid_in());
            prestmt.setInt(3, this.getPid_out());
            prestmt.setString(4, this.getMinute());
            prestmt.setInt(5, this.getSide());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<SubstitutesModel> submlist) {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `substitutes` (`odds7m_mid`, `pid_in`, `pid_out`, `minute`, `side`) VALUES (?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (SubstitutesModel subm : submlist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, subm.getMid());
                prestmt.setInt(2, subm.getPid_in());
                prestmt.setInt(3, subm.getPid_out());
                prestmt.setString(4, subm.getMinute());
                prestmt.setInt(5, subm.getSide());
                success = prestmt.execute();
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean clear(int mid) {
        boolean success = false;
        String sql = "DELETE FROM `substitutes` WHERE `odds7m_mid`=?;";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, mid);
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the pid_in
     */
    public int getPid_in() {
        return pid_in;
    }

    /**
     * @param pid_in the pid_in to set
     */
    public void setPid_in(int pid_in) {
        this.pid_in = pid_in;
    }

    /**
     * @return the minute
     */
    public String getMinute() {
        return minute;
    }

    /**
     * @param minute the minute to set
     */
    public void setMinute(String minute) {
        this.minute = minute;
    }

    /**
     * @return the pid_out
     */
    public int getPid_out() {
        return pid_out;
    }

    /**
     * @param pid_out the pid_out to set
     */
    public void setPid_out(int pid_out) {
        this.pid_out = pid_out;
    }

    /**
     * @return the side
     */
    public int getSide() {
        return side;
    }

    /**
     * @param side the side to set
     */
    public void setSide(int side) {
        this.side = side;
    }
}
