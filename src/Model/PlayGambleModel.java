/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;
/**
 *
 * @author NanoK
 */
public class PlayGambleModel extends DbConfig{
    private int id;
    private int match_id;
    private int home_water_bill;
    private double handicap;
    private int away_water_bill;
    private String created_at;
    private String updated_at;
    
    
    public boolean saveHandicap() {
        boolean success = false;
        String sql = "INSERT IGNORE `gamble` (`match_id`, `handicap`, `created_at`, `updated_at`) VALUES (?, ?, ?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMatchId());
            prestmt.setDouble(2, this.getHandicap());
            prestmt.setString(3, this.getCreated_at());
            prestmt.setString(4, this.getUpdated_at());
           
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public int getMatchId() {return match_id;}
    public void setMatchId(int match_id) {this.match_id = match_id;}
    public int getHomeWaterBill() {return home_water_bill;}
    public void setHomeWaterBill(int home_water_bill) {this.home_water_bill = home_water_bill;}
    public double getHandicap() {return handicap;}
    public void setHandicap(double handicap) {this.handicap = handicap;}
    public int getAwayWaterBill() {return away_water_bill;}
    public void setAwayWaterBill(int away_water_bill) {this.away_water_bill = away_water_bill;}
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}
}
