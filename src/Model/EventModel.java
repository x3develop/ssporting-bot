/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class EventModel extends DbConfig {

    private String id;
    private String zdid;
    private String zdrid;
    private String mid;
    private String zdz;
    private String a;
    private String hn;
    private String gn;
    private String cm;
    private String s;
    private String datetime;

    public boolean saveAll(List<EventModel> eventlist) {
        boolean success = false;
        String sql = "insert into event (zdid,mid,score,cm,message) "
                + "values(?,?,?,?,?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        String message = "";
        try {
            for (EventModel event : eventlist) {
                prestmt = this.conn.prepareStatement(sql);
                message = event.getCm() + " " + event.getHn() + " " + event.getS() + " " + event.getGn();
                prestmt.setInt(1, Integer.parseInt(event.getZdid()));
                prestmt.setInt(2, Integer.parseInt(event.getMid()));
                prestmt.setString(3, event.getS());
                prestmt.setString(4, event.getCm());
                prestmt.setString(5, message);
                success = prestmt.execute();
//                System.out.println(prestmt.toString());
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public int getLastEventID() {
        String sql = "select *from last_update where type='last_event'";

        int id = 0;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);

            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                id = this.rs.getInt("valueint");
            }

            prestmt.close();
        } catch (Exception e) {

        } finally {
            this.closeDB();
            prestmt = null;
        }
        return id;
    }

    public boolean updateLastEventID(int zdid) {
        String sql = "update last_update set `valueint`=? where `type`='last_event'";

        boolean success = false;
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, zdid);
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the zdid
     */
    public String getZdid() {
        return zdid;
    }

    /**
     * @param zdid the zdid to set
     */
    public void setZdid(String zdid) {
        this.zdid = zdid;
    }

    /**
     * @return the zdrid
     */
    public String getZdrid() {
        return zdrid;
    }

    /**
     * @param zdrid the zdrid to set
     */
    public void setZdrid(String zdrid) {
        this.zdrid = zdrid;
    }

    /**
     * @return the mid
     */
    public String getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(String mid) {
        this.mid = mid;
    }

    /**
     * @return the zdz
     */
    public String getZdz() {
        return zdz;
    }

    /**
     * @param zdz the zdz to set
     */
    public void setZdz(String zdz) {
        this.zdz = zdz;
    }

    /**
     * @return the a
     */
    public String getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(String a) {
        this.a = a;
    }

    /**
     * @return the hn
     */
    public String getHn() {
        return hn;
    }

    /**
     * @param hn the hn to set
     */
    public void setHn(String hn) {
        this.hn = hn;
    }

    /**
     * @return the gn
     */
    public String getGn() {
        return gn;
    }

    /**
     * @param gn the gn to set
     */
    public void setGn(String gn) {
        this.gn = gn;
    }

    /**
     * @return the cm
     */
    public String getCm() {
        return cm;
    }

    /**
     * @param cm the cm to set
     */
    public void setCm(String cm) {
        this.cm = cm;
    }

    /**
     * @return the s
     */
    public String getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(String s) {
        this.s = s;
    }

    /**
     * @return the datetime
     */
    public String getDatetime() {
        return datetime;
    }

    /**
     * @param datetime the datetime to set
     */
    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
