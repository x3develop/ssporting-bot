/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class GameInfoModel extends DbConfig {

    private int mid;
    private String homerank;
    private String awayrank;
    private String homelastresult;
    private String awaylastresult;
    private String channel;
    private String weather;
    private String temperature;
    private String homeformation;
    private String awayformation;

    public boolean save() {
        boolean success = false;
        String sql = "REPLACE INTO `game_info` (`odds7m_mid`, `home_rank`, `away_rank`, \n"
                + "`home_last_result`, `away_last_result`, `channel`, \n"
                + "`weather`, `temperature`, `home_formation`, `away_formation`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setString(2, this.getHomerank());
            prestmt.setString(3, this.getAwayrank());
            prestmt.setString(4, this.getHomelastresult());
            prestmt.setString(5, this.getAwaylastresult());
            prestmt.setString(6, this.getChannel());
            prestmt.setString(7, this.getWeather());
            prestmt.setString(8, this.getTemperature());
            prestmt.setString(9, this.getHomeformation());
            prestmt.setString(10, this.getAwayformation());

            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<GameInfoModel> gilist) {
        boolean success = false;
        String sql = "REPLACE INTO `game_info` (`odds7m_mid`, `home_rank`, `away_rank`, \n"
                + "`home_last_result`, `away_last_result`, `channel`, \n"
                + "`weather`, `temperature`, `home_formation`, `away_formation`) \n"
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (GameInfoModel gi : gilist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, gi.getMid());
                prestmt.setString(2, gi.getHomerank());
                prestmt.setString(3, gi.getAwayrank());
                prestmt.setString(4, gi.getHomelastresult());
                prestmt.setString(5, gi.getAwaylastresult());
                prestmt.setString(6, gi.getChannel());
                prestmt.setString(7, gi.getWeather());
                prestmt.setString(8, gi.getTemperature());
                prestmt.setString(9, gi.getHomeformation());
                prestmt.setString(10, gi.getAwayformation());

                success = prestmt.execute();
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the homelastresult
     */
    public String getHomelastresult() {
        return homelastresult;
    }

    /**
     * @param homelastresult the homelastresult to set
     */
    public void setHomelastresult(String homelastresult) {
        this.homelastresult = homelastresult;
    }

    /**
     * @return the awaylastresult
     */
    public String getAwaylastresult() {
        return awaylastresult;
    }

    /**
     * @param awaylastresult the awaylastresult to set
     */
    public void setAwaylastresult(String awaylastresult) {
        this.awaylastresult = awaylastresult;
    }

    /**
     * @return the channel
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @param channel the channel to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return the weather
     */
    public String getWeather() {
        return weather;
    }

    /**
     * @param weather the weather to set
     */
    public void setWeather(String weather) {
        this.weather = weather;
    }

    /**
     * @return the temperature
     */
    public String getTemperature() {
        return temperature;
    }

    /**
     * @param temperature the temperature to set
     */
    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    /**
     * @return the homeformation
     */
    public String getHomeformation() {
        return homeformation;
    }

    /**
     * @param homeformation the homeformation to set
     */
    public void setHomeformation(String homeformation) {
        this.homeformation = homeformation;
    }

    /**
     * @return the awayformation
     */
    public String getAwayformation() {
        return awayformation;
    }

    /**
     * @param awayformation the awayformation to set
     */
    public void setAwayformation(String awayformation) {
        this.awayformation = awayformation;
    }

    /**
     * @return the homerank
     */
    public String getHomerank() {
        return homerank;
    }

    /**
     * @param homerank the homerank to set
     */
    public void setHomerank(String homerank) {
        this.homerank = homerank;
    }

    /**
     * @return the awayrank
     */
    public String getAwayrank() {
        return awayrank;
    }

    /**
     * @param awayrank the awayrank to set
     */
    public void setAwayrank(String awayrank) {
        this.awayrank = awayrank;
    }

}
