/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LiveMatchModel extends DbConfig {

    private int zid;
    private int mid;
    private int x;
    private int lid;
    private int underscorelid;
    private int oid;
    private int sid;
    private int kid;
    private int lnr;
    private int c0;
    private int c1;
    private int c2;
    private int ml;
    private int hid;
    private int gid;
    private String hn;
    private String gn;
    private int hrci;
    private int grci;
    private String grc;
    private String hrc;
    private String s1;
    private String s2;
    private int mstan;
    private String otv;
    private int hstan;
    private int gstan;
    private int goli;
    private int l;
    private int a;
    private int ao;
    private int cx;
    private String info;
    private int rek_h;
    private String date;
    private String showDate;
    private String m;

    public boolean save() {
        boolean success = false;
        String sql = "REPLACE INTO `live_match` "
                + "(`mid`,`lid`,`_lid`,`oid`,`sid`,"
                + "`kid`,`lnr`,`c0`,`c1`,`c2`,"
                + "`ml`,`hid`,`gid`,`hn`,`gn`,"
                + "`hrci`,`grci`,`grc`,`hrc`,`s1`,"
                + "`s2`,`mstan`,`otv`,`hstan`,`gstan`,"
                + "`golI`,`l`,`a`,`ao`,`cx`,"
                + "`info`,`rek_h`,`date`,`showDate`,`hide`,"
                + "`m`,`new`) "
                + "VALUES (?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?)";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setInt(2, this.getLid());
            prestmt.setInt(3, this.getUnderscorelid());
            prestmt.setInt(4, this.getOid());
            prestmt.setInt(5, this.getSid());
            prestmt.setInt(6, this.getKid());
            prestmt.setInt(7, this.getLnr());
            prestmt.setInt(8, this.getC0());
            prestmt.setInt(9, this.getC1());
            prestmt.setInt(10, this.getC2());
            prestmt.setInt(11, this.getMl());
            prestmt.setInt(12, this.getHid());
            prestmt.setInt(13, this.getGid());
            prestmt.setString(14, this.getHn());
            prestmt.setString(15, this.getGn());
            prestmt.setInt(16, this.getHrci());
            prestmt.setInt(17, this.getGrci());
            prestmt.setString(18, this.getHrc());
            prestmt.setString(19, this.getGrc());
            prestmt.setString(20, this.getS1());
            prestmt.setString(21, this.getS2());
            prestmt.setInt(22, this.getMstan());
            prestmt.setString(23, this.getOtv());
            prestmt.setInt(24, this.getHstan());
            prestmt.setInt(25, this.getGstan());
            prestmt.setInt(26, this.getGoli());
            prestmt.setInt(27, this.getL());
            prestmt.setInt(28, this.getA());
            prestmt.setInt(29, this.getAo());
            prestmt.setInt(30, this.getCx());
            prestmt.setString(31, this.getInfo());
            prestmt.setInt(32, this.getRek_h());
            prestmt.setString(33, this.getDate());
            prestmt.setString(34, this.getShowDate());
            prestmt.setString(35, "N");
            prestmt.setString(36, "");
            prestmt.setString(37, "Y");
            System.out.println(prestmt.toString());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<LiveMatchModel> lmlist) {
        boolean success = false;
        String sql = "REPLACE INTO `live_match` "
                + "(`mid`,`lid`,`_lid`,`oid`,`sid`,"
                + "`kid`,`lnr`,`c0`,`c1`,`c2`,"
                + "`ml`,`hid`,`gid`,`hn`,`gn`,"
                + "`hrci`,`grci`,`grc`,`hrc`,`s1`,"
                + "`s2`,`mstan`,`otv`,`hstan`,`gstan`,"
                + "`golI`,`l`,`a`,`ao`,`cx`,"
                + "`info`,`rek_h`,`date`,`showDate`,`hide`,"
                + "`m`,`new`) "
                + "VALUES (?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?, ?, ?, ?, "
                + "?, ?)";
        String sqlupdate = "UPDATE `live_match` SET `new`='N' WHERE  `mid`=? AND `showDate` <> CURRENT_DATE;";
        this.connectDB();
        PreparedStatement prestmt = null;
//        PreparedStatement prestmt1 = null;
        try {
            for (LiveMatchModel lm : lmlist) {
//                System.out.println(lm.getMid());
//                prestmt1 = this.conn.prepareStatement(sqlupdate);
//                prestmt1.setInt(1, lm.getMid());
//                prestmt1.execute();
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, lm.getMid());
                prestmt.setInt(2, lm.getLid());
                prestmt.setInt(3, lm.getUnderscorelid());
                prestmt.setInt(4, lm.getOid());
                prestmt.setInt(5, lm.getSid());
                prestmt.setInt(6, lm.getKid());
                prestmt.setInt(7, lm.getLnr());
                prestmt.setInt(8, lm.getC0());
                prestmt.setInt(9, lm.getC1());
                prestmt.setInt(10, lm.getC2());
                prestmt.setInt(11, lm.getMl());
                prestmt.setInt(12, lm.getHid());
                prestmt.setInt(13, lm.getGid());
                prestmt.setString(14, lm.getHn());
                prestmt.setString(15, lm.getGn());
                prestmt.setInt(16, lm.getHrci());
                prestmt.setInt(17, lm.getGrci());
                prestmt.setString(18, lm.getHrc());
                prestmt.setString(19, lm.getGrc());
                prestmt.setString(20, lm.getS1());
                prestmt.setString(21, lm.getS2());
                prestmt.setInt(22, lm.getMstan());
                prestmt.setString(23, lm.getOtv());
                prestmt.setInt(24, lm.getHstan());
                prestmt.setInt(25, lm.getGstan());
                prestmt.setInt(26, lm.getGoli());
                prestmt.setInt(27, lm.getL());
                prestmt.setInt(28, lm.getA());
                prestmt.setInt(29, lm.getAo());
                prestmt.setInt(30, lm.getCx());
                prestmt.setString(31, lm.getInfo());
                prestmt.setInt(32, lm.getRek_h());
                prestmt.setString(33, lm.getDate());
                prestmt.setString(34, lm.getShowDate());
                prestmt.setString(35, "N");
                prestmt.setString(36, "");
                prestmt.setString(37, "Y");
//                System.out.println(prestmt.toString());
                success = prestmt.execute();
            }
            prestmt.close();
//            prestmt1.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean clearAll(List<LiveMatchModel> lmlist) {
        boolean success = false;
        String sqlupdate = "UPDATE `live_match` SET `new`='N' WHERE  `mid`=? AND `showDate` <> ?;";
        this.connectDB();
        PreparedStatement prestmt = null;

        try {
            for (LiveMatchModel lm : lmlist) {
                prestmt = this.conn.prepareStatement(sqlupdate);
                prestmt.setInt(1, lm.getMid());
                prestmt.setString(2, lm.getShowDate());
                prestmt.executeUpdate();
//
                System.out.println(lm.getMid() + ":" + lm.getHn() + "-" + lm.getGn());
                System.out.println(prestmt.toString());
            }
            prestmt.close();
//            prestmt1.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public LiveMatchModel findMatchById7m(int hid, int gid) {
        LiveMatchModel lmm = null;
        String sql = "SELECT lm.* FROM live_match lm\n"
                + "RIGHT JOIN lang_team ht ON ht.tid = lm.hid\n"
                + "RIGHT JOIN lang_team gt ON gt.tid = lm.gid\n"
                + "WHERE \n"
                + "ht.id7m=?\n"
                + "AND gt.id7m=? "
                + "ORDER BY lm.showDate DESC LIMIT 1";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, hid);
            prestmt.setInt(2, gid);
            this.rs = prestmt.executeQuery();
            while (this.rs.next()) {
                lmm = new LiveMatchModel();
                lmm.setMid(this.rs.getInt("mid"));
                lmm.setHid(this.rs.getInt("hid"));
                lmm.setGid(this.rs.getInt("gid"));
                lmm.setHn(this.rs.getString("hn"));
                lmm.setGn(this.rs.getString("gn"));
                lmm.setSid(this.rs.getInt("sid"));
                lmm.setShowDate(this.rs.getString("showDate"));
            }
            prestmt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.closeDB();
            prestmt = null;
        }

        return lmm;
    }

    /**
     * @return the zid
     */
    public int getZid() {
        return zid;
    }

    /**
     * @param zid the zid to set
     */
    public void setZid(int zid) {
        this.zid = zid;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the x
     */
    public int getX() {
        return x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return the lid
     */
    public int getLid() {
        return lid;
    }

    /**
     * @param lid the lid to set
     */
    public void setLid(int lid) {
        this.lid = lid;
    }

    /**
     * @return the oid
     */
    public int getOid() {
        return oid;
    }

    /**
     * @param oid the oid to set
     */
    public void setOid(int oid) {
        this.oid = oid;
    }

    /**
     * @return the sid
     */
    public int getSid() {
        return sid;
    }

    /**
     * @param sid the sid to set
     */
    public void setSid(int sid) {
        this.sid = sid;
    }

    /**
     * @return the kid
     */
    public int getKid() {
        return kid;
    }

    /**
     * @param kid the kid to set
     */
    public void setKid(int kid) {
        this.kid = kid;
    }

    /**
     * @return the lnr
     */
    public int getLnr() {
        return lnr;
    }

    /**
     * @param lnr the lnr to set
     */
    public void setLnr(int lnr) {
        this.lnr = lnr;
    }

    /**
     * @return the c0
     */
    public int getC0() {
        return c0;
    }

    /**
     * @param c0 the c0 to set
     */
    public void setC0(int c0) {
        this.c0 = c0;
    }

    /**
     * @return the c1
     */
    public int getC1() {
        return c1;
    }

    /**
     * @param c1 the c1 to set
     */
    public void setC1(int c1) {
        this.c1 = c1;
    }

    /**
     * @return the c2
     */
    public int getC2() {
        return c2;
    }

    /**
     * @param c2 the c2 to set
     */
    public void setC2(int c2) {
        this.c2 = c2;
    }

    /**
     * @return the ml
     */
    public int getMl() {
        return ml;
    }

    /**
     * @param ml the ml to set
     */
    public void setMl(int ml) {
        this.ml = ml;
    }

    /**
     * @return the hid
     */
    public int getHid() {
        return hid;
    }

    /**
     * @param hid the hid to set
     */
    public void setHid(int hid) {
        this.hid = hid;
    }

    /**
     * @return the gid
     */
    public int getGid() {
        return gid;
    }

    /**
     * @param gid the gid to set
     */
    public void setGid(int gid) {
        this.gid = gid;
    }

    /**
     * @return the hn
     */
    public String getHn() {
        return hn;
    }

    /**
     * @param hn the hn to set
     */
    public void setHn(String hn) {
        this.hn = hn;
    }

    /**
     * @return the gn
     */
    public String getGn() {
        return gn;
    }

    /**
     * @param gn the gn to set
     */
    public void setGn(String gn) {
        this.gn = gn;
    }

    /**
     * @return the hrci
     */
    public int getHrci() {
        return hrci;
    }

    /**
     * @param hrci the hrci to set
     */
    public void setHrci(int hrci) {
        this.hrci = hrci;
    }

    /**
     * @return the grci
     */
    public int getGrci() {
        return grci;
    }

    /**
     * @param grci the grci to set
     */
    public void setGrci(int grci) {
        this.grci = grci;
    }

    /**
     * @return the grc
     */
    public String getGrc() {
        return grc;
    }

    /**
     * @param grc the grc to set
     */
    public void setGrc(String grc) {
        this.grc = grc;
    }

    /**
     * @return the hrc
     */
    public String getHrc() {
        return hrc;
    }

    /**
     * @param hrc the hrc to set
     */
    public void setHrc(String hrc) {
        this.hrc = hrc;
    }

    /**
     * @return the s1
     */
    public String getS1() {
        return s1;
    }

    /**
     * @param s1 the s1 to set
     */
    public void setS1(String s1) {
        this.s1 = s1;
    }

    /**
     * @return the s2
     */
    public String getS2() {
        return s2;
    }

    /**
     * @param s2 the s2 to set
     */
    public void setS2(String s2) {
        this.s2 = s2;
    }

    /**
     * @return the mstan
     */
    public int getMstan() {
        return mstan;
    }

    /**
     * @param mstan the mstan to set
     */
    public void setMstan(int mstan) {
        this.mstan = mstan;
    }

    /**
     * @return the otv
     */
    public String getOtv() {
        return otv;
    }

    /**
     * @param otv the otv to set
     */
    public void setOtv(String otv) {
        this.otv = otv;
    }

    /**
     * @return the hstan
     */
    public int getHstan() {
        return hstan;
    }

    /**
     * @param hstan the hstan to set
     */
    public void setHstan(int hstan) {
        this.hstan = hstan;
    }

    /**
     * @return the gstan
     */
    public int getGstan() {
        return gstan;
    }

    /**
     * @param gstan the gstan to set
     */
    public void setGstan(int gstan) {
        this.gstan = gstan;
    }

    /**
     * @return the goli
     */
    public int getGoli() {
        return goli;
    }

    /**
     * @param goli the goli to set
     */
    public void setGoli(int goli) {
        this.goli = goli;
    }

    /**
     * @return the l
     */
    public int getL() {
        return l;
    }

    /**
     * @param l the l to set
     */
    public void setL(int l) {
        this.l = l;
    }

    /**
     * @return the a
     */
    public int getA() {
        return a;
    }

    /**
     * @param a the a to set
     */
    public void setA(int a) {
        this.a = a;
    }

    /**
     * @return the ao
     */
    public int getAo() {
        return ao;
    }

    /**
     * @param ao the ao to set
     */
    public void setAo(int ao) {
        this.ao = ao;
    }

    /**
     * @return the cx
     */
    public int getCx() {
        return cx;
    }

    /**
     * @param cx the cx to set
     */
    public void setCx(int cx) {
        this.cx = cx;
    }

    /**
     * @return the info
     */
    public String getInfo() {
        return info;
    }

    /**
     * @param info the info to set
     */
    public void setInfo(String info) {
        this.info = info;
    }

    /**
     * @return the rek_h
     */
    public int getRek_h() {
        return rek_h;
    }

    /**
     * @param rek_h the rek_h to set
     */
    public void setRek_h(int rek_h) {
        this.rek_h = rek_h;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the showDate
     */
    public String getShowDate() {
        return showDate;
    }

    /**
     * @param showDate the showDate to set
     */
    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    /**
     * @return the m
     */
    public String getM() {
        return m;
    }

    /**
     * @param m the m to set
     */
    public void setM(String m) {
        this.m = m;
    }

    /**
     * @return the underscorelid
     */
    public int getUnderscorelid() {
        return underscorelid;
    }

    /**
     * @param underscorelid the underscorelid to set
     */
    public void setUnderscorelid(int underscorelid) {
        this.underscorelid = underscorelid;
    }

}
