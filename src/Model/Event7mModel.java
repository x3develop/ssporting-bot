/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class Event7mModel extends DbConfig {

    private int mid;
    private int pid;
    private int type;
    private String minute;
    private String score;
    private int side;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `event_odds7m` (`odds7m_mid`, `at_minute`, `pid`, `type`, `score`, `side`) VALUES (?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setString(2, this.getMinute());
            prestmt.setInt(3, this.getPid());
            prestmt.setInt(4, this.getType());
            prestmt.setString(5, this.getScore());
            prestmt.setInt(6, this.getSide());

            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<Event7mModel> e7mlist) {
        boolean success = false;
        String sql = "INSERT IGNORE INTO `event_odds7m` (`odds7m_mid`, `at_minute`, `pid`, `type`, `score`, `side`) VALUES (?, ?, ?, ?, ?, ?);";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (Event7mModel e7m : e7mlist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, e7m.getMid());
                prestmt.setString(2, e7m.getMinute());
                prestmt.setInt(3, e7m.getPid());
                prestmt.setInt(4, e7m.getType());
                prestmt.setString(5, e7m.getScore());
                prestmt.setInt(6, e7m.getSide());

                success = prestmt.execute();
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean clear(int mid) {
        boolean success = false;
        String sql = "DELETE FROM `event_odds7m` WHERE `odds7m_mid`=?";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, mid);
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the pid
     */
    public int getPid() {
        return pid;
    }

    /**
     * @param pid the pid to set
     */
    public void setPid(int pid) {
        this.pid = pid;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the minute
     */
    public String getMinute() {
        return minute;
    }

    /**
     * @param minute the minute to set
     */
    public void setMinute(String minute) {
        this.minute = minute;
    }

    /**
     * @return the score
     */
    public String getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(String score) {
        this.score = score;
    }

    /**
     * @return the side
     */
    public int getSide() {
        return side;
    }

    /**
     * @param side the side to set
     */
    public void setSide(int side) {
        this.side = side;
    }

}
