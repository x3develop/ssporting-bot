/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import ssporting.bot.controller.DbConfig;
/**
 *
 * @author NanoK
 */
public class PlayMatchPlayerModel extends DbConfig {
    private int id;
    private int league_id;
    private int match_id;
    private int player_id;
    private int player_id_asset;
    private int minute;
    private int over_minute;
    private String team;
    private String status;
    private String created_at;
    private String updated_at;
    
    public boolean save() {
        boolean success = false;
        String sql = "INSERT IGNORE `match_player` (`league_id`, `match_id`, `player_id`, `player_id_asset`, `minute`, `over_minute`, `team`, `status`, `created_at`,`updated_at`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getLeague_id());
            prestmt.setInt(2, this.getMatch_id());
            prestmt.setInt(3, this.getPlayer_id());
            prestmt.setInt(4, this.getPlayer_id_asset());
            prestmt.setInt(5, this.getMinute());
            prestmt.setInt(6, this.getOver_minute());
            prestmt.setString(7, this.getTeam());
            prestmt.setString(8, this.getStatus());
             prestmt.setString(9, this.getCreated_at());
            prestmt.setString(10, this.getUpdated_at());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }
    
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}
    public int getLeague_id() {return league_id;}
    public void setLeague_id(int league_id) {this.league_id = league_id;}
    public int getMatch_id() {return match_id;}
    public void setMatch_id(int match_id) {this.match_id = match_id;}
    public int getPlayer_id() {return player_id;}
    public void setPlayer_id(int player_id) {this.player_id = player_id;}
    public int getPlayer_id_asset() {return player_id_asset;}
    public void setPlayer_id_asset(int player_id_asset) {this.player_id_asset = player_id_asset;}
    public int getMinute() {return minute;}
    public void setMinute(int minute) {this.minute = minute;}
    public int getOver_minute() {return over_minute;}
    public void setOver_minute(int over_minute) {this.over_minute = over_minute;}
    public String getTeam() {return team;}
    public void setTeam(String team) {this.team = team;}
    public String getStatus() {return status;}
    public void setStatus(String status) {this.status = status;}
    public String getCreated_at() {return created_at;}
    public void setCreated_at(String created_at) {this.created_at = created_at;}
    public String getUpdated_at() {return updated_at;}
    public void setUpdated_at(String updated_at) {this.updated_at = updated_at;}
}
