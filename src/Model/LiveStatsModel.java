/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.PreparedStatement;
import java.util.List;
import ssporting.bot.controller.DbConfig;

/**
 *
 * @author mrsyrop
 */
public class LiveStatsModel extends DbConfig {

    private int mid;
    private int type;
    private String value;

    public boolean save() {
        boolean success = false;
        String sql = "INSERT INTO `live_stats` (`odds7m_mid`, `type`, `value`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `value`=?;";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            prestmt = this.conn.prepareStatement(sql);
            prestmt.setInt(1, this.getMid());
            prestmt.setInt(2, this.getType());
            prestmt.setString(3, this.getValue());
            prestmt.setString(4, this.getValue());
            success = prestmt.execute();
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    public boolean saveAll(List<LiveStatsModel> lsmlist) {
        boolean success = false;
        String sql = "INSERT INTO `live_stats` (`odds7m_mid`, `type`, `value`) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `value`=?;";

        this.connectDB();
        PreparedStatement prestmt = null;
        try {
            for (LiveStatsModel lsm : lsmlist) {
                prestmt = this.conn.prepareStatement(sql);
                prestmt.setInt(1, lsm.getMid());
                prestmt.setInt(2, lsm.getType());
                prestmt.setString(3, lsm.getValue());
                prestmt.setString(4, lsm.getValue());
                success = prestmt.execute();
            }
            prestmt.close();
        } catch (Exception e) {
            success = false;
        } finally {
            this.closeDB();
            prestmt = null;
        }
        return success;
    }

    /**
     * @return the mid
     */
    public int getMid() {
        return mid;
    }

    /**
     * @param mid the mid to set
     */
    public void setMid(int mid) {
        this.mid = mid;
    }

    /**
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

}
