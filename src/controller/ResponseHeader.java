/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package javarunauto;

import java.util.Map;
import java.util.Vector;

/**
 *
 * @author Tingly
 */
public class ResponseHeader {

    private Map headerContent;
    private String content;
    private Vector<String> contentVector;

    public Map getHeaderContent() {
        return headerContent;
    }

    public void setHeaderContent(Map headerContent) {
        this.headerContent = headerContent;
    }
    
    public Vector<String> getContentVector() {
        return contentVector;
    }

    public void setContentVector(Vector<String> contentVector) {
        this.contentVector = contentVector;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
