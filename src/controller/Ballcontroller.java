/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.PlayChannelMatchModel;
import Model.PlayChannelModel;
import Model.PlayCountryModel;
import Model.PlayLeagueModel;
import Model.PlayMatchModel;
import Model.PlayMatchPlayerModel;
import Model.PlayPlayerModel;
import Model.PlayTeamModel;
import Model.PlayGambleModel;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.util.List;

/**
 *
 * @author NanoK
 */
public class Ballcontroller {
    public void getTvMatch(){
            HttpReqController httpPost = new HttpReqController();
            String strContent = httpPost.getHTML("http://tv.7m.com.cn/th/");
            System.out.println("http://tv.7m.com.cn/th/");
            Document doc = Jsoup.parse(strContent);
            Elements tbLists = doc.select("table#tbLists").select("tr");
            Date date = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            for (Element tr : tbLists) {
                if(tr.attr("onmouseover")!=""){
                    Element a=tr.select("a").last();
                    Elements tdLists = tr.select("td").select("table.jm_c").select("td");
                    String str=a.attr("href");
                    str= str.replaceAll("[^-?0-9]+", " "); 
                    String[] strList=str.trim().split(" ");
//                    strList[0].trim(); match_id
//                    System.out.println(strList[0]);
                    for (Element td : tdLists) {
                        if(!td.attr("class").equals("hide")){
                            PlayChannelModel channel=new PlayChannelModel();
                            channel=channel.getCheckChannel(td.text());
                            if(channel==null){
                                channel=new PlayChannelModel();
                                channel.setChannelName(td.text());
                                channel.setChannelPath("");
                                channel.setCreated_at(dateFormat.format(date));
                                channel.setUpdated_at(dateFormat.format(date));
                                channel.save();
                                channel=channel.getCheckChannel(td.text());
                            }
                            
                            PlayChannelMatchModel channelMatch=new PlayChannelMatchModel();
                            channelMatch.setChannel_id(channel.getId());
                            channelMatch.setMatch_id(Integer.parseInt(strList[0]));
                            channelMatch.setLink("");
                            channelMatch.setCreated_at(dateFormat.format(date));
                            channelMatch.setUpdated_at(dateFormat.format(date));
                            boolean status=channelMatch.save();
                            System.out.println("match_id: "+strList[0]+" channel_id: "+channel.getId()+" status: "+status);
                        }
                    }
                }
            }
           
    }
    
    public void getMatchGamble(){
//       HttpReqController httpPost = new HttpReqController();
       PlayMatchModel playMatch =new PlayMatchModel();
       List<PlayMatchModel> lllist = playMatch.getAllFullTimeMatch7mId(100000);
       Date date = new Date();
       SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
       double rq=0,sx=0;
       for (PlayMatchModel ll : lllist) {
//           System.out.println(ll.getMatch_7m_id());
           HttpReqController httpPost = new HttpReqController();
            String strContent = httpPost.getHTML("http://data.7m.com.cn/goaldata/js/"+ll.getMatch_7m_id()+".js");
//            String strContent = httpPost.getHTML("http://data.7m.com.cn/goaldata/js/317741.js");
            System.out.println("http://data.7m.com.cn/goaldata/js/"+ll.getMatch_7m_id()+".js");
            Document doc = Jsoup.parse(strContent);
            Elements score = doc.select("body");
//            System.out.println(score.text().contains("rq"));
            if(score.text().contains("rq")){
                String[] texts = score.text().split(";");
                for (String text : texts) {
//                    System.out.println(text);
                    String[] textIns = text.split("=");
                    if(textIns.length==2){
                        if(textIns[0].contains("rq")){
                           rq=Double.parseDouble(textIns[1].trim());
                        }else if(textIns[0].contains("sx")){
                           sx=Double.parseDouble(textIns[1].trim());
                        }
                    }
                }
//                System.out.println("rq: "+rq+" sx:"+sx);
                if(sx==0 || sx==1){
//                    System.out.println("rq: "+rq+" sx:"+sx);
        
                    if(sx==0){
                       rq=rq*-1; 
                    }
                    
                    PlayGambleModel gamble=new PlayGambleModel();
                    gamble.setHandicap(rq);
                    gamble.setMatchId(ll.getId());
                    gamble.setCreated_at(dateFormat.format(date));
                    gamble.setUpdated_at(dateFormat.format(date));
                    gamble.saveHandicap();
                               
                    System.out.println("BB rq: "+rq+" sx:"+sx);
                }
            }else if(score.select("div").text().equals("")){
                String[] texts = score.text().split(";");
                for (String text : texts) {
                    String[] textIns = text.split("=");
                    if(textIns.length==2){
                        if(textIns[0].contains("rq")){
                           rq=Double.parseDouble(textIns[1].trim());
                        }else if(textIns[0].contains("sx")){
                           sx=Double.parseDouble(textIns[1].trim());
                        }
                    }
                }
                
                if(sx==0 || sx==1){
                    System.out.println("rq: "+rq+" sx:"+sx);
                    
                    if(sx==0){
                       rq=rq*-1; 
                    }
                    
                    PlayGambleModel gamble=new PlayGambleModel();
                    gamble.setHandicap(rq);
                    gamble.setMatchId(ll.getId());
                    gamble.setCreated_at(dateFormat.format(date));
                    gamble.setUpdated_at(dateFormat.format(date));
                    gamble.saveHandicap();
                               
                    System.out.println("rq: "+rq+" sx:"+sx);
                }
            }
       }
    }
    
    public void getHistoryLeague(){
        HttpReqController httpPost = new HttpReqController();
        PlayLeagueModel playLeague =new PlayLeagueModel();
        List<PlayLeagueModel> lllist = playLeague.getAll(1);
        int lastMatchByleague=0;
        String[] yearsAll={"2004-2005","2005-2006","2006-2007","2007-2008","2008-2009","2009-2010","2010-2011","2011-2012","2012-2013","2013-2014","2014-2015","2015-2016","2016-2017"};
        String[] yearsThai={"2008","2009","2010","2011-2012","2012","2013","2014","2015","2016","2017"};
//        String[] years={"2008","2009","2010","2011-2012","2012","2013","2014","2015","2016","2017"};
        String[] years={};
        String strContent="",strContentMatches="";
        Elements divs,score,main;
        Document doc;
        String [][] live_bh_arr = new String[30][0];
        String [][] start_time_arr= new String[30][0];
        String [][] TeamA_arr= new String[30][0];
        String [][] TeamB_arr= new String[30][0];
        for (PlayLeagueModel ll : lllist){ 
             years=yearsAll;
            if(ll.getId()==590){
                years=yearsThai;
            }
            if(ll.getId()==1 || ll.getId()==18 ||  ll.getId()==44 || ll.getId()==27 || ll.getId()==36 || ll.getId()==105 || ll.getId()==590 || ll.getId()==608){
                for (String year : years) {
//                    System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+year);
                    int defaultord=0,columns=0,row=0;
                    String dateTimeMatch,currentRun = null;
                    String[] dateTimeMatchList=null,text_run_Head_Arr=null,text_Tmp_bh_Arr=null,text_Run_Arr=null,text_Time_Arr=null,text_TeamA_Arr=null,text_TeamB_Arr=null;
                    Date date = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                     strContent = httpPost.getHTML("http://data.7m.com.cn/history_Matches_Data/"+year+"/"+ll.getId_7m()+"/en/fixture.js");
                     strContentMatches = httpPost.getHTML("http://data.7m.com.cn/history_Matches_Data/"+year+"/"+ll.getId_7m()+"/en/matches.js");
                     if(strContent=="false" && strContentMatches=="false"){ 
                        strContent = httpPost.getHTML("http://data.7m.com.cn/history_Matches_Data/"+year+"/"+ll.getId_7m()+"/en/index.shtml");
                        System.out.println("http://data.7m.com.cn/history_Matches_Data/"+year+"/"+ll.getId_7m()+"/en/index.shtml");
                        doc = Jsoup.parse(strContent);
                        score = doc.select("body");
                        divs = score.select("td.cou_tm9").select("div").select("script");
                        for(Element elem : divs){
                            if(elem.html().contains("var currentRun")){
//                                System.out.println("IN");
                                String[] texts = elem.html().split(";");
                                for (String text : texts) {
//                                    System.out.println(text);
                                    String[] textIns = text.split("=");
//                                    System.out.println(">>>>>>"+textIns[0]);
                                    if(textIns.length==2){
                                        if(textIns[1].contains("new Array(") && textIns[1].contains(")")){
                                            if(textIns[0].trim().contains("var TeamA_Arr")){
                                                text_TeamA_Arr=textIns[1].trim().substring(10, textIns[1].length()-2).split(",");
                                            }else if(textIns[0].trim().contains("var TeamA_arr")){
                                                text_TeamA_Arr=textIns[1].trim().substring(10, textIns[1].length()-2).split(",");
                                            }else if(textIns[0].trim().contains("var TeamB_Arr")){
                                                text_TeamB_Arr=textIns[1].trim().substring(10, textIns[1].length()-2).split(",");
                                            }else if(textIns[0].trim().contains("var TeamB_arr")){
                                                text_TeamB_Arr=textIns[1].trim().substring(10, textIns[1].length()-2).split(",");
                                            }else if(textIns[0].trim().contains("var Time_Arr")){
                                                text_Time_Arr=textIns[1].trim().substring(10, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].trim().contains("var Start_time_arr")){
                                                text_Time_Arr=textIns[1].trim().substring(10, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].trim().contains("var Tmp_bh_Arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(10, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].trim().contains("var live_bh_arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(10, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].trim().contains("var Run_Arr")){
                                                text_Run_Arr=textIns[1].trim().substring(10, textIns[1].trim().length()-2).split(",");
                                            }else if(textIns[0].trim().contains("var run_Head_Arr")){
                                                text_run_Head_Arr=textIns[1].trim().substring(10, textIns[1].trim().length()-2).split(",");
                                            }
                                        }else if(textIns[1].contains("['") && textIns[1].contains("']")){
                                            if(textIns[0].contains("TeamA_Arr")){
                                                text_TeamA_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                                            }else if(textIns[0].contains("TeamB_Arr")){
                                                text_TeamB_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                                            }else if(textIns[0].contains("Time_Arr")){
                                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                            }else if(textIns[0].contains("Tmp_bh_Arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                            }else if(textIns[0].contains("Run_Arr")){
                                                text_Run_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("run_Head_Arr")){
                                                text_run_Head_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }
                                        }else if(textIns[1].contains("[") && textIns[1].contains("]")){
                                            if(textIns[0].contains("TeamA_Arr")){
                                                text_TeamA_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",\"");
                                            }else if(textIns[0].contains("TeamA_arr")){
                                                text_TeamA_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",\"");
                                            }else if(textIns[0].contains("TeamB_Arr")){
                                                text_TeamB_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",\"");
                                            }else if(textIns[0].contains("TeamB_arr")){
                                                text_TeamB_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",\"");
                                            }else if(textIns[0].contains("Time_Arr")){
                                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",\"");
                                            }else if(textIns[0].contains("var Start_time_arr")){
                                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",\"");
                                            }else if(textIns[0].contains("Tmp_bh_Arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("live_bh_arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("Run_Arr")){
                                                text_Run_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("run_Head_Arr")){
                                                text_run_Head_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }
                                        }else{
                                            if(textIns[0].contains("currentRun")){
                                                currentRun=textIns[1].trim();
    //                                            System.out.println(currentRun);
                                            }
                                        }
                                    }
                                }
                            }
                        }
//                        System.out.println(text_Time_Arr[0]+"::"+text_Time_Arr[(text_Time_Arr.length-1)]);
//                        System.out.println(text_Tmp_bh_Arr[0]+"::"+text_Tmp_bh_Arr[(text_Tmp_bh_Arr.length-1)]);
//                        System.out.println(text_TeamA_Arr[0]+"::"+text_TeamA_Arr[(text_TeamA_Arr.length-1)]);
//                        System.out.println(text_TeamB_Arr[0]+"::"+text_TeamB_Arr[(text_TeamB_Arr.length-1)]);
                     }else{
                        if(strContentMatches=="false"){
                            System.out.println("http://data.7m.com.cn/history_Matches_Data/"+year+"/"+ll.getId_7m()+"/en/fixture.js");
                            doc = Jsoup.parse(strContent);
                            score = doc.select("body");
                            main=score.select("div");
                            if(main.text().equals("")){
                                String[] texts = score.text().split(";");
                                for (String text : texts) {
                                    String[] textIns = text.split("=");
                                    if(textIns.length==2){
                                        if(textIns[1].contains("['") && textIns[1].contains("']")){
                                            if(textIns[0].contains("TeamA_Arr")){
                                                text_TeamA_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                                            }else if(textIns[0].contains("TeamB_Arr")){
                                                text_TeamB_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                                            }else if(textIns[0].contains("Time_Arr")){
                                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                            }else if(textIns[0].contains("Tmp_bh_Arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                            }else if(textIns[0].contains("Run_Arr")){
                                                text_Run_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("run_Head_Arr")){
                                                text_run_Head_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }
                                        }else if(textIns[1].contains("[") && textIns[1].contains("]")){
                                            if(textIns[0].contains("TeamA_Arr")){
                                                text_TeamA_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("TeamB_Arr")){
                                                text_TeamB_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("Time_Arr")){
                                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",");
                                            }else if(textIns[0].contains("Tmp_bh_Arr")){
                                                text_Tmp_bh_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("Run_Arr")){
                                                text_Run_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }else if(textIns[0].contains("run_Head_Arr")){
                                                text_run_Head_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                            }
                                        }else{
                                            if(textIns[0].contains("currentRun")){
                                                currentRun=textIns[1].trim();
    //                                            System.out.println(currentRun);
                                            }
                                        }
                                    }
                                }
//                            System.out.println(text_Time_Arr[0]+"::"+text_Time_Arr[(text_Time_Arr.length-1)]);
//                            System.out.println(text_Tmp_bh_Arr[0]+"::"+text_Tmp_bh_Arr[(text_Tmp_bh_Arr.length-1)]);
//                            System.out.println(text_TeamA_Arr[0]+"::"+text_TeamA_Arr[(text_TeamA_Arr.length-1)]);
//                            System.out.println(text_TeamB_Arr[0]+"::"+text_TeamB_Arr[(text_TeamB_Arr.length-1)]);
                            }
                        }else{
                            live_bh_arr = new String[30][0];
                            start_time_arr= new String[30][0];
                            TeamA_arr= new String[30][0];
                            TeamB_arr= new String[30][0];
                            String str;
                            String[] number;
                            System.out.println("http://data.7m.com.cn/history_Matches_Data/"+year+"/"+ll.getId_7m()+"/en/matches.js");
                            doc = Jsoup.parse(strContentMatches);
                            score = doc.select("body");
                            if(score.select("div").text().equals("")){
                                 String[] texts = score.text().split(";");
                                    for (String text : texts) {
//                                        System.out.println(text);
                                        String[] textIns = text.split("=");
                                        if(textIns.length==2){
                                            if(textIns[1].contains("['") && textIns[1].contains("']")){

                                            }else if(textIns[1].contains("[") && textIns[1].contains("]")){
            //                                    System.out.println("::"+textIns[0]);
                                                if(textIns[0].contains("live_bh_arr")){
                                                    str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                                    number=str.trim().split(" ");
                                                    if(number.length==1 && !number[0].trim().isEmpty()){
                                                        columns=Integer.parseInt(number[0]);
                                                        live_bh_arr[columns]=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                                    }
                                                }else if(textIns[0].contains("Start_time_arr")){
                                                    str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                                    number=str.trim().split(" ");
                                                    if(number.length==1 && !number[0].trim().isEmpty()){
            //                                            System.out.println(number[0]);
                                                        columns=Integer.parseInt(number[0]);
                                                        start_time_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
                    //                                    System.out.println(start_time_arr[columns][0]+"::"+start_time_arr[columns][5]);
                                                    }
                                                }else if(textIns[0].contains("TeamA_arr")){
                                                    str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                                    number=str.trim().split(" ");
                                                    if(number.length==1 && !number[0].trim().isEmpty()){
                                                        columns=Integer.parseInt(number[0]);
                                                        TeamA_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
                    //                                    System.out.println(TeamA_arr[columns][0]+":A:"+TeamA_arr[columns][5]);
                                                    }
                                                }else if(textIns[0].contains("TeamB_arr")){
                                                    str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                                    number=str.trim().split(" ");
                                                    if(number.length==1 && !number[0].trim().isEmpty()){
                                                        columns=Integer.parseInt(number[0]);
                                                        TeamB_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
                    //                                    System.out.println(TeamB_arr[columns][0]+":B:"+TeamB_arr[columns][5]);
                                                    }
                                                }
                                            }else{
                                                if(textIns[0].contains("defaultord")){
                                                    defaultord= Integer.parseInt(textIns[1].trim());
                                                }
                                            }
                                        }
                                    }
                            }
                        }
                    }
                    if(defaultord>0){
                         for(int m = 0; m <= defaultord; m++) {
                            if(live_bh_arr[m].length>0 && start_time_arr[m].length>0 && TeamA_arr[m].length>0 && TeamB_arr[m].length>0){
                                for(int i = 0; i < live_bh_arr[m].length; i++) {
                                    if((Integer.parseInt(live_bh_arr[m][i].trim())>lastMatchByleague)){
                                        PlayTeamModel teamHome=new PlayTeamModel();
                                        PlayTeamModel teamAway=new PlayTeamModel();

                                        System.out.println(TeamA_arr[m][i].trim().replace("\"", "")+"::"+TeamB_arr[m][i].trim().replace("\"", ""));

                                        teamHome=teamHome.getCheckNameTeam(TeamA_arr[m][i].trim().replace("\"", ""));
                                        teamAway=teamAway.getCheckNameTeam(TeamB_arr[m][i].trim().replace("\"", ""));

                                        start_time_arr[m][i] = start_time_arr[m][i].replace("\"", "");
                                        List<String> elephantList = Arrays.asList(start_time_arr[m][i].split(","));
                                        dateTimeMatch=elephantList.get(0)+"-"+elephantList.get(1)+"-"+elephantList.get(2)+" "+elephantList.get(3)+":"+elephantList.get(4)+":"+elephantList.get(5);

    //                                    System.out.println(dateTimeMatch+"::"+live_bh_arr[m][i].trim()+":"+TeamA_arr[m][i].trim()+":"+TeamB_arr[m][i].trim());
    //                                    System.out.println(teamHome.getId()+"::"+teamAway.getId());
                                        if(teamHome!=null && teamAway!=null){
    //                                         System.out.println("True");
                                                PlayMatchModel Match =new PlayMatchModel();
                                                    Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[m][i].trim()));
                                                    Match.setLeague_id(ll.getId());
                                                    Match.setTeam_home(teamHome.getId());
                                                    Match.setTeam_away(teamAway.getId());
                                                    Match.setStatus("create");
                                                    Match.setTime_match(dateTimeMatch);
                                                    Match.setCreated_at(dateFormat.format(date));
                                                    Match.setUpdated_at(dateFormat.format(date));
                                                    System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+teamAway.getId()+" "+live_bh_arr[m][i]+" "+Match.saveNewMatch());    
                                            }else if(teamHome!=null){
    //                                            System.out.println("H True");
                                                    PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                        AddTeamAway.setLeague_id(ll.getId());
                                                        AddTeamAway.setName_en(TeamB_arr[m][i].trim().replace("\"", ""));
                                                        AddTeamAway.setName_th("");
                                                        AddTeamAway.setCreated_at(dateFormat.format(date));
                                                        AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                        AddTeamAway.save();

                                                    AddTeamAway=AddTeamAway.getCheckNameTeam(TeamB_arr[m][i].trim().replace("\"", ""));

                                                    PlayMatchModel Match =new PlayMatchModel();
                                                    Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[m][i].trim()));
                                                    Match.setLeague_id(ll.getId());
                                                    Match.setTeam_home(teamHome.getId());
                                                    Match.setTeam_away(AddTeamAway.getId());
                                                    Match.setStatus("create");
                                                    Match.setTime_match(dateTimeMatch);
                                                    Match.setCreated_at(dateFormat.format(date));
                                                    Match.setUpdated_at(dateFormat.format(date));
                                                    System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+AddTeamAway.getId()+" "+live_bh_arr[m][i]+" "+Match.saveNewMatch());
                                            }else if(teamAway!=null){
    //                                            System.out.println("A True");
                                                    PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                        AddTeamHome.setLeague_id(ll.getId());
                                                        AddTeamHome.setName_en(TeamA_arr[m][i].trim().replace("\"", ""));
                                                        AddTeamHome.setName_th("");
                                                        AddTeamHome.setCreated_at(dateFormat.format(date));
                                                        AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                        AddTeamHome.save();

                                                    AddTeamHome=AddTeamHome.getCheckNameTeam(TeamA_arr[m][i].trim().replace("\"", ""));

                                                    PlayMatchModel Match =new PlayMatchModel();
                                                    Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[m][i].trim()));
                                                    Match.setLeague_id(ll.getId());
                                                    Match.setTeam_home(AddTeamHome.getId());
                                                    Match.setTeam_away(teamAway.getId());
                                                    Match.setStatus("create");
                                                    Match.setTime_match(dateTimeMatch);
                                                    Match.setCreated_at(dateFormat.format(date));
                                                    Match.setUpdated_at(dateFormat.format(date));
                                                    System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+teamAway.getId()+" "+live_bh_arr[m][i]+" "+Match.saveNewMatch());
                                            }else{
    //                                            System.out.println("false");
                                                    PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                        AddTeamHome.setLeague_id(ll.getId());
                                                        AddTeamHome.setName_en(TeamA_arr[m][i].trim().replace("\"", ""));
                                                        AddTeamHome.setName_th("");
                                                        AddTeamHome.setCreated_at(dateFormat.format(date));
                                                        AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                        AddTeamHome.save();

                                                    AddTeamHome=AddTeamHome.getCheckNameTeam(TeamA_arr[m][i].trim().replace("\"", ""));

                                                    PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                        AddTeamAway.setLeague_id(ll.getId());
                                                        AddTeamAway.setName_en(TeamB_arr[m][i].trim().replace("\"", ""));
                                                        AddTeamAway.setName_th("");
                                                        AddTeamAway.setCreated_at(dateFormat.format(date));
                                                        AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                        AddTeamAway.save();
                                                    AddTeamAway=AddTeamAway.getCheckNameTeam(TeamB_arr[m][i].trim().replace("\"", ""));

                                                    PlayMatchModel Match =new PlayMatchModel();
                                                    Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[m][i].trim()));
                                                    Match.setLeague_id(ll.getId());
                                                    Match.setTeam_home(AddTeamHome.getId());
                                                    Match.setTeam_away(AddTeamAway.getId());
                                                    Match.setStatus("create");
                                                    Match.setTime_match(dateTimeMatch);
                                                    Match.setCreated_at(dateFormat.format(date));
                                                    Match.setUpdated_at(dateFormat.format(date));
                                                    System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+AddTeamAway.getId()+" "+live_bh_arr[m][i]+" "+Match.saveNewMatch());
                                            } 
                                        }
                                }
                            }
                        }
                    }else if(text_Tmp_bh_Arr!=null){
                        for(int i = 0; i < text_Tmp_bh_Arr.length; i++) {
                            if(text_Time_Arr[i].contains(",")){
                                            List<String> elephantList = Arrays.asList(text_Time_Arr[i].split(","));
                                            dateTimeMatch=elephantList.get(0)+"-"+elephantList.get(1)+"-"+elephantList.get(2)+" "+elephantList.get(3)+":"+elephantList.get(4)+":"+elephantList.get(5);
                                        }else{
                                            dateTimeMatch=text_Time_Arr[i];
                                        }
                                        PlayTeamModel teamHome=new PlayTeamModel();
                                        PlayTeamModel teamAway=new PlayTeamModel();
                                        teamHome=teamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));
                                        teamAway=teamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));
                                        dateTimeMatch = dateTimeMatch.replace("\"", "");
                                        System.out.println(dateTimeMatch+"::"+text_Tmp_bh_Arr[i]+":"+text_TeamA_Arr[i]+":"+text_TeamB_Arr[i]);
                                                if(teamHome!=null && teamAway!=null){
                                                        PlayMatchModel Match =new PlayMatchModel();
                                                        Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                                        Match.setLeague_id(ll.getId());
                                                        Match.setTeam_home(teamHome.getId());
                                                        Match.setTeam_away(teamAway.getId());
                                                        Match.setStatus("create");
                                                        Match.setTime_match(dateTimeMatch);
                                                        Match.setCreated_at(dateFormat.format(date));
                                                        Match.setUpdated_at(dateFormat.format(date));
                                                        System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+teamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());    
                                                }else if(teamHome!=null){
                                                        PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                            AddTeamAway.setLeague_id(ll.getId());
                                                            AddTeamAway.setName_en(text_TeamB_Arr[i].trim().replace("\"", ""));
                                                            AddTeamAway.setName_th("");
                                                            AddTeamAway.setCreated_at(dateFormat.format(date));
                                                            AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                            AddTeamAway.save();
                                                            AddTeamAway=AddTeamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));
                                                        PlayMatchModel Match =new PlayMatchModel();
                                                        Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                                        Match.setLeague_id(ll.getId());
                                                        Match.setTeam_home(teamHome.getId());
                                                        Match.setTeam_away(AddTeamAway.getId());
                                                        Match.setStatus("create");
                                                        Match.setTime_match(dateTimeMatch);
                                                        Match.setCreated_at(dateFormat.format(date));
                                                        Match.setUpdated_at(dateFormat.format(date));
                                                        System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+AddTeamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                                                }else if(teamAway!=null){
                                                        PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                            AddTeamHome.setLeague_id(ll.getId());
                                                            AddTeamHome.setName_en(text_TeamA_Arr[i].trim().replace("\"", ""));
                                                            AddTeamHome.setName_th("");
                                                            AddTeamHome.setCreated_at(dateFormat.format(date));
                                                            AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                            AddTeamHome.save();
                                                        AddTeamHome=AddTeamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));
                                                        PlayMatchModel Match =new PlayMatchModel();
                                                        Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                                        Match.setLeague_id(ll.getId());
                                                        Match.setTeam_home(AddTeamHome.getId());
                                                        Match.setTeam_away(teamAway.getId());
                                                        Match.setStatus("create");
                                                        Match.setTime_match(dateTimeMatch);
                                                        Match.setCreated_at(dateFormat.format(date));
                                                        Match.setUpdated_at(dateFormat.format(date));
                                                        System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+teamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                                                }else{
                                                        PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                            AddTeamHome.setLeague_id(ll.getId());
                                                            AddTeamHome.setName_en(text_TeamA_Arr[i].trim().replace("\"", ""));
                                                            AddTeamHome.setName_th("");
                                                            AddTeamHome.setCreated_at(dateFormat.format(date));
                                                            AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                            AddTeamHome.save();

                                                        AddTeamHome=AddTeamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));

                                                        PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                            AddTeamAway.setLeague_id(ll.getId());
                                                            AddTeamAway.setName_en(text_TeamB_Arr[i].trim().replace("\"", ""));
                                                            AddTeamAway.setName_th("");
                                                            AddTeamAway.setCreated_at(dateFormat.format(date));
                                                            AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                            AddTeamAway.save();
                                                        AddTeamAway=AddTeamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));

                                                        PlayMatchModel Match =new PlayMatchModel();
                                                        Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                                        Match.setLeague_id(ll.getId());
                                                        Match.setTeam_home(AddTeamHome.getId());
                                                        Match.setTeam_away(AddTeamAway.getId());
                                                        Match.setStatus("create");
                                                        Match.setTime_match(dateTimeMatch);
                                                        Match.setCreated_at(dateFormat.format(date));
                                                        Match.setUpdated_at(dateFormat.format(date));
                                                        System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+AddTeamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                            }                
                        }                  
                    }            
                }     
            }
        }
    }
    
    public void getUEFAChampionsLeague(){
        System.out.println("getUEFAChampionsLeague");
        PlayMatchModel playMatch =new PlayMatchModel();
        PlayLeagueModel playLeague =new PlayLeagueModel();
        List<PlayLeagueModel> lllist = playLeague.getAllGroup(1000);
        int lastMatchByleague=0;
        for (PlayLeagueModel ll : lllist){ 
//            if(ll.getId()==645){
                lastMatchByleague=playMatch.lastMatchByleague(ll.getId());
                System.out.println(lastMatchByleague);
                HttpReqController httpPost = new HttpReqController();
                System.out.println("http://data.7m.com.cn/matches_data/"+ll.getId_7m()+"/en/matches.js");
                String strContent = httpPost.getHTML("http://data.7m.com.cn/matches_data/"+ll.getId_7m()+"/en/matches.js");
                Document doc = Jsoup.parse(strContent);
                Elements score = doc.select("body");
                int defaultord=0,columns=0,row=0;
                String [][] live_bh_arr = new String[30][0];
                String [][] start_time_arr= new String[30][0];
                String [][] TeamA_arr= new String[30][0];
                String [][] TeamB_arr= new String[30][0];
                String [][] groups_arr= new String[30][0];
                String str,dateTimeMatch;
                String[] number;
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                if(score.select("div").text().equals("")){
                     String[] texts = score.text().split(";");
                        for (String text : texts) {
                            String[] textIns = text.split("=");
                            if(textIns.length==2){
                                if(textIns[1].contains("['") && textIns[1].contains("']")){
                                    if(textIns[0].contains("groups_arr")){
                                        System.out.println("IN :: groups_arr");
                                    }
                                }else if(textIns[1].contains("[") && textIns[1].contains("]")){
//                                    System.out.println("::"+textIns[0]);
                                    if(textIns[0].contains("live_bh_arr")){
                                        str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                        number=str.trim().split(" ");
                                        if(number.length==1 && !number[0].trim().isEmpty()){
                                            columns=Integer.parseInt(number[0]);
                                            live_bh_arr[columns]=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                        }
                                    }else if(textIns[0].contains("Start_time_arr")){
                                        str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                        number=str.trim().split(" ");
                                        if(number.length==1 && !number[0].trim().isEmpty()){
//                                            System.out.println(number[0]);
                                            columns=Integer.parseInt(number[0]);
                                            start_time_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
        //                                    System.out.println(start_time_arr[columns][0]+"::"+start_time_arr[columns][5]);
                                        }
                                    }else if(textIns[0].contains("TeamA_arr")){
                                        str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                        number=str.trim().split(" ");
                                        if(number.length==1 && !number[0].trim().isEmpty()){
                                            columns=Integer.parseInt(number[0]);
                                            TeamA_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
        //                                    System.out.println(TeamA_arr[columns][0]+":A:"+TeamA_arr[columns][5]);
                                        }
                                    }else if(textIns[0].contains("TeamB_arr")){
                                        str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                        number=str.trim().split(" ");
                                        if(number.length==1 && !number[0].trim().isEmpty()){
                                            columns=Integer.parseInt(number[0]);
                                            TeamB_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
        //                                    System.out.println(TeamB_arr[columns][0]+":B:"+TeamB_arr[columns][5]);
                                        }
                                    }else if(textIns[0].contains("groups_arr")){
                                        str=textIns[0].replaceAll("[^-?0-9]+", " ");
                                        number=str.trim().split(" ");
                                        if(number.length==1 && !number[0].trim().isEmpty()){
                                            columns=Integer.parseInt(number[0]);
                                            groups_arr[columns]=textIns[1].trim().substring(3, textIns[1].trim().length()-2).split("','");
                                        }
                                    }
                                }else{
                                    if(textIns[0].contains("defaultord")){
                                        defaultord= Integer.parseInt(textIns[1].trim());
                                    }
                                }
                            }
                        }
//                        System.out.println(groups_arr[0][0]);
                        if(live_bh_arr[defaultord].length>0 && start_time_arr[defaultord].length>0 && TeamA_arr[defaultord].length>0 && TeamB_arr[defaultord].length>0){
                            for(int i = 0; i < live_bh_arr[defaultord].length; i++) {
                                if((Integer.parseInt(live_bh_arr[defaultord][i].trim())>lastMatchByleague)){
                                    PlayTeamModel teamHome=new PlayTeamModel();
                                    PlayTeamModel teamAway=new PlayTeamModel();
                                    
                                    System.out.println(TeamA_arr[defaultord][i].trim().replace("\"", "")+"::"+TeamB_arr[defaultord][i].trim().replace("\"", ""));
                                    
                                    teamHome=teamHome.getCheckNameTeam(TeamA_arr[defaultord][i].trim().replace("\"", ""));
                                    teamAway=teamAway.getCheckNameTeam(TeamB_arr[defaultord][i].trim().replace("\"", ""));

                                    start_time_arr[defaultord][i] = start_time_arr[defaultord][i].replace("\"", "");
                                    List<String> elephantList = Arrays.asList(start_time_arr[defaultord][i].split(","));
                                    dateTimeMatch=elephantList.get(0)+"-"+elephantList.get(1)+"-"+elephantList.get(2)+" "+elephantList.get(3)+":"+elephantList.get(4)+":"+elephantList.get(5);
                                        
//                                    System.out.println(dateTimeMatch+"::"+live_bh_arr[defaultord][i].trim()+":"+TeamA_arr[defaultord][i].trim()+":"+TeamB_arr[defaultord][i].trim());
//                                    System.out.println(teamHome.getId()+"::"+teamAway.getId());
                                    if(teamHome!=null && teamAway!=null){
                                         System.out.println("True Group::"+groups_arr[defaultord][i].trim());
                                            PlayMatchModel Match =new PlayMatchModel();
                                                Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[defaultord][i].trim()));
                                                Match.setGroups(groups_arr[defaultord][i].trim());
                                                Match.setLeague_id(ll.getId());
                                                Match.setTeam_home(teamHome.getId());
                                                Match.setTeam_away(teamAway.getId());
                                                Match.setStatus("create");
                                                Match.setTime_match(dateTimeMatch);
                                                Match.setCreated_at(dateFormat.format(date));
                                                Match.setUpdated_at(dateFormat.format(date));
                                                System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+teamAway.getId()+" "+live_bh_arr[defaultord][i]+" "+Match.saveNewMatch());    
                                        }else if(teamHome!=null){
//                                            System.out.println("H True");
                                                PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                    AddTeamAway.setLeague_id(ll.getId());
                                                    AddTeamAway.setName_en(TeamB_arr[defaultord][i].trim().replace("\"", ""));
                                                    AddTeamAway.setName_th("");
                                                    AddTeamAway.setCreated_at(dateFormat.format(date));
                                                    AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                    AddTeamAway.save();

                                                AddTeamAway=AddTeamAway.getCheckNameTeam(TeamB_arr[defaultord][i].trim().replace("\"", ""));

                                                PlayMatchModel Match =new PlayMatchModel();
                                                Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[defaultord][i].trim()));
                                                Match.setGroups(groups_arr[defaultord][i].trim());
                                                Match.setLeague_id(ll.getId());
                                                Match.setTeam_home(teamHome.getId());
                                                Match.setTeam_away(AddTeamAway.getId());
                                                Match.setStatus("create");
                                                Match.setTime_match(dateTimeMatch);
                                                Match.setCreated_at(dateFormat.format(date));
                                                Match.setUpdated_at(dateFormat.format(date));
                                                System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+AddTeamAway.getId()+" "+live_bh_arr[defaultord][i]+" "+Match.saveNewMatch());
                                        }else if(teamAway!=null){
//                                            System.out.println("A True");
                                                PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                    AddTeamHome.setLeague_id(ll.getId());
                                                    AddTeamHome.setName_en(TeamA_arr[defaultord][i].trim().replace("\"", ""));
                                                    AddTeamHome.setName_th("");
                                                    AddTeamHome.setCreated_at(dateFormat.format(date));
                                                    AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                    AddTeamHome.save();

                                                AddTeamHome=AddTeamHome.getCheckNameTeam(TeamA_arr[defaultord][i].trim().replace("\"", ""));

                                                PlayMatchModel Match =new PlayMatchModel();
                                                Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[defaultord][i].trim()));
                                                Match.setGroups(groups_arr[defaultord][i].trim());
                                                Match.setLeague_id(ll.getId());
                                                Match.setTeam_home(AddTeamHome.getId());
                                                Match.setTeam_away(teamAway.getId());
                                                Match.setStatus("create");
                                                Match.setTime_match(dateTimeMatch);
                                                Match.setCreated_at(dateFormat.format(date));
                                                Match.setUpdated_at(dateFormat.format(date));
                                                System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+teamAway.getId()+" "+live_bh_arr[defaultord][i]+" "+Match.saveNewMatch());
                                        }else{
//                                            System.out.println("false");
                                                PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                    AddTeamHome.setLeague_id(ll.getId());
                                                    AddTeamHome.setName_en(TeamA_arr[defaultord][i].trim().replace("\"", ""));
                                                    AddTeamHome.setName_th("");
                                                    AddTeamHome.setCreated_at(dateFormat.format(date));
                                                    AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                    AddTeamHome.save();

                                                AddTeamHome=AddTeamHome.getCheckNameTeam(TeamA_arr[defaultord][i].trim().replace("\"", ""));

                                                PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                    AddTeamAway.setLeague_id(ll.getId());
                                                    AddTeamAway.setName_en(TeamB_arr[defaultord][i].trim().replace("\"", ""));
                                                    AddTeamAway.setName_th("");
                                                    AddTeamAway.setCreated_at(dateFormat.format(date));
                                                    AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                    AddTeamAway.save();
                                                AddTeamAway=AddTeamAway.getCheckNameTeam(TeamB_arr[defaultord][i].trim().replace("\"", ""));

                                                PlayMatchModel Match =new PlayMatchModel();
                                                Match.setMatch_7m_id(Integer.parseInt(live_bh_arr[defaultord][i].trim()));
                                                Match.setGroups(groups_arr[defaultord][i].trim());
                                                Match.setLeague_id(ll.getId());
                                                Match.setTeam_home(AddTeamHome.getId());
                                                Match.setTeam_away(AddTeamAway.getId());
                                                Match.setStatus("create");
                                                Match.setTime_match(dateTimeMatch);
                                                Match.setCreated_at(dateFormat.format(date));
                                                Match.setUpdated_at(dateFormat.format(date));
                                                System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+AddTeamAway.getId()+" "+live_bh_arr[defaultord][i]+" "+Match.saveNewMatch());
                                        } 
                                    }
                            }
                        }
                }
//            }
        }
        
    }
    
    public void getLeagueAll(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        HttpReqController httpPost = new HttpReqController();
        
        String strContent = httpPost.getHTML("http://data.7m.com.cn/matches_data/odds_way_en.shtml");
        Document doc = Jsoup.parse(strContent);
        String[] nameleague=null,idleague=null;
        String dateTimeMatch,nameTeamHome,nameTeamAway,math7mId, symbol, link;
        Elements table_tr = doc.select("div#all").select("td").select("table.ldtb1");
            int leagueId=1;
            for (Element tr : table_tr) {
                PlayCountryModel playCountry =new PlayCountryModel();
                playCountry.setId_7m(tr.attr("onclick").substring(6, tr.attr("onclick").length()-2));
                playCountry.setName(tr.select(".ldtbt1_en").text());
                playCountry.setCreated_at(dateFormat.format(date));
                playCountry.setUpdated_at(dateFormat.format(date));
                playCountry.save();
                System.out.println(tr.attr("onclick").substring(6, tr.attr("onclick").length()-2)+"::"+tr.select(".ldtbt1_en").text());
            }

        PlayCountryModel playCountry =new PlayCountryModel();
        List<PlayCountryModel> lllist = playCountry.getAllCountry(500);
        for (PlayCountryModel ll : lllist) {
            Elements a = doc.select("div#"+ll.getId_7m()).select("a");
            for (Element tr : a) {
                idleague=tr.attr("href").split("/");
                PlayLeagueModel playLeague =new PlayLeagueModel();
                    playLeague.setCountry_id(ll.getId());
                    playLeague.setId_7m(idleague[0]);
                    playLeague.setName(tr.text().replace("·", ""));
                    playLeague.setName_7m("");
                    playLeague.setCreated_at(dateFormat.format(date));
                    playLeague.setUpdated_at(dateFormat.format(date));
                System.out.println(idleague[0]+"::"+tr.text()+"::"+playLeague.save());
            }
        }
        
    }
    
    public void getMatchByLeague(){
        PlayMatchModel playMatch =new PlayMatchModel();
        PlayLeagueModel playLeague =new PlayLeagueModel();
        List<PlayLeagueModel> lllist = playLeague.getAll(1000);
        int lastMatchByleague=0;
        for (PlayLeagueModel ll : lllist){ 
//              if(ll.getId()==37){
                lastMatchByleague=playMatch.lastMatchByleague(ll.getId());
                System.out.println(lastMatchByleague);
                HttpReqController httpPost = new HttpReqController();
                String strContent = httpPost.getHTML("http://data.7m.com.cn/matches_data/"+ll.getId_7m()+"/en/fixture.js");
    //            String strContent = httpPost.getHTML("http://data.7m.com.cn/matches_data/722/en/fixture.js");
                System.out.println("http://data.7m.com.cn/matches_data/"+ll.getId_7m()+"/en/fixture.js");
                Document doc = Jsoup.parse(strContent);
                Elements score = doc.select("body");
                String dateTimeMatch,currentRun = null;
                int leagueId=ll.getId();
                String[] dateTimeMatchList=null,text_run_Head_Arr=null,text_Tmp_bh_Arr=null,text_Run_Arr=null,text_Time_Arr=null,text_TeamA_Arr=null,text_TeamB_Arr=null;
                Date date = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                if(score.select("div").text().equals("")){
                    String[] texts = score.text().split(";");
                    for (String text : texts) {
                        String[] textIns = text.split("=");
                        if(textIns.length==2){
                            if(textIns[1].contains("['") && textIns[1].contains("']")){
                                if(textIns[0].contains("TeamA_Arr")){
                                    text_TeamA_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                                }else if(textIns[0].contains("TeamB_Arr")){
                                    text_TeamB_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                                }else if(textIns[0].contains("Time_Arr")){
                                    text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                }else if(textIns[0].contains("Tmp_bh_Arr")){
                                    text_Tmp_bh_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                }else if(textIns[0].contains("Run_Arr")){
                                    text_Run_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }else if(textIns[0].contains("run_Head_Arr")){
                                    text_run_Head_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }
                            }else if(textIns[1].contains("[") && textIns[1].contains("]")){
                                if(textIns[0].contains("TeamA_Arr")){
                                    text_TeamA_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }else if(textIns[0].contains("TeamB_Arr")){
                                    text_TeamB_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }else if(textIns[0].contains("Time_Arr")){
                                    text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",");
                                }else if(textIns[0].contains("Tmp_bh_Arr")){
                                    text_Tmp_bh_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }else if(textIns[0].contains("Run_Arr")){
                                    text_Run_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }else if(textIns[0].contains("run_Head_Arr")){
                                    text_run_Head_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }
                            }else{
                                if(textIns[0].contains("currentRun")){
                                    currentRun=textIns[1].trim();
                                    System.out.println(currentRun);
                                }
                            }
                        }
                    }

                    if(text_Tmp_bh_Arr!=null){
                        for(int i = 0; i < text_Tmp_bh_Arr.length; i++) {
                            System.out.println(Integer.parseInt(text_Run_Arr[i].trim())+"::"+Integer.parseInt(text_Run_Arr[i].trim())+"::"+text_Tmp_bh_Arr[i].trim());
                           if((Integer.parseInt(text_Run_Arr[i].trim())<=Integer.parseInt(currentRun.trim()))){
                                PlayTeamModel teamHome=new PlayTeamModel();
                                PlayTeamModel teamAway=new PlayTeamModel();
                                System.out.println(text_TeamA_Arr[i].trim().replace("\"", "")+"::"+text_TeamB_Arr[i].trim().replace("\"", ""));
                                teamHome=teamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));
                                teamAway=teamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));
                
                                text_Time_Arr[i] = text_Time_Arr[i].replace("\"", "");
                                List<String> elephantList = Arrays.asList(text_Time_Arr[i].split(","));
                                dateTimeMatch=elephantList.get(0)+"-"+elephantList.get(1)+"-"+elephantList.get(2)+" "+elephantList.get(3)+":"+elephantList.get(4)+":"+elephantList.get(5);

                                System.out.println(dateTimeMatch+"::"+text_Tmp_bh_Arr[i]+":"+text_TeamA_Arr[i]+":"+text_TeamB_Arr[i]);
                                
                                
                                
                                if(teamHome!=null && teamAway!=null){
                                        PlayMatchModel Match =new PlayMatchModel();
                                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                            Match.setLeague_id(ll.getId());
                                            Match.setTeam_home(teamHome.getId());
                                            Match.setTeam_away(teamAway.getId());
                                            Match.setStatus("create");
                                            Match.setTime_match(dateTimeMatch);
                                            Match.setCreated_at(dateFormat.format(date));
                                            Match.setUpdated_at(dateFormat.format(date));
                                            System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+teamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());    
                                    }else if(teamHome!=null){
                                            PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                AddTeamAway.setLeague_id(ll.getId());
                                                AddTeamAway.setName_en(text_TeamB_Arr[i].trim().replace("\"", ""));
                                                AddTeamAway.setName_th("");
                                                AddTeamAway.setCreated_at(dateFormat.format(date));
                                                AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                AddTeamAway.save();

                                            AddTeamAway=AddTeamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));

                                            PlayMatchModel Match =new PlayMatchModel();
                                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                            Match.setLeague_id(ll.getId());
                                            Match.setTeam_home(teamHome.getId());
                                            Match.setTeam_away(AddTeamAway.getId());
                                            Match.setStatus("create");
                                            Match.setTime_match(dateTimeMatch);
                                            Match.setCreated_at(dateFormat.format(date));
                                            Match.setUpdated_at(dateFormat.format(date));
                                            System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+AddTeamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                                    }else if(teamAway!=null){
                                            PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                AddTeamHome.setLeague_id(ll.getId());
                                                AddTeamHome.setName_en(text_TeamA_Arr[i].trim().replace("\"", ""));
                                                AddTeamHome.setName_th("");
                                                AddTeamHome.setCreated_at(dateFormat.format(date));
                                                AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                AddTeamHome.save();

                                            AddTeamHome=AddTeamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));

                                            PlayMatchModel Match =new PlayMatchModel();
                                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                            Match.setLeague_id(ll.getId());
                                            Match.setTeam_home(AddTeamHome.getId());
                                            Match.setTeam_away(teamAway.getId());
                                            Match.setStatus("create");
                                            Match.setTime_match(dateTimeMatch);
                                            Match.setCreated_at(dateFormat.format(date));
                                            Match.setUpdated_at(dateFormat.format(date));
                                            System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+teamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                                    }else{
                                            PlayTeamModel AddTeamHome=new PlayTeamModel();
                                                AddTeamHome.setLeague_id(ll.getId());
                                                AddTeamHome.setName_en(text_TeamA_Arr[i].trim().replace("\"", ""));
                                                AddTeamHome.setName_th("");
                                                AddTeamHome.setCreated_at(dateFormat.format(date));
                                                AddTeamHome.setUpdated_at(dateFormat.format(date));
                                                AddTeamHome.save();

                                            AddTeamHome=AddTeamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));

                                            PlayTeamModel AddTeamAway=new PlayTeamModel();
                                                AddTeamAway.setLeague_id(ll.getId());
                                                AddTeamAway.setName_en(text_TeamB_Arr[i].trim().replace("\"", ""));
                                                AddTeamAway.setName_th("");
                                                AddTeamAway.setCreated_at(dateFormat.format(date));
                                                AddTeamAway.setUpdated_at(dateFormat.format(date));
                                                AddTeamAway.save();
                                            AddTeamAway=AddTeamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));

                                            PlayMatchModel Match =new PlayMatchModel();
                                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                                            Match.setLeague_id(ll.getId());
                                            Match.setTeam_home(AddTeamHome.getId());
                                            Match.setTeam_away(AddTeamAway.getId());
                                            Match.setStatus("create");
                                            Match.setTime_match(dateTimeMatch);
                                            Match.setCreated_at(dateFormat.format(date));
                                            Match.setUpdated_at(dateFormat.format(date));
                                            System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+AddTeamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                                    }    
                            }     
                        }        
                    }
                }
//            }
        }
    }
    
    public void getThaiLeague(){
        HttpReqController httpPost = new HttpReqController();
        String strContent = httpPost.getHTML("http://data.7m.com.cn/matches_data/722/en/fixture.js");
        System.out.println("http://data.7m.com.cn/matches_data/722/en/fixture.js");
        Document doc = Jsoup.parse(strContent);
        Elements score = doc.select("body");
        String dateTimeMatch;
        int leagueId=28;
        String[] dateTimeMatchList=null,text_Tmp_bh_Arr=null,text_Time_Arr=null,text_TeamA_Arr=null,text_TeamB_Arr=null;
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(score.select("div").text().equals("")){
                String[] texts = score.text().split(";");
                for (String text : texts) {
                    String[] textIns = text.split("=");
                    if(textIns.length==2){
                        if(textIns[1].contains("['") && textIns[1].contains("']")){
                            if(textIns[0].contains("TeamA_Arr")){
                                text_TeamA_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                            }else if(textIns[0].contains("TeamB_Arr")){
                                text_TeamB_Arr=textIns[1].substring(3, textIns[1].length()-1).split("','");
                            }else if(textIns[0].contains("Time_Arr")){
                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }else if(textIns[0].contains("Tmp_bh_Arr")){
                                text_Tmp_bh_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }
                        }else if(textIns[1].contains("[") && textIns[1].contains("]")){
                            if(textIns[0].contains("TeamA_Arr")){
                                text_TeamA_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].contains("TeamB_Arr")){
                                text_TeamB_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].contains("Time_Arr")){
                                text_Time_Arr=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("\",");
                            }else if(textIns[0].contains("Tmp_bh_Arr")){
                                text_Tmp_bh_Arr=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }
                        }
                    }
                }
                
                if(text_Tmp_bh_Arr!=null){
                    for(int i = 0; i < text_Tmp_bh_Arr.length; i++) {
                        PlayTeamModel teamHome=new PlayTeamModel();
                        PlayTeamModel teamAway=new PlayTeamModel();
                        
                        teamHome=teamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));
                        teamAway=teamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));
                        
//                        System.out.println(text_TeamA_Arr[i].trim().replace("\"", "")+"::"+text_TeamB_Arr[i].trim().replace("\"", ""));
                        text_Time_Arr[i] = text_Time_Arr[i].replace("\"", "");
                        List<String> elephantList = Arrays.asList(text_Time_Arr[i].split(","));
                        dateTimeMatch=elephantList.get(0)+"-"+elephantList.get(1)+"-"+elephantList.get(2)+" "+elephantList.get(3)+":"+elephantList.get(4)+":"+elephantList.get(5);
                        
                        System.out.println(dateTimeMatch+"::"+text_Tmp_bh_Arr[i]+":"+text_TeamA_Arr[i]+":"+text_TeamB_Arr[i]);
                        if(teamHome!=null && teamAway!=null){
                            PlayMatchModel Match =new PlayMatchModel();
                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                            Match.setLeague_id(leagueId);
                            Match.setTeam_home(teamHome.getId());
                            Match.setTeam_away(teamAway.getId());
                            Match.setStatus("create");
                            Match.setTime_match(dateTimeMatch);
                            Match.setCreated_at(dateFormat.format(date));
                            Match.setUpdated_at(dateFormat.format(date));
                            System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+teamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                        }else if(teamHome!=null){
                            PlayTeamModel AddTeamAway=new PlayTeamModel();
                                AddTeamAway.setLeague_id(leagueId);
                                AddTeamAway.setName_en(text_TeamB_Arr[i].trim().replace("\"", ""));
                                AddTeamAway.setName_th("");
                                AddTeamAway.setCreated_at(dateFormat.format(date));
                                AddTeamAway.setUpdated_at(dateFormat.format(date));
                                AddTeamAway.save();

                            AddTeamAway=AddTeamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));

                            PlayMatchModel Match =new PlayMatchModel();
                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                            Match.setLeague_id(leagueId);
                            Match.setTeam_home(teamHome.getId());
                            Match.setTeam_away(AddTeamAway.getId());
                            Match.setStatus("create");
                            Match.setTime_match(dateTimeMatch);
                            Match.setCreated_at(dateFormat.format(date));
                            Match.setUpdated_at(dateFormat.format(date));
                            System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+AddTeamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                        }else if(teamAway!=null){
                            PlayTeamModel AddTeamHome=new PlayTeamModel();
                                AddTeamHome.setLeague_id(leagueId);
                                AddTeamHome.setName_en(text_TeamA_Arr[i].trim().replace("\"", ""));
                                AddTeamHome.setName_th("");
                                AddTeamHome.setCreated_at(dateFormat.format(date));
                                AddTeamHome.setUpdated_at(dateFormat.format(date));
                                AddTeamHome.save();

                            AddTeamHome=AddTeamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));

                            PlayMatchModel Match =new PlayMatchModel();
                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                            Match.setLeague_id(leagueId);
                            Match.setTeam_home(AddTeamHome.getId());
                            Match.setTeam_away(teamAway.getId());
                            Match.setStatus("create");
                            Match.setTime_match(dateTimeMatch);
                            Match.setCreated_at(dateFormat.format(date));
                            Match.setUpdated_at(dateFormat.format(date));
                            System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+teamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                        }else{
                            PlayTeamModel AddTeamHome=new PlayTeamModel();
                                AddTeamHome.setLeague_id(leagueId);
                                AddTeamHome.setName_en(text_TeamA_Arr[i].trim().replace("\"", ""));
                                AddTeamHome.setName_th("");
                                AddTeamHome.setCreated_at(dateFormat.format(date));
                                AddTeamHome.setUpdated_at(dateFormat.format(date));
                                AddTeamHome.save();

                            AddTeamHome=AddTeamHome.getCheckNameTeam(text_TeamA_Arr[i].trim().replace("\"", ""));

                            PlayTeamModel AddTeamAway=new PlayTeamModel();
                                AddTeamAway.setLeague_id(leagueId);
                                AddTeamAway.setName_en(text_TeamB_Arr[i].trim().replace("\"", ""));
                                AddTeamAway.setName_th("");
                                AddTeamAway.setCreated_at(dateFormat.format(date));
                                AddTeamAway.setUpdated_at(dateFormat.format(date));
                                AddTeamAway.save();
                            AddTeamAway=AddTeamAway.getCheckNameTeam(text_TeamB_Arr[i].trim().replace("\"", ""));

                            PlayMatchModel Match =new PlayMatchModel();
                            Match.setMatch_7m_id(Integer.parseInt(text_Tmp_bh_Arr[i].trim()));
                            Match.setLeague_id(leagueId);
                            Match.setTeam_home(AddTeamHome.getId());
                            Match.setTeam_away(AddTeamAway.getId());
                            Match.setStatus("create");
                            Match.setTime_match(dateTimeMatch);
                            Match.setCreated_at(dateFormat.format(date));
                            Match.setUpdated_at(dateFormat.format(date));
                            System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+AddTeamAway.getId()+" "+text_Tmp_bh_Arr[i]+" "+Match.saveNewMatch());
                        }

//                        System.out.println(dateTimeMatch+" :: "+text_Tmp_bh_Arr[i]+" :: "+teamHome.getId()+":"+text_TeamA_Arr[i]+" :: "+teamAway.getId()+":"+text_TeamB_Arr[i]);
                    }
                }
        }
    }
    
    public void getMatchDetail(){
        PlayMatchModel playMatch =new PlayMatchModel();
        List<PlayMatchModel> lllist = playMatch.getAllMatch7mId(1000);
        System.out.println(lllist.size());
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int home_score=0,away_score=0;
        String playerGoalName="",playerAssetName="";
//        analyse H to H
//        http://analyse.7msport.com/1916310/index.shtml
        
//http://data.7m.com.cn/goaldata/goaltime_stat/1916305.js?nocache=1513921794219
//http://data.7m.com.cn/goaldata/en/1916305.js?nocache=1513921792713
//http://data.7m.com.cn/goaldata/js/1916305.js?nocache=1513921234171
//http://js.wlive.7m.com.cn/lineupdata/en/1916307.js?nocache=1514222801621

//thai
//        http://data.7m.com.cn/matches_data/722/en/index.shtml
        for (PlayMatchModel ll : lllist) {
//            if(ll.getMatch_7m_id()==2050418){
            HttpReqController httpPost = new HttpReqController();
            String strContent = httpPost.getHTML("http://data.7m.com.cn/goaldata/en/"+ll.getMatch_7m_id()+".js");
            System.out.println("http://data.7m.com.cn/goaldata/en/"+ll.getMatch_7m_id()+".js");
            Document doc = Jsoup.parse(strContent);
            Elements score = doc.select("body");
            home_score=0;
            away_score=0;
//            System.out.println(dateDiff(ll.getTime_match(),dateFormat.format(date)));
            if(score.select("div").text().equals("")){
//                System.out.println(score.text());
                String[] texts = score.text().replaceAll("&#355;","").split(";");
                String[] minute=null,text_d_ssx=null,text_d_pn=null,text_ass_d_pn= null,text_d_lx = null,text_d_tm= null,text_d_sx= null,text_d_upn= null,text_d_dpn= null,text_d_stm= null;
                
                for (String text : texts) {
//                    System.out.println(text);
                    String[] textIns = text.split("=");
                    if(textIns.length==2){
//                        System.out.println(textIns[0]+"::"+textIns[0].trim().equals("var d_lx")+"::"+textIns[0].trim().equals("d_lx"));
                        if(textIns[1].contains("['") && textIns[1].contains("']")){
                            if((textIns[0].trim().equals("ass_d_pn") || textIns[0].trim().equals("var ass_d_pn")) && !textIns[1].equals("[]") ){
//                                System.out.println(textIns[1].substring(3, textIns[1].length()-1));
                                text_ass_d_pn=textIns[1].substring(3, textIns[1].length()-1).split("','");
                            }else if(textIns[0].trim().equals("d_pn") || textIns[0].trim().equals("var d_pn")){
//                                System.out.println(textIns[1].substring(3, textIns[1].length()-1));
                                text_d_pn=textIns[1].substring(3, textIns[1].length()-1).split("','");
                            }else if(textIns[0].trim().equals("d_lx") || textIns[0].trim().equals("var d_lx")){
//                                System.out.println(textIns[1]);
                                if(!textIns[1].trim().equals("[]")){
                                    text_d_lx=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                }
                            }else if(textIns[0].trim().equals("d_tm") || textIns[0].trim().equals("var d_tm")){
                                text_d_tm=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }else if(textIns[0].trim().equals("d_sx") || textIns[0].trim().equals("var d_sx")){
                                text_d_sx=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }else if(textIns[0].trim().equals("d_upn") || textIns[0].trim().equals("var d_upn")){
                                text_d_upn=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }else if(textIns[0].trim().equals("d_dpn") || textIns[0].trim().equals("var d_dpn")){
                                text_d_dpn=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }else if(textIns[0].trim().equals("d_stm") || textIns[0].trim().equals("var d_stm")){
                                text_d_stm=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                            }else if(textIns[0].trim().equals("d_ssx") || textIns[0].trim().equals("var d_ssx")){
                                if(!textIns[1].trim().equals("[]")){
                                    text_d_ssx=textIns[1].trim().substring(2, textIns[1].trim().length()-2).split("','");
                                }
                            }
                        }else if(textIns[1].contains("[") && textIns[1].contains("]")){
                            if((textIns[0].trim().equals("ass_d_pn") || textIns[0].trim().equals("var ass_d_pn")) && !textIns[1].equals("[]")){
                                text_ass_d_pn=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].trim().equals("d_pn") || textIns[0].trim().equals("var d_pn")){
                                text_d_pn=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].trim().equals("d_lx") || textIns[0].trim().equals("var d_lx")){
//                                 System.out.println(textIns[1]);
                                if(!textIns[1].trim().equals("[]")){
                                    text_d_lx=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                                }
                            }else if(textIns[0].trim().equals("d_tm") || textIns[0].trim().equals("var d_tm")){
                                text_d_tm=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].trim().equals("d_sx") || textIns[0].trim().equals("var d_sx")){
                                text_d_sx=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].trim().equals("d_upn") || textIns[0].trim().equals("var d_upn")){
                                text_d_upn=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].trim().equals("d_dpn") || textIns[0].trim().equals("var d_dpn")){
                                text_d_dpn=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");
                            }else if(textIns[0].trim().equals("d_stm") || textIns[0].trim().equals("var d_stm")){
                                text_d_stm=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");  
                            }else if(textIns[0].trim().equals("d_ssx") || textIns[0].trim().equals("var d_ssx")){
                                if(!textIns[1].trim().trim().equals("[]")){
                                    text_d_ssx=textIns[1].trim().substring(1, textIns[1].trim().length()-1).split(",");  
                                }
                            }
                        }
                    }
                }
//                System.out.println(""+text_ass_d_pn[0]+" :: "+text_ass_d_pn[(text_ass_d_pn.length-1)]);
//                System.out.println(""+text_d_pn[0]+" :d_pn: "+text_d_pn[(text_d_pn.length-1)]);
//                System.out.println(""+text_d_lx[0]+" :d_lx: "+text_d_lx[(text_d_lx.length-1)]);
//                System.out.println(""+text_d_tm[0]+" :d_tm: "+text_d_tm[(text_d_tm.length-1)]);
//                System.out.println(""+text_d_sx[0]+" :d_sx: "+text_d_sx[(text_d_sx.length-1)]);
//                System.out.println(""+text_d_upn[0]+" :d_upn: "+text_d_upn[(text_d_upn.length-1)]);
//                System.out.println(""+text_d_dpn[0]+" :d_dpn: "+text_d_dpn[(text_d_dpn.length-1)]);
//                System.out.println(""+text_d_stm[0]+" :d_stm: "+text_d_stm[(text_d_stm.length-1)]);
//                System.out.println(""+text_d_ssx[0]+" :d_ssx: "+text_d_ssx[(text_d_ssx.length-1)]);
                
//                System.out.println(">>>>>>>>>>>>>>>>>"+text_d_upn.length+" :: "+text_d_dpn.length);
//                System.out.println(">>>>>>>>"+text_d_pn.length+"::"+text_ass_d_pn.length+"::"+text_d_ssx.length);
                if(text_d_ssx!=null){
                    int count=text_d_ssx.length;
                    if(text_d_upn.length<count){
                        count=text_d_upn.length;
                    }
                    if(text_d_dpn.length<count){
                        count=text_d_dpn.length;
                    }
                    for(int i = 0; i < count; i++) {
//                        System.out.println(i);
                        PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                        PlayPlayerModel PlayerAsset=new PlayPlayerModel();
                        
                        PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_upn[i]);
                        PlayerAsset=PlayerAsset.getCheckNamePlayer(text_d_dpn[i]);
                        
//                        System.out.println(text_d_upn[i]+" :: "+text_d_dpn[i]);
                        if(PlayerGoal!=null && PlayerAsset!=null){                           
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(PlayerAsset.getId());
                                if(text_d_stm[i].contains("+")){
                                    minute=text_d_stm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_stm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_ssx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_ssx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("change");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("change::"+text_d_ssx[i]+":"+PlayerGoal.getId()+":"+text_d_upn[i]+":"+PlayerAsset.getId()+":"+text_d_dpn[i]);
                        }else if(PlayerGoal==null && PlayerAsset!=null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_upn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_upn[i]);

                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(PlayerAsset.getId());
                                if(text_d_stm[i].contains("+")){
                                    minute=text_d_stm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_stm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_ssx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_ssx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("change");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("change::"+text_d_ssx[i]+":"+AddPlayerGoal.getId()+":"+text_d_upn[i]+":"+PlayerAsset.getId()+":"+text_d_dpn[i]);
                        }else if(PlayerAsset==null && PlayerGoal!=null){
                                PlayPlayerModel AddPlayerAsset=new PlayPlayerModel();
                                    AddPlayerAsset.setName(text_d_dpn[i]);
    //                                AddPlayerAsset.setPath("");
                                    AddPlayerAsset.setCreated_at(dateFormat.format(date));
                                    AddPlayerAsset.setUpdated_at(dateFormat.format(date));
                                    AddPlayerAsset.save();
                                AddPlayerAsset=AddPlayerAsset.getCheckNamePlayer(text_d_dpn[i]);

                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(AddPlayerAsset.getId());
                                if(text_d_stm[i].contains("+")){
                                    minute=text_d_stm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_stm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_ssx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_ssx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("change");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save(); 
                                System.out.println("change::"+text_d_ssx[i]+":"+PlayerGoal.getId()+":"+text_d_upn[i]+":"+AddPlayerAsset.getId()+":"+text_d_dpn[i]);
                        }          
                    }
                }

                if(text_d_lx!=null){
                    System.out.println("<<<<<<<<<<"+text_d_lx[0]+" :: "+text_d_lx[(text_d_lx.length-1)]);
                    for(int i = 0; i < text_d_lx.length; i++) {
                        if(i==text_d_lx.length-1){
                            text_d_pn[i]=text_d_pn[i].substring(0, text_d_pn[i].length()-1);
                                if(text_ass_d_pn.length>0){
//                                    System.out.println("text_d_lx:: "+text_d_lx.length+" :: text_ass_d_pn"+text_ass_d_pn.length);
//                                    System.out.println("text_ass_d_pn:: "+text_ass_d_pn[i]);
                                    if(text_ass_d_pn[i].length()>1){
                                        text_ass_d_pn[i]=text_ass_d_pn[i].substring(0, text_ass_d_pn[i].length()-1);
                                    }else{
                                        text_ass_d_pn[i]="";
                                    }
                                }    
                            System.out.println(">>>>>>>>>>"+text_d_lx[i]+" :: "+text_d_pn[i]);
                        }
                        if(text_ass_d_pn!=null){
                            System.out.println("<<<<<<<<<<"+text_d_lx[i]+" :: "+text_d_pn[i]);
                        }else{
                            System.out.println("<<<<<<<<<<"+text_d_lx[i]+" :: "+text_d_pn[i]);
                        }
                        if(text_d_lx[i].equals("0") && text_ass_d_pn!=null ){ //asset goal:goal
                            PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                            PlayPlayerModel PlayerAsset=new PlayPlayerModel();
//                            System.out.println(">>>>>>>>"+text_d_pn[i]+"::"+text_ass_d_pn[i]);
                            PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                            PlayerAsset=PlayerAsset.getCheckNamePlayer(text_ass_d_pn[i]);
                            if(PlayerGoal!=null && PlayerAsset!=null){                           
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                if(text_ass_d_pn[i]!=""){
                                    matchPlayer.setPlayer_id_asset(PlayerAsset.getId());
                                }
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("goal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("assetGoal::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]+":"+PlayerAsset.getId()+":"+text_ass_d_pn[i]);
                            }else if(PlayerGoal==null && PlayerAsset!=null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_pn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_pn[i]);

                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(PlayerAsset.getId());
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("goal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("assetGoal::"+text_d_lx[i]+":"+AddPlayerGoal.getId()+":"+text_d_pn[i]+":"+PlayerAsset.getId()+":"+text_ass_d_pn[i]);
                            }else if(PlayerAsset==null && PlayerGoal!=null){
                                PlayPlayerModel AddPlayerAsset=new PlayPlayerModel();
                                    AddPlayerAsset.setName(text_ass_d_pn[i]);
    //                                AddPlayerAsset.setPath("");
                                    AddPlayerAsset.setCreated_at(dateFormat.format(date));
                                    AddPlayerAsset.setUpdated_at(dateFormat.format(date));
                                    AddPlayerAsset.save();
                                AddPlayerAsset=AddPlayerAsset.getCheckNamePlayer(text_ass_d_pn[i]);

                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(AddPlayerAsset.getId());
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("goal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save(); 
                                System.out.println("assetGoal::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]+":"+AddPlayerAsset.getId()+":"+text_ass_d_pn[i]);
                            }                        
                        }else if(text_d_lx[i].equals("1") || text_d_lx[i].equals("0")){ //goal:goal
                            PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                            PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_pn[i]);
//                            System.out.println(text_d_pn[i].substring(1, text_d_pn[i].length()-1));
                            if(PlayerGoal==null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_pn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                                
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("goal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("goal::"+text_d_lx[i]+":"+AddPlayerGoal.getId()+":"+text_d_pn[i]);
                            }else if(PlayerGoal!=null){
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("goal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("goal::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]);
                            }
                        }else if(text_d_lx[i].equals("2")){ //ยิงเข้าประตูตัวเอง:Owngoal
//                            http://data.7m.com.cn/goaldata/en/1916128.shtml
                            PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                            PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                            if(PlayerGoal==null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_pn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                                
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("Owngoal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("Owngoal::"+text_d_lx[i]+":"+AddPlayerGoal.getId()+":"+text_d_pn[i]);
                            }else if(PlayerGoal!=null){
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("Owngoal");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("Owngoal::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]);
                            }
                        }else if(text_d_lx[i].equals("3")){ //เหลือง:CYellow
                            PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                            PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                            if(PlayerGoal==null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_pn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                                
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("CYellow");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("CYellow::"+text_d_lx[i]+":"+AddPlayerGoal.getId()+":"+text_d_pn[i]);
                            }else if(PlayerGoal!=null){
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("CYellow");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("CYellow::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]);
                            }
                        }else if(text_d_lx[i].equals("4")){ //เเดง:CRed
                            PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                            PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                            if(PlayerGoal==null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_pn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                                
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("CRed");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("CRed::"+text_d_lx[i]+":"+AddPlayerGoal.getId()+":"+text_d_pn[i]);
                            }else if(PlayerGoal!=null){
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("CRed");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("CRed::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]);
                            }
                        }else if(text_d_lx[i].equals("5")){ //เหลืองเเดง:CYellowRed
                            PlayPlayerModel PlayerGoal=new PlayPlayerModel();
                            PlayerGoal=PlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                            if(PlayerGoal==null){
                                PlayPlayerModel AddPlayerGoal=new PlayPlayerModel();
                                    AddPlayerGoal.setName(text_d_pn[i]);
                                    AddPlayerGoal.setCreated_at(dateFormat.format(date));
                                    AddPlayerGoal.setUpdated_at(dateFormat.format(date));
                                    AddPlayerGoal.save();
                                AddPlayerGoal=AddPlayerGoal.getCheckNamePlayer(text_d_pn[i]);
                                
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(AddPlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("CYellowRed");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();                           
                                System.out.println("CYellowRed::"+text_d_lx[i]+":"+AddPlayerGoal.getId()+":"+text_d_pn[i]);
                            }else if(PlayerGoal!=null){
                                PlayMatchPlayerModel matchPlayer=new PlayMatchPlayerModel();
                                matchPlayer.setLeague_id(ll.getLeague_id());                        
                                matchPlayer.setMatch_id(ll.getId());
                                matchPlayer.setPlayer_id(PlayerGoal.getId());
                                matchPlayer.setPlayer_id_asset(0);
                                if(text_d_tm[i].contains("+")){
                                    minute=text_d_tm[i].split("\\+");
                                    matchPlayer.setMinute(Integer.parseInt(minute[0]));
                                    matchPlayer.setOver_minute(Integer.parseInt(minute[1]));
                                }else{
                                    matchPlayer.setMinute(Integer.parseInt(text_d_tm[i]));
                                    matchPlayer.setOver_minute(0);
                                }
                                if(text_d_sx[i].equals("0")){
//                                    home_score++;
                                    matchPlayer.setTeam("home");
                                }else if(text_d_sx[i].equals("-1")){
//                                    away_score++;
                                    matchPlayer.setTeam("away");
                                }
                                matchPlayer.setStatus("CYellowRed");
                                matchPlayer.setCreated_at(dateFormat.format(date));
                                matchPlayer.setUpdated_at(dateFormat.format(date));
                                matchPlayer.save();
                                System.out.println("CYellowRed::"+text_d_lx[i]+":"+PlayerGoal.getId()+":"+text_d_pn[i]);
                            }
                        }
                    }
                }
            }
            
            Calendar parsedDateJs=null;
            
            if(dateDiff(ll.getTime_match(),dateFormat.format(date))>(-360)){
                System.out.println(">>>>>>>>>>>>>>>FullTime IN<<<<<<<<<<<<<<<<");
                String strContentJs = httpPost.getHTML("http://data.7m.com.cn/goaldata/js/"+ll.getMatch_7m_id()+".js");
                System.out.println("http://data.7m.com.cn/goaldata/js/"+ll.getMatch_7m_id()+".js");
                Document docJs = Jsoup.parse(strContentJs);
                Elements scoreJs = docJs.select("body");
                String[] textsJs = scoreJs.text().split(";");
                Date parsedDate=null;
                SimpleDateFormat dateFormatJs = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                try{ 
                    parsedDate = dateFormatJs.parse(ll.getTime_match());
                    for (String textJs : textsJs) {
                        String[] textsJsData = textJs.trim().split("=");
                        if(textsJsData[0].equals("var start_time")){
                            List<String> elephantList = Arrays.asList(textsJsData[1].substring(1,textsJsData[1].length()-1).split(","));
    //                        System.out.println(Integer.parseInt(elephantList.get(1)));
                            parsedDateJs = new GregorianCalendar(Integer.parseInt(elephantList.get(0)),Integer.parseInt(elephantList.get(1))-1,Integer.parseInt(elephantList.get(2)),Integer.parseInt(elephantList.get(3)),Integer.parseInt(elephantList.get(4)),Integer.parseInt(elephantList.get(5)));
    //                        System.out.println(dateFormat.format(parsedDateJs.getTime())+"::"+dateFormat.format(parsedDate)+"::"+dateFormat.format(parsedDateJs.getTime()).equals(dateFormat.format(parsedDate)));
                        }
                    }
                    PlayMatchModel updateMatch=new PlayMatchModel();
                    if(dateFormat.format(parsedDateJs.getTime()).equals(dateFormat.format(parsedDate))){
                        updateMatch.setStatus("fullTime");
                        updateMatch.setHome_end_time_score(home_score);
                        updateMatch.setAway_end_time_score(away_score);
                        updateMatch.setUpdated_at(dateFormat.format(date));
                        updateMatch.setId(ll.getId());
                        updateMatch.updateMatch();
                        System.out.println("update Match fullTime home:"+home_score+" away: "+away_score);
                    }else{
                        updateMatch.setTime_match(dateFormat.format(parsedDateJs.getTime()));
                        updateMatch.setUpdated_at(dateFormat.format(date));
                        updateMatch.setId(ll.getId());
                        updateMatch.updateTimeMatch();
                        System.out.println("update Time Match");
                    }
                }catch(Exception e){
                    
                }
            }
            
            
            
//            Out:เปลิ่ยนออก
//              In:เปลิ่ยนเข้า
//                System.out.println(score);
//            String bf =score.text();
//            String pk =score.select("#pk").select("a").text();
//                System.out.println(bf + ":" + pk);
//        }
        }
    }
    
    public void getLeague(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        HttpReqController httpPost = new HttpReqController();
            String strContent = httpPost.getHTML("http://predict.7msport.com/en/engpr/");
            //System.out.println(""+strContent);
            Document doc = Jsoup.parse(strContent);
//            System.out.println(""+doc.toString());
            String name, name_7m, symbol, link;
//            Elements table_tr = doc.select(".content").select("tbody").select("td");
            Elements option_league = doc.select("#sp_League").select("option");
            //System.out.println(""+table_tr.toString());
            for (Element option : option_league) {
                name = option.text();
                name_7m=option.attr("value");
             
                if(!name_7m.equals("0")){
                    PlayLeagueModel playLeague =new PlayLeagueModel();
                    playLeague.setName(name);
                    playLeague.setName_7m(name_7m);
                    playLeague.setCreated_at(dateFormat.format(date));
                    playLeague.setUpdated_at(dateFormat.format(date));
                    System.out.println(dateFormat.format(date)+" :: "+name+" :: "+name_7m+" :: "+playLeague.save());
                }
//                for (Element div : tr.select("div")) {
//                   
//                }//** end for sol symbol fund

            }//*** end for sol day
            //i=100;
    }
    
    public void getByLeague(){
//        PlayLeagueModel playLeague =new PlayLeagueModel();
//        List<PlayLeagueModel> lllist = playLeague.getAll();
//        for (PlayLeagueModel ll : lllist) {
//            System.out.println(ll.getName() + ":" + ll.getName_7m());
//        }
    }
    
    public void getPrediction(){
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        HttpReqController httpPost = new HttpReqController();
            String strContent = httpPost.getHTML("http://predict.7msport.com/en/engpr/index.shtml");
            //System.out.println(""+strContent);
            Document doc = Jsoup.parse(strContent);
//            System.out.println(""+doc.toString());
            String dateTimeMatch,nameTeamHome,nameTeamAway,math7mId, symbol, link;
//            Elements table_tr = doc.select(".content").select("tbody").select("td");
            Elements table_tr = doc.select(".prd_bar");
            //System.out.println(""+table_tr.toString());
            int leagueId=1;
            for (Element tr : table_tr) {
                PlayTeamModel teamHome=new PlayTeamModel();
                PlayTeamModel teamAway=new PlayTeamModel();
                dateTimeMatch = tr.select(".prd_tbar").select("font").text();
//                System.out.println(">>>>>>>>>>>>>>"+dateTimeMatch);
                List<String> elephantList = Arrays.asList(dateTimeMatch.split(","));
                dateTimeMatch=elephantList.get(0)+"-"+elephantList.get(1)+"-"+elephantList.get(2)+" "+elephantList.get(3)+":"+elephantList.get(4)+":"+elephantList.get(5);
                
                nameTeamHome = tr.select(".tpr_l").select("a").text();
                nameTeamAway = tr.select(".tpr_r").select("a").text();
                
                teamHome=teamHome.getCheckNameTeam(nameTeamHome);
                teamAway=teamAway.getCheckNameTeam(nameTeamAway);
                
                math7mId= tr.select(".tpr_vs").select("a").attr("href");
                math7mId=Arrays.asList(math7mId.replaceAll("[^0-9]+", " ").trim().split(" ")).get(0);
                
                if(teamHome!=null && teamAway!=null){
                    PlayMatchModel Match =new PlayMatchModel();
                    Match.setMatch_7m_id(Integer.parseInt(math7mId));
                    Match.setLeague_id(leagueId);
                    Match.setTeam_home(teamHome.getId());
                    Match.setTeam_away(teamAway.getId());
                    Match.setStatus("create");
                    Match.setTime_match(dateTimeMatch);
                    Match.setCreated_at(dateFormat.format(date));
                    Match.setUpdated_at(dateFormat.format(date));
                    System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+teamAway.getId()+" "+math7mId+" "+Match.saveNewMatch());
                }else if(teamHome!=null){
                    
                    PlayTeamModel AddTeamAway=new PlayTeamModel();
                        AddTeamAway.setLeague_id(leagueId);
                        AddTeamAway.setName_en(nameTeamHome);
                        AddTeamAway.setName_th("");
                        AddTeamAway.setCreated_at(dateFormat.format(date));
                        AddTeamAway.setUpdated_at(dateFormat.format(date));
                        AddTeamAway.save();
            
                    AddTeamAway=AddTeamAway.getCheckNameTeam(nameTeamAway);
                    
                    PlayMatchModel Match =new PlayMatchModel();
                    Match.setMatch_7m_id(Integer.parseInt(math7mId));
                    Match.setLeague_id(leagueId);
                    Match.setTeam_home(teamHome.getId());
                    Match.setTeam_away(AddTeamAway.getId());
                    Match.setStatus("create");
                    Match.setTime_match(dateTimeMatch);
                    Match.setCreated_at(dateFormat.format(date));
                    Match.setUpdated_at(dateFormat.format(date));
                    System.out.println(dateTimeMatch+" "+teamHome.getId()+" "+AddTeamAway.getId()+" "+math7mId+" "+Match.saveNewMatch());
                }else if(teamAway!=null){
                    PlayTeamModel AddTeamHome=new PlayTeamModel();
                        AddTeamHome.setLeague_id(leagueId);
                        AddTeamHome.setName_en(nameTeamHome);
                        AddTeamHome.setName_th("");
                        AddTeamHome.setCreated_at(dateFormat.format(date));
                        AddTeamHome.setUpdated_at(dateFormat.format(date));
                        AddTeamHome.save();
                        
                    AddTeamHome=AddTeamHome.getCheckNameTeam(nameTeamHome);
                    
                    PlayMatchModel Match =new PlayMatchModel();
                    Match.setMatch_7m_id(Integer.parseInt(math7mId));
                    Match.setLeague_id(leagueId);
                    Match.setTeam_home(AddTeamHome.getId());
                    Match.setTeam_away(teamAway.getId());
                    Match.setStatus("create");
                    Match.setTime_match(dateTimeMatch);
                    Match.setCreated_at(dateFormat.format(date));
                    Match.setUpdated_at(dateFormat.format(date));
                    System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+teamAway.getId()+" "+math7mId+" "+Match.saveNewMatch());
                }else{
                    PlayTeamModel AddTeamHome=new PlayTeamModel();
                        AddTeamHome.setLeague_id(leagueId);
                        AddTeamHome.setName_en(nameTeamHome);
                        AddTeamHome.setName_th("");
                        AddTeamHome.setCreated_at(dateFormat.format(date));
                        AddTeamHome.setUpdated_at(dateFormat.format(date));
                        AddTeamHome.save();
                        
                    AddTeamHome=AddTeamHome.getCheckNameTeam(nameTeamHome);
                    
                    PlayTeamModel AddTeamAway=new PlayTeamModel();
                        AddTeamAway.setLeague_id(leagueId);
                        AddTeamAway.setName_en(nameTeamAway);
                        AddTeamAway.setName_th("");
                        AddTeamAway.setCreated_at(dateFormat.format(date));
                        AddTeamAway.setUpdated_at(dateFormat.format(date));
                        AddTeamAway.save();
                    AddTeamAway=AddTeamAway.getCheckNameTeam(nameTeamAway);
                    
                    PlayMatchModel Match =new PlayMatchModel();
                    Match.setMatch_7m_id(Integer.parseInt(math7mId));
                    Match.setLeague_id(leagueId);
                    Match.setTeam_home(AddTeamHome.getId());
                    Match.setTeam_away(AddTeamAway.getId());
                    Match.setStatus("create");
                    Match.setTime_match(dateTimeMatch);
                    Match.setCreated_at(dateFormat.format(date));
                    Match.setUpdated_at(dateFormat.format(date));
                    System.out.println(dateTimeMatch+" "+AddTeamHome.getId()+" "+AddTeamAway.getId()+" "+math7mId+" "+Match.saveNewMatch());
                }
                
//                System.out.println(""+math7mId);
//                for (Element div : tr.select("div")) {
//                   
//                }//** end for sol symbol fund

            }//*** end for sol day
            //i=100;
            
    }
    
    public static int dateDiff(String startDate,String endDate){
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
            try {
			Date startdate = df.parse(startDate);
			Date enddate = df.parse(endDate);
			long diff = enddate.getTime() - startdate.getTime();
			int minuteDiff = (int) (diff / (60*1000));
			return minuteDiff;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;    	
    }
}
