/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.LeagueModel;
import Model.LiveFileModel;
import Model.LiveMatchModel;
import Model.MatchesMapModel;
import Model.TeamModel;
import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.JavaScriptPage;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebRequest;
import com.gargoylesoftware.htmlunit.html.DomElement;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;
import com.gargoylesoftware.htmlunit.xml.XmlPage;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.ImageCopy;

/**
 *
 * @author mrsyrop
 */
public class UrlController {

    protected final String[] lang = {"en", "th"};

    public void getShooter() {
        FileManager fileman = new FileManager();
        LeagueModel leaguem = new LeagueModel();
        List<int[]> leaguelist = leaguem.getLive7m();
        int prev_7m = 0;

        fileman.setPath("shooter");

        String url;
        String referer;
        URL shooterURL;
        HttpURLConnection shooterURLConnection;
        try {
            for (int[] league : leaguelist) {
                if (league[0] != prev_7m) {
                    for (String lng : lang) {
                        fileman.setFilename(league[0] + "_" + lng + ".js");
                        url = "http://data.7m.cn/matches_data/" + league[0] + "/" + lng + "/shooter.js";
                        referer = "http://data.7m.cn/matches_data/" + league[0] + "/" + lng + "/shooter.shtml";
                        shooterURL = new URL(url);
                        shooterURLConnection = (HttpURLConnection) shooterURL.openConnection();
                        shooterURLConnection.setRequestMethod("GET");
                        shooterURLConnection.setRequestProperty("Content-Type", "application/x-javascript");
                        shooterURLConnection.setRequestProperty("Referer", referer);
                        shooterURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
                        shooterURLConnection.connect();
                        String content = "";
                        StringBuffer result = new StringBuffer();
                        String line = "";
                        if (shooterURLConnection.getResponseCode() == 200) {
                            BufferedReader rd = new BufferedReader(new InputStreamReader(shooterURLConnection.getInputStream()));
                            while ((line = rd.readLine()) != null) {
                                result.append(line);
                            }
                            fileman.setContent(result.toString());
                            fileman.write();
                        }
                        prev_7m = league[0];
                        shooterURLConnection.disconnect();
                    }
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void get7mStatistic() {
        FileManager fileman = new FileManager();
        MatchesMapModel mmm = new MatchesMapModel();

        List<MatchesMapModel> mmlist = mmm.getTodayWaitMatch();

        List<String> datascript = new ArrayList<>();

        for (MatchesMapModel mm : mmlist) {
            HashMap<String, String> lnmap = new HashMap<>();
            lnmap.put("en", "http://analyse.7msport.com/");
            lnmap.put("th", "http://analyse.7mth.com/");

            Set<Map.Entry<String, String>> lnset = lnmap.entrySet();
            Iterator<Map.Entry<String, String>> lnit = lnset.iterator();

            while (lnit.hasNext()) {
                Map.Entry<String, String> currentlang = lnit.next();
                datascript.clear();
                fileman.setPath("statistic/" + mm.getO7m_mid() + "/" + currentlang.getKey());
                String url = currentlang.getValue() + mm.getO7m_mid() + "/index.shtml";
                System.out.println(url);
                try (final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
//                webClient.getOptions().setJavaScriptEnabled(false);
                    webClient.getOptions().setThrowExceptionOnScriptError(false);
                    webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
//                webClient.waitForBackgroundJavaScript(60000);
                    webClient.addRequestHeader("referer", url);

                    HtmlPage page = webClient.getPage(url);

                    DomNodeList<DomElement> scriptlist = page.getElementsByTagName("script");
                    for (DomElement script : scriptlist) {
                        String scriptsource = script.getAttribute("src");
//                        System.out.println("Found:" + scriptsource);
                        if (scriptsource.contains("analyse.7m.cn") && scriptsource.contains("" + mm.getO7m_mid())) {
//                            System.out.println("Right script!");
                            datascript.add(scriptsource);

                        }
                    }

                    DomElement formationa = page.getElementById("tdFormationpA");
                    fileman.setFilename("formationa.txt");
                    fileman.setContent(formationa.asText());
                    fileman.write();
                    DomElement formationb = page.getElementById("tdFormationpB");
                    fileman.setFilename("formationb.txt");
                    fileman.setContent(formationb.asText());
                    fileman.write();

                    fileman.setFilename("all.html");
                    fileman.setContent(page.asXml());
                    fileman.write();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                for (String scripturl : datascript) {
                    System.out.println(scripturl);

                    try (final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
                        WebRequest webRequest = new WebRequest(new URL(scripturl));
                        webRequest.setCharset("utf-8");
                        webClient.getOptions().setJavaScriptEnabled(false);
                        webClient.getOptions().setThrowExceptionOnScriptError(false);
                        webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
                        webClient.addRequestHeader("referer", url);

                        JavaScriptPage page = webClient.getPage(webRequest);

                        String[] urlfrag = scripturl.split("/");

                        String content = page.getContent();

//                    byte[] byteText = content.getBytes(Charset.forName("Big5"));
//                    String originalString = new String(byteText, "UTF-8");
                        fileman.setFilename(urlfrag[urlfrag.length - 1]);
                        fileman.setContent(content);
                        fileman.write();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            }

        }

    }

    public void getFixture() {
        FileManager fileman = new FileManager();
        LeagueModel leaguem = new LeagueModel();
        String url;

        fileman.setPath("fixture/mid");

        for (int i = 1; i <= 7; i++) {

            url = "http://data.7m.cn/fixture_data/default_en.shtml?date=" + i;
            try (WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45, "123.30.238.16", 3128)) {
                HtmlPage page = webClient.getPage(url);

//                String pageAsXml = page.asXml();
//                System.out.println(pageAsXml);
//                fileman.setContent(pageAsXml);
//                fileman.write();
//                NamedNodeMap attrs = page.getAttributes();
//                for (int j = 0; j < attrs.getLength(); j++) {
//                    Node attr = attrs.item(j);
//                    System.out.println(attr.toString());
//                }
//
//                List<HtmlAnchor> links = page.getAnchors();
//                for (HtmlAnchor link : links) {
//                    String href = link.getHrefAttribute();
//                    System.out.println(href);
//
//                }
                List<?> rows = page.getByXPath("//tr");
                for (int j = 0; j < rows.size(); j++) {
                    HtmlTableRow row = (HtmlTableRow) rows.get(j);
                    if (row.hasAttribute("name")) {
                        String name = row.getAttribute("name");
                        fileman.setFilename(name + ".html");
                        fileman.setContent(row.asXml());
                        fileman.write();
                    }
                }

//                if (i == 1) {
//                    DomElement row = page.getElementById("mid_1685426");
//                    System.out.println(row.asXml());
//                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public void getLeaguePts() {
        FileManager fileman = new FileManager();
        LeagueModel leaguem = new LeagueModel();
        String url;
        fileman.setPath("leaguepts");
        List<Integer> id7mlist = leaguem.getAllid7m();

        for (Integer id7m : id7mlist) {
            System.out.println(id7m);
            url = "http://data.7m.cn/matches_data/" + id7m + "/en/standing.shtml";
            try (WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
//                webClient.getOptions().setJavaScriptEnabled(false);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                HtmlPage page = webClient.getPage(url);
                fileman.setFilename(id7m + ".html");
                fileman.setContent(page.asXml());
                fileman.write();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public void getLiveMatch() {
        FileManager fileman = new FileManager();
        String url;
        fileman.setPath("futbol24/new");
        Date dt = new Date();
        url = "http://www.futbol24.com/f24/u/liveNow_15.xml?_=" + dt.getTime();
        try (WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
//                webClient.getOptions().setJavaScriptEnabled(false);
//                webClient.getOptions().setThrowExceptionOnScriptError(false);
            XmlPage page = webClient.getPage(url);
            String content = page.asXml();
            System.out.println(content);
//            fileman.setContent(content);
//            fileman.setFilename(dt.getTime() + ".xml");
//            fileman.write();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    
    public void getPredict(){
        String url;
        url = "http://predict.7msport.com/en/engpr";
            System.out.println(url);
            try (WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
                webClient.getOptions().setJavaScriptEnabled(true);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
//                webClient.addRequestHeader("referer", referer);
                WebRequest webRequest = new WebRequest(new URL(url));
                webRequest.setCharset("utf-8");
                XmlPage page = webClient.getPage(webRequest);
                
                String content = page.asXml();
                Document doc = Jsoup.parse(content);
                if (doc != null) {
                    System.out.println("DOC TRUE");
//                    Element f24 = doc.select("f24").first();
//                    Element mecze = f24.select("Mecze").first();
//                    System.out.println(f24.text());
//                    if (f24 != null) {
//                        String name = f24.attr("M");
//                        Elements mlist = mecze.select("M");
//                        if (!mlist.isEmpty()) {
//                            fileman.setContent(content);
//                            fileman.setFilename(name + ".xml");
//                            fileman.write();
//                        }
//                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
    }

    public void getLiveMatchEvent() {
        FileManager fileman = new FileManager();
        String url;
        fileman.setPath("futbol24/update");
        Date dt = new Date();
        LiveFileModel livefilem = new LiveFileModel();
        LiveFileModel lfm = livefilem.getByCurrentdate();
        if (lfm != null) {
            url = "http://www.futbol24.com/f24/u/" + lfm.getPlinkXML() + "?_=" + dt.getTime();
            System.out.println(url);
            try (WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
                webClient.getOptions().setJavaScriptEnabled(true);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
//                webClient.addRequestHeader("referer", referer);
                WebRequest webRequest = new WebRequest(new URL(url));
                webRequest.setCharset("utf-8");
                XmlPage page = webClient.getPage(webRequest);
                
                String content = page.asXml();
                Document doc = Jsoup.parse(content);
                System.out.println(doc.text());
                if (doc != null) {
                    Element f24 = doc.select("f24").first();
                    Element mecze = f24.select("Mecze").first();
                    System.out.println(f24.text());
                    if (f24 != null) {
                        String name = f24.attr("M");
                        Elements mlist = mecze.select("M");
                        if (!mlist.isEmpty()) {
                            fileman.setContent(content);
                            fileman.setFilename(name + ".xml");
                            fileman.write();
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void map7mLiveMatch() {

        LiveMatchModel lmm = new LiveMatchModel();
        FileManager fileman = new FileManager();
        fileman.setPath("7m");
        String url;
        url = "http://ctc.live.7m.cn/datafile/fen.js";
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        try (WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
            webClient.getOptions().setJavaScriptEnabled(false);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            webClient.addRequestHeader("referer", "http://live.7msport.com/default_en.aspx?view=all&line=no");
            JavaScriptPage page = webClient.getPage(url);
            String jscontent = page.getContent();
//            System.out.println(page.getWebResponse().getStatusMessage());
//            System.out.println(page.getContent());
            engine.eval("var sDt=[];var WLID=[]; var ORD=[];");
            engine.eval(jscontent);
            ScriptObjectMirror sdt = (ScriptObjectMirror) engine.get("sDt");

            for (String key : sdt.getOwnKeys(false)) {
                System.out.println("Mid: " + key);

                try {
                    ScriptObjectMirror match = (ScriptObjectMirror) sdt.get(key);

                    int hid = (int) match.getSlot(9);
                    int gid = (int) match.getSlot(10);

                    System.out.println(hid + ":" + gid);
                    System.out.println(match.getSlot(2) + "|" + match.getSlot(3));
                    lmm = new LiveMatchModel();
                    lmm = lmm.findMatchById7m(hid, gid);
                    if (lmm != null) {
                        MatchesMapModel mmm = new MatchesMapModel();
                        mmm.setO7m_mid(Integer.parseInt(key));
                        mmm.setF24_mid(lmm.getMid());
                        mmm.setHn(lmm.getHn());
                        mmm.setGn(lmm.getGn());
                        mmm.setShowdate(lmm.getShowDate());
                        mmm.save();
                        System.out.println("Found:" + lmm.getHn() + "-" + lmm.getGn());
                    } else {
                        System.out.println("Not found!");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            fileman.setFilename("live7m.js");
            fileman.setContent(page.getContent());
            fileman.write();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void get7mLiveEvent() {
        FileManager fileman = new FileManager();
        MatchesMapModel mmm = new MatchesMapModel();

        List<MatchesMapModel> mmlist = mmm.getTodayLiveMatch();

        String[] lang = {"en", "th"};

        for (MatchesMapModel mm : mmlist) {

            System.out.println(mm.getHn() + "-" + mm.getGn());
            for (String ln : lang) {
                fileman.setPath("event/" + mm.getO7m_mid() + "/" + ln);
                String url = "http://data.7m.cn/goaldata/" + ln + "/" + mm.getO7m_mid() + ".js";
                String referer = "http://data.7m.cn/goaldata/" + ln + "/" + mm.getO7m_mid() + ".shtml";
                System.out.println(url);
                try (final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
//                webClient.getOptions().setJavaScriptEnabled(false);
                    webClient.getOptions().setThrowExceptionOnScriptError(false);
                    webClient.addRequestHeader("referer", referer);
                    WebRequest webRequest = new WebRequest(new URL(url));
                    webRequest.setCharset("utf-8");

                    JavaScriptPage page = webClient.getPage(webRequest);

                    fileman.setFilename(mm.getO7m_mid() + ".js");
                    fileman.setContent(page.getContent());
                    fileman.write();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void get7mTeamData() {
        FileManager fileman = new FileManager();
        MatchesMapModel mmm = new MatchesMapModel();
        TeamModel teamm = new TeamModel();
        List<Integer> id7mlist = teamm.getAllTeamId7m();

        for (Integer id7m : id7mlist) {
            fileman.setPath("teamdata/" + id7m);
            String url = "http://team.7msport.com/v2/encrypt/fun/getdata.php?id=600&lang=th";
            String referer = "http://team.7msport.com/" + id7m + "/index.shtml";
            String homeshirt = "http://data.7m.cn/team_data/" + id7m + "/logo_img/mainshirt.jpg";
            String awayshirt = "http://data.7m.cn/team_data/" + id7m + "/logo_img/awayshirt.jpg";
            System.out.println(referer);
            try (final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
                webClient.getOptions().setJavaScriptEnabled(true);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                webClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
//                webClient.addRequestHeader("referer", referer);
                WebRequest webRequest = new WebRequest(new URL(referer));
                webRequest.setCharset("utf-8");

                HtmlPage page = webClient.getPage(webRequest);
                /*
                DomNodeList<DomElement> scriptlist = page.getElementsByTagName("script");
                for (DomElement script : scriptlist) {
                    String scriptsource = script.getAttribute("src");

//                    if (scriptsource.contains("data.7m.cn")) {
                    datascript.add(scriptsource);

//                    }
                }
                 */
                DomElement logo = page.getElementById("logo_img");
                String logourl = logo.getAttribute("src");
                System.out.println(logourl);
                String name = id7m + ".jpg";
                String[] pathsplit;
                if (logourl != null) {
                    pathsplit = logourl.split("/");
                    name = pathsplit[pathsplit.length - 1];
                }
                ImageCopy.copyByUrl(logourl, "teamdata/" + id7m + "/" + name);

                ImageCopy.copyByUrl(homeshirt, "teamdata/" + id7m + "/" + id7m + "_home.jpg");
                ImageCopy.copyByUrl(awayshirt, "teamdata/" + id7m + "/" + id7m + "_away.jpg");

//                DomElement baseinfo = page.getElementById("baseInfo_tb");
//                if (baseinfo != null) {
//                    fileman.setFilename("baseinfo.html");
//                    fileman.setContent(baseinfo.asXml());
//                    fileman.write();
//                }
                fileman.setFilename("page.html");
                fileman.setContent(page.asXml());
                fileman.write();

                String[] headertab = {"javascript:switchTag('tag2');vC(1);loadGlory();", "javascript:switchTag('tag3');vC(2);loadBest();", "javascript:switchTag('tag4');vC(3);loadProfile();", "javascript:switchTag('tag5');vC(4);loadCoach();"};
                for (int i = 0; i < 5; i++) {
                    if (i > 0) {
                        page.executeJavaScript(headertab[i - 1]);
                    }
                    DomElement cc = page.getElementById("cc" + i);
                    if (cc != null) {
                        fileman.setFilename("cc" + i + ".html");
                        fileman.setContent(cc.asXml());
                        fileman.write();
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void get7mOdds() {
        FileManager fileman = new FileManager();

        fileman.setPath("odds");
        String[] urls = {"http://odds.7m.cn/default/data/odds/3000368.js", "http://odds.7m.cn/default/data/odds/3000181.js", "http://odds.7m.cn/default/data/odds/3000068.js"};
        String referer = "http://odds.7m.hk/en/default.shtml";
        for (String url : urls) {
            try (final WebClient webClient = new WebClient(BrowserVersion.FIREFOX_45)) {
                Date dt = new Date();
                webClient.getOptions().setJavaScriptEnabled(false);
                webClient.getOptions().setThrowExceptionOnScriptError(false);
                webClient.addRequestHeader("referer", referer);
                WebRequest webRequest = new WebRequest(new URL(url + "?nocache=" + dt.getTime()));
                webRequest.setCharset("utf-8");

                JavaScriptPage page = webClient.getPage(webRequest);

                String[] urlfrag = url.split("/");

                String content = page.getContent();
                fileman.setFilename(urlfrag[urlfrag.length - 1]);
                fileman.setContent(content);
                fileman.write();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
