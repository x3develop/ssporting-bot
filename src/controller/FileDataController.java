/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.Event7mModel;
import Model.EventModel;
import Model.FixtureModel;
import Model.GameHistoryModel;
import Model.GameInfoModel;
import Model.LeagueModel;
import Model.LineupModel;
import Model.LiveFileModel;
import Model.LiveLeagueModel;
import Model.LiveMatchModel;
import Model.LiveStatsModel;
import Model.MatchesMapModel;
import Model.StatTableModel;
import Model.SubstitutesModel;
import Model.TeamFixtureModel;
import Model.TeamModel;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author mrsyrop
 */
public class FileDataController {

    private Elements mecze;

    public void addFixture() {
        FileManager fileman = new FileManager();
        FileManager target = new FileManager();
        LeagueModel leaguem = new LeagueModel();
        TeamModel teamm = new TeamModel();
        String dir = "fixture/mid";
        fileman.setPath(dir);
        target.setPath("fixture/waitforstat");
        fileman.setFilename(FileManager.getOldestFile(dir));
        try {
            while (FileManager.isFile(fileman)) {
                System.out.println(fileman.getFilename());
                Document doc = Jsoup.parse(fileman.read());
                Elements links = doc.select("a[href]");
                List<String> idlist = new ArrayList<String>();
                String href;
                String lnk = "";
                String homename = "";
                String awayname = "";
                boolean hashome = false;
                boolean hasaway = false;
                for (Element link : links) {
//                System.out.println(link.attr("href"));
//                System.out.println(link.text());
                    if (idlist.size() == 0) {
                        lnk = link.text();
                    } else if (idlist.size() == 1) {
                        homename = link.text();
                    } else {
                        awayname = link.text();
                    }
                    href = link.attr("href");
                    idlist.add(href.replaceAll("\\D+", ""));
                }
                if (idlist.size() == 3) {
                    FixtureModel fixture = leaguem.getLeagueById7m(Integer.parseInt(idlist.get(0)), lnk);
                    if (fixture != null) {
                        hashome = teamm.getLive7m(fixture, Integer.parseInt(idlist.get(1)), "home", homename);
                        hasaway = teamm.getLive7m(fixture, Integer.parseInt(idlist.get(2)), "away", awayname);

                        fixture.setLnk(lnk);
                        Element dtstr = doc.select("div").first();
//                    System.out.println(dtstr.text());
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/M HH:mm");

                        Date date;
                        date = sdf.parse(dtstr.text());
                        Date now = new Date();
                        if (now.getMonth() > date.getMonth()) {
                            date.setYear(now.getYear() + 1);
                        } else {
                            date.setYear(now.getYear());
                        }
//                    System.out.println(date);

                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
                        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                        fixture.setDate(dateFormat.format(date));
                        fixture.setShow_time(timeFormat.format(date));
                        fixture.setTeamNamePk(fixture.getTeamHomeNamePk());
                        if (hashome && hasaway) {
                            fixture.save();
                            System.out.println("saved.");
                        } else {
                            System.out.println("missing data.");
                        }

                    } else {
                        System.out.println("");

                    }
                    target.setFilename(fileman.getFilename());
                    FileManager.move(fileman, target);
                } else {
                    fileman.delete();
                }
                fileman.setFilename(FileManager.getOldestFile(dir));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void addLeaguePts() {
        FileManager fileman = new FileManager();
        LeagueModel leaguem = new LeagueModel();

        String url;
        String dir = "leaguepts";
        fileman.setPath(dir);

        try {

            fileman.setFilename(FileManager.getOldestFile(dir));
            while (FileManager.isFile(fileman)) {
                String filenumber = fileman.getFilename().replaceAll("\\D+", "");
                int id7m = Integer.parseInt(filenumber);
                Document doc = Jsoup.parse(fileman.read());
                Element table = doc.select(".jfbtb1").first();
                if (table != null) {
                    Elements rows = table.select("tr");
                    for (int line = 1; line < rows.size(); line++) {
                        Element row = rows.get(line);
                        StatTableModel stattable = leaguem.getLeagueById7m(id7m);
                        if (stattable != null) {
                            Elements column = row.children();
                            if (column.size() == 10) {
                                for (int i = 0; i < column.size(); i++) {
                                    Element col = column.get(i);
//                                    System.out.println(col.outerHtml());
                                    this.setDataLeaguePts(stattable, col, i);
                                }
                                stattable.save();
                            }
                        } else {
                            System.out.println("No league: " + id7m);
                            break;
                        }

                    }

                } else {
                    table = doc.select("table[cellspacing=3]").first();
//                    System.out.println(table.outerHtml());
                    if (table != null) {
                        String group = "none";
                        Elements rows = table.select("tr");
                        for (int line = 1; line < rows.size(); line++) {
                            Element row = rows.get(line);
                            StatTableModel stattable = leaguem.getLeagueById7m(id7m);

                            if (stattable != null) {
                                Elements column = row.children();
                                if (column.size() == 1) {
                                    group = column.get(0).text();
                                }
//                                System.out.println(group);
                                stattable.setGroup(group);
                                if (column.size() == 10) {
                                    for (int i = 0; i < column.size(); i++) {
                                        Element col = column.get(i);
//                                        System.out.println(col.outerHtml());
                                        this.setDataLeaguePts(stattable, col, i);
                                    }
                                    stattable.save();
                                }
                            }

                        }
                    }

                }
                fileman.delete();
                fileman.setFilename(FileManager.getOldestFile(dir));
            }

//            fileman.write();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setDataLeaguePts(StatTableModel stattable, Element col, int i) {
        TeamModel teamm = new TeamModel();
//        System.out.println(col.outerHtml());
        switch (i) {
            case 0:
                stattable.setNo(Integer.parseInt(col.text()));
                break;
            case 1:
                Element link = col.select("a").first();
                String href = link.attr("href");
                String tid = href.replaceAll("\\D+", "");
                boolean success = teamm.getTeamByid7m(stattable, Integer.parseInt(tid));
                break;

            case 2:
                stattable.setGp(Integer.parseInt(col.text()));
                break;
            case 3:
                stattable.setW(Integer.parseInt(col.text()));
                break;
            case 4:
                stattable.setD(Integer.parseInt(col.text()));
                break;
            case 5:
                stattable.setL(Integer.parseInt(col.text()));
                break;
            case 6:
                stattable.setGf(Integer.parseInt(col.text()));
                break;
            case 7:
                stattable.setGa(Integer.parseInt(col.text()));
                break;
            case 8:
                stattable.setPts(Integer.parseInt(col.text()));
                break;
            default:
                break;
        }
    }

    public HashMap<String, Integer> getProxyList() {
        HashMap<String, Integer> list = new HashMap<>();
        FileManager fileman = new FileManager();
        String dir = "proxy_list";
        String name = "_reliable_list.txt";
        fileman.setPath(dir);
        fileman.setFilename(name);
        String allline = fileman.read();
        Scanner scanner = null;
        try {
            scanner = new Scanner(allline);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
//                System.out.println(line);
                String[] flg = line.split(":");
                if (flg.length == 2) {
                    list.put(flg[0], Integer.parseInt(flg[1]));
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            scanner.close();
        }
        return list;
    }

    public void runLiveMatchEventInfinity() {
        FileManager fileman = new FileManager();
        String dir = "futbol24/new";
        fileman.setPath(dir);
        fileman.setFilename(FileManager.getOldestFile(dir));
        while (FileManager.isFile(fileman)) {
            String content = fileman.read();
            Document doc = Jsoup.parse(content);

            this.addLivefile(doc);
            List<LiveLeagueModel> lllist = this.gatheringLiveLeague(doc);
            LiveLeagueModel llm = new LiveLeagueModel();
            llm.saveAll(lllist);
            List<LiveMatchModel> lmlist = this.gatheringLiveMatch(doc);
            LiveMatchModel lm = new LiveMatchModel();
//            System.out.println(lmlist.size());          
            lm.saveAll(lmlist);

            EventModel eventm = new EventModel();
            List<EventModel> eventlist = this.gatheringLiveEvent(doc);
            eventm.saveAll(eventlist);

            fileman.delete();
            fileman.setFilename(FileManager.getOldestFile(dir));
        }
    }

    public void addLivefile(Document doc) {
        Element f24 = doc.select("F24").first();
        if (f24 != null) {
//            System.out.println(f24.toString());
            LiveFileModel lf = new LiveFileModel();
            Date dt = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            lf.setDate(dateFormat.format(dt));
            lf.setTime(timeFormat.format(dt));
            lf.setPlinkXML(f24.attr("PlikXml"));
            lf.setMl00(Integer.parseInt(f24.attr("ml00")));
            lf.setMl01(Integer.parseInt(f24.attr("ml01")));
            lf.setMl02(Integer.parseInt(f24.attr("ml02")));
            lf.setMl10(Integer.parseInt(f24.attr("ml10")));
            lf.setMl11(Integer.parseInt(f24.attr("ml11")));
            lf.setMl12(Integer.parseInt(f24.attr("ml12")));
            lf.setMa(Integer.parseInt(f24.attr("ma")));
            lf.setZid(Integer.parseInt(f24.attr("zid")));
            if (!lf.save()) {
                System.out.println("new plikxml:" + lf.getPlinkXML() + ", zid:" + lf.getZid());
            }
        }
    }

    public List<LiveLeagueModel> gatheringLiveLeague(Document doc) {
        List<LiveLeagueModel> lllist = new ArrayList<>();
        Element f24 = doc.select("F24").first();
        Elements countrylist = doc.select("Kraje");
        String kn = "";
        String kkod = "";
        int kid = 0;
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dt = fromFormat.parse(f24.attr("d"));
            if (countrylist != null) {
                for (Element country : countrylist.select("K")) {
//                    System.out.println(country.outerHtml());
                    kn = country.attr("KN");
                    kkod = country.attr("KKod");
                    kid = Integer.parseInt(country.attr("KId"));
                    Elements leaguelist = country.select("l");
                    if (leaguelist != null) {
                        for (Element league : leaguelist) {
//                            System.out.println(league.outerHtml());
                            LiveLeagueModel llm = new LiveLeagueModel();
                            llm.setLeagueid(Integer.parseInt(league.attr("LId")));
                            llm.setSubleagueid(Integer.parseInt(league.attr("LId")));
                            llm.setKn(kn);
                            llm.setKkod(kkod);
                            llm.setDate(toFormat.format(dt));
                            llm.setCompetitionid(kid);
                            llm.setLn(league.attr("ln"));
                            llm.setLnk(league.attr("lnk"));
                            llm.setBg(league.attr("fg"));
                            llm.setFg(league.attr("fg"));
                            llm.setOid(Integer.parseInt(league.attr("oid")));

                            Elements subleaguelist = league.select("pl");
                            if (subleaguelist != null) {
                                for (Element subleague : subleaguelist) {
                                    llm.setSubleagueid(Integer.parseInt(subleague.attr("lid")));
                                    llm.setLeagueid(Integer.parseInt(subleague.attr("_lid")));
                                    llm.setSubleaguename(subleague.attr("ln"));
                                }
                            }
                            lllist.add(llm);
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return lllist;
    }

    public List<LiveMatchModel> gatheringLiveMatch(Document doc) {
        List<LiveMatchModel> lmlist = new ArrayList<>();
        Elements matchlist = doc.select("Mecze");
        Element f24 = doc.select("F24").first();
        SimpleDateFormat fromFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd");
        int sumWait = 0;
        int sumLive = 0;
        int sumResult = 0;
        if (matchlist != null) {
            try {
                Date sdt = fromFormat.parse(f24.attr("d"));
                for (Element match : matchlist.select("M")) {
                    LiveMatchModel matchm = new LiveMatchModel();

                    matchm.setMid(Integer.parseInt(match.attr("MId")));
                    matchm.setLid(Integer.parseInt(match.attr("LId")));
                    matchm.setUnderscorelid(Integer.parseInt(match.attr("_LId")));
                    matchm.setOid(Integer.parseInt(match.attr("oid")));
                    matchm.setSid(Integer.parseInt(match.attr("sid")));
                    matchm.setKid(Integer.parseInt(match.attr("kid")));
                    matchm.setLnr(Integer.parseInt(match.attr("lnr")));
                    matchm.setC0(Integer.parseInt(match.attr("c0")));
                    matchm.setC1(Integer.parseInt(match.attr("c1")));
                    matchm.setC2(Integer.parseInt(match.attr("c2")));
                    matchm.setMl(Integer.parseInt(match.attr("ml")));
                    matchm.setHid(Integer.parseInt(match.attr("hid")));
                    matchm.setGid(Integer.parseInt(match.attr("gid")));
                    matchm.setHn(match.attr("hn"));
                    matchm.setGn(match.attr("gn"));
                    matchm.setHrci(Integer.parseInt(match.attr("hrci")));
                    matchm.setGrci(Integer.parseInt(match.attr("grci")));
                    matchm.setHrc(match.attr("hrc"));
                    matchm.setGrc(match.attr("grc"));
                    matchm.setS1(match.attr("s1"));
                    matchm.setS2(match.attr("s2"));
                    matchm.setMstan(Integer.parseInt(match.attr("mstan")));
                    matchm.setHstan(Integer.parseInt(match.attr("hstan")));
                    matchm.setGoli(Integer.parseInt(match.attr("goli")));
                    matchm.setL(Integer.parseInt(match.attr("l")));
                    matchm.setA(Integer.parseInt(match.attr("a")));
                    matchm.setAo(Integer.parseInt(match.attr("ao")));
                    matchm.setInfo(match.attr("info"));
                    matchm.setOtv(match.attr("otv"));
//                    System.out.println(match.attr("cx"));
                    if (match.hasAttr("cx")) {
                        matchm.setCx(Integer.parseInt(match.attr("cx")));
                    } else {
                        matchm.setCx(0);
                    }

                    SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date dt = new Date();
                    dt.setTime(matchm.getC0() * 1000L);
                    matchm.setDate(dFormat.format(dt));
                    matchm.setShowDate(toFormat.format(sdt));

//                    matchm.save();
                    lmlist.add(matchm);

                    if (matchm.getSid() == 1) {
                        sumWait++;
                    } else if (matchm.getSid() > 1 && matchm.getLnr() == 1) {
                        sumLive++;
                    } else if (matchm.getSid() > 4 && matchm.getLnr() == 2) {
                        sumResult++;
                    }

                }

                FileManager fileman = new FileManager();
                String dir = "file";
                String name = "sumWait.txt";
                fileman.setPath(dir);
                fileman.setFilename(name);
                fileman.setContent(sumWait + "");
                fileman.write();
                name = "sumLive.txt";
                fileman.setFilename(name);
                fileman.setContent(sumLive + "");
                fileman.write();
                name = "sumResult.txt";
                fileman.setFilename(name);
                fileman.setContent(sumResult + "");
                fileman.write();
                System.out.println("live:" + sumLive + ",wait:" + sumWait + ",result:" + sumResult);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return lmlist;
    }

    public List<EventModel> gatheringLiveEvent(Document doc) {
        List<EventModel> eventlist = new ArrayList<>();
        Element zd = doc.select("Zd").first();
        EventModel evd = new EventModel();
        int lastid = evd.getLastEventID();
//        System.out.println("Last id:" + lastid);
        if (zd != null) {
            for (Element event : zd.select("Z")) {
                if (lastid < Integer.parseInt(event.attr("zdid"))) {
                    EventModel eventm = new EventModel();
                    eventm.setZdid(event.attr("zdid"));
                    eventm.setZdrid(event.attr("zdrid"));
                    eventm.setZdz(event.attr("zdz"));
                    eventm.setMid(event.attr("mid"));
                    eventm.setA(event.attr("a"));
                    eventm.setHn(event.attr("hn"));
                    eventm.setGn(event.attr("gn"));
                    eventm.setCm(event.attr("cm"));
                    eventm.setS(event.attr("s"));
                    eventlist.add(eventm);
                }
            }
            evd.updateLastEventID(Integer.parseInt(zd.attr("zdid")));
        }
        return eventlist;
    }

    public void gatheringLiveUpdate() {

        FileManager fileman = new FileManager();
        FileManager writer = new FileManager();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new Date());
        String dir = "futbol24/update";
        fileman.setPath(dir);
        writer.setPath("gameupdate/" + date);
        fileman.setFilename(FileManager.getOldestFile(dir));
        JSONParser parser = new JSONParser();
        while (FileManager.isFile(fileman)) {
            String content = fileman.read();
            Document doc = Jsoup.parse(content);
            Element f24 = doc.select("f24").first();
            String f24m = f24.attr("M");
            mecze = f24.select("mecze");
            JSONArray jarr = new JSONArray();
            String jsonpattern = "{\"hrci\":\"\",\"hid\":\"\",\"gid\":\"\",\"lid\":\"\",\"hn\":\"\",\"kid\":\"\",\"mid\":\"\",\"oid\":\"\",\"mstan\":\"\",\"sid\":\"\",\"zid\":\"\",\"gstan\":\"\",\"goli\":\"\",\"hrc\":\"\",\"hstan\":\"\",\"grc\":\"\",\"s1\":\"\",\"ml\":\"\",\"info\":\"\",\"s2\":\"\",\"grci\":\"\",\"a\":\"\",\"_lid\":\"\",\"gn\":\"\",\"l\":\"\",\"c0\":\"\",\"n\":\"\",\"c1\":\"\",\"ao\":\"\",\"c2\":\"\",\"otv\":\"\",\"lnr\":\"\",\"rek_h\":\"\",\"cx\":\"\",\"w\":\"\",\"x\":\"\"}";
            for (Element m : mecze.select("M")) {
                JSONObject jsn = new JSONObject();
                try {
                    jsn = (JSONObject) parser.parse(jsonpattern);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                List<Attribute> attrs = m.attributes().asList();
                for (Attribute attr : attrs) {
//                    System.out.println(attr.getKey() + ":" + attr.getValue());
                    jsn.put(attr.getKey().toLowerCase(), attr.getValue());
                }
                jarr.add(jsn);
            }
            String zid = mecze.attr("zid");
            writer.setFilename(zid + ".json");
            writer.setContent(jarr.toJSONString());
            writer.write();
            fileman.delete();
            fileman.setFilename(FileManager.getOldestFile(dir));
        }
    }

    public void gatheringLineUp() {
        FileManager fileman = new FileManager();
        String maindir = "statistic";
        String gameinfopath = "en/gameinfo_en.js";
        String lnthpath = "th/gamelineup_th.js";
        String lnenpath = "en/gamelineup_en.js";
        MatchesMapModel mmm = new MatchesMapModel();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        HashMap<String, LineupModel> lineupmap = new HashMap<>();

        List<MatchesMapModel> mmlist = mmm.getTodayWaitMatch();

        for (MatchesMapModel mm : mmlist) {
            fileman.setPath(maindir + "/" + mm.getO7m_mid());
            fileman.setFilename(gameinfopath);
            if (FileManager.isFile(fileman)) {
                String gameinfojs = fileman.read();
                engine = new ScriptEngineManager().getEngineByName("nashorn");

                try {
                    engine.eval(gameinfojs);

                    ScriptObjectMirror gameinfo = (ScriptObjectMirror) engine.get("gameInfo");
                    String taid = (String) gameinfo.get("taid");
                    String tbid = (String) gameinfo.get("tbid");
                    String taname = (String) gameinfo.get("taname");
                    String tbname = (String) gameinfo.get("tbname");
                    System.out.println("===" + mm.getO7m_mid() + ":" + taname + " - " + tbname + "===");

                    fileman.setFilename(lnenpath);
                    if (FileManager.isFile(fileman)) {
                        engine.eval(fileman.read());
                        ScriptObjectMirror lineupjs = (ScriptObjectMirror) engine.get("gameLineup");
                        ScriptObjectMirror ateam = (ScriptObjectMirror) lineupjs.get("A");
                        if (ateam != null) {
                            for (int asize = 0; asize < ateam.size(); asize++) {
                                ScriptObjectMirror player = (ScriptObjectMirror) ateam.getSlot(asize);

                                String pid = (String) player.get("id");
                                String name = (String) player.get("n");
                                String position = (String) player.get("p");
                                String status = (String) player.get("s");
                                System.out.println(pid + ":" + name);

                                LineupModel playerinline = new LineupModel();
                                playerinline.setMid(mm.getO7m_mid());
                                playerinline.setTid(Integer.parseInt(taid));
                                playerinline.setPid(Integer.parseInt(pid));
                                playerinline.setName_en(name);
                                playerinline.setPosition(Integer.parseInt(position));
                                playerinline.setStatus(Integer.parseInt(status));
                                playerinline.setSide("home");
                                lineupmap.put(pid, playerinline);
                            }
                        }

                        ScriptObjectMirror bteam = (ScriptObjectMirror) lineupjs.get("B");
                        if (bteam != null) {
                            for (int bsize = 0; bsize < bteam.size(); bsize++) {
                                ScriptObjectMirror player = (ScriptObjectMirror) bteam.getSlot(bsize);

                                String pid = (String) player.get("id");
                                String name = (String) player.get("n");
                                String position = (String) player.get("p");
                                String status = (String) player.get("s");
                                System.out.println(pid + ":" + name);

                                LineupModel playerinline = new LineupModel();
                                playerinline.setMid(mm.getO7m_mid());
                                playerinline.setTid(Integer.parseInt(tbid));
                                playerinline.setPid(Integer.parseInt(pid));
                                playerinline.setName_en(name);
                                playerinline.setPosition(Integer.parseInt(position));
                                playerinline.setStatus(Integer.parseInt(status));
                                playerinline.setSide("away");
                                lineupmap.put(pid, playerinline);
                            }
                        }

                    }

                    fileman.setFilename(lnthpath);
                    if (FileManager.isFile(fileman)) {
                        engine.eval(fileman.read());
                        ScriptObjectMirror lineupjs = (ScriptObjectMirror) engine.get("gameLineup");
                        ScriptObjectMirror ateam = (ScriptObjectMirror) lineupjs.get("A");
                        if (ateam != null) {
                            for (int asize = 0; asize < ateam.size(); asize++) {
                                ScriptObjectMirror player = (ScriptObjectMirror) ateam.getSlot(asize);
                                String pid = (String) player.get("id");
                                String name = (String) player.get("n");
                                System.out.println(pid + ":" + name);

                                lineupmap.get(pid).setName_th(name);

                            }
                        }

                        ScriptObjectMirror bteam = (ScriptObjectMirror) lineupjs.get("B");
                        if (bteam != null) {
                            for (int bsize = 0; bsize < bteam.size(); bsize++) {
                                ScriptObjectMirror player = (ScriptObjectMirror) bteam.getSlot(bsize);
                                String pid = (String) player.get("id");
                                String name = (String) player.get("n");
                                System.out.println(pid + ":" + name);

                                lineupmap.get(pid).setName_th(name);

                            }
                        }
                    }

                    Set<Map.Entry<String, LineupModel>> lineset = lineupmap.entrySet();
                    Iterator<Map.Entry<String, LineupModel>> lineit = lineset.iterator();

                    ArrayList<LineupModel> linelist = new ArrayList<>();
                    while (lineit.hasNext()) {
                        Map.Entry<String, LineupModel> lnmp = lineit.next();
//                        lnmp.getValue().save();
                        linelist.add(lnmp.getValue());
                    }
                    LineupModel playerinline = new LineupModel();
                    playerinline.saveAll(linelist);

                } catch (ScriptException ex) {
                    ex.printStackTrace();
                }

            }

        }

    }

    public void gatheringGamehistory() {
        FileManager fileman = new FileManager();
        String maindir = "statistic";
        String gameinfopath = "en/gameinfo_en.js";
        String lnenpath = "en/gamehistory_en.js";

        MatchesMapModel mmm = new MatchesMapModel();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        HashMap<String, LineupModel> lineupmap = new HashMap<>();

        List<MatchesMapModel> mmlist = mmm.getTodayWaitMatch();

        for (MatchesMapModel mm : mmlist) {
            fileman.setPath(maindir + "/" + mm.getO7m_mid());
            fileman.setFilename(gameinfopath);
            if (FileManager.isFile(fileman)) {
                String gameinfojs = fileman.read();
                engine = new ScriptEngineManager().getEngineByName("nashorn");

                try {
                    engine.eval(gameinfojs);

                    ScriptObjectMirror gameinfo = (ScriptObjectMirror) engine.get("gameInfo");
                    String taname = (String) gameinfo.get("taname");
                    String tbname = (String) gameinfo.get("tbname");
                    System.out.println(mm.getO7m_mid() + ":" + taname + " - " + tbname);

                    fileman.setFilename(lnenpath);
                    if (FileManager.isFile(fileman)) {
                        engine.eval(fileman.read());
                        ScriptObjectMirror gamehistory = (ScriptObjectMirror) engine.get("gamehistory");
                        ScriptObjectMirror gamehistoryjs = (ScriptObjectMirror) gamehistory.get("historymatch");
                        ScriptObjectMirror midlist = (ScriptObjectMirror) gamehistoryjs.get("id");
                        ScriptObjectMirror lidlist = (ScriptObjectMirror) gamehistoryjs.get("mid");
                        ScriptObjectMirror hidlist = (ScriptObjectMirror) gamehistoryjs.get("aid");
                        ScriptObjectMirror gidlist = (ScriptObjectMirror) gamehistoryjs.get("bid");
                        ScriptObjectMirror datelist = (ScriptObjectMirror) gamehistoryjs.get("date");
                        ScriptObjectMirror homescorelist = (ScriptObjectMirror) gamehistoryjs.get("liveA");
                        ScriptObjectMirror awayscorelist = (ScriptObjectMirror) gamehistoryjs.get("liveB");
                        ScriptObjectMirror oddslist = (ScriptObjectMirror) gamehistoryjs.get("rq");
                        ScriptObjectMirror resultlist = (ScriptObjectMirror) gamehistoryjs.get("worl");
                        ScriptObjectMirror oddsresultlist = (ScriptObjectMirror) gamehistoryjs.get("oworl");
                        ScriptObjectMirror htlist = (ScriptObjectMirror) gamehistoryjs.get("bc");

                        if (midlist != null) {
                            ArrayList<GameHistoryModel> ghlist = new ArrayList<>();
                            for (int arrsize = 0; arrsize < midlist.size(); arrsize++) {
                                Integer mid = (Integer) midlist.getSlot(arrsize);
                                Integer lid = (Integer) lidlist.getSlot(arrsize);
                                Integer hid = (Integer) hidlist.getSlot(arrsize);
                                Integer gid = (Integer) gidlist.getSlot(arrsize);
                                String date = (String) datelist.getSlot(arrsize);
                                Integer hscore = (Integer) homescorelist.getSlot(arrsize);
                                Integer ascore = (Integer) awayscorelist.getSlot(arrsize);
                                String odds = (String) oddslist.getSlot(arrsize);
                                Integer result = (Integer) resultlist.getSlot(arrsize);
                                Integer oddsresult = (Integer) oddsresultlist.getSlot(arrsize);
                                String ht = (String) htlist.getSlot(arrsize);

                                SimpleDateFormat dFormat = new SimpleDateFormat("dd/MM/yy");
                                SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");
                                Date dt;
                                try {
                                    dt = dFormat.parse(date);
                                    date = dbFormat.format(dt);
                                } catch (java.text.ParseException ex) {
                                    ex.printStackTrace();
                                }

                                GameHistoryModel ghm = new GameHistoryModel();
                                ghm.setMid(mm.getO7m_mid());
                                ghm.setHistory_mid(mid);
                                ghm.setLid(lid);
                                ghm.setHid(hid);
                                ghm.setGid(gid);
                                ghm.setShow_date(date);
                                ghm.setHome_score(hscore);
                                ghm.setAway_score(ascore);
                                ghm.setOdds(odds);
                                ghm.setResult(result);
                                ghm.setOdds_result(oddsresult);
                                ghm.setHt_score(ht);
                                ghm.setType("game");
//                                ghm.save();
                                ghlist.add(ghm);
                            }
                            GameHistoryModel ghm = new GameHistoryModel();
                            ghm.saveAll(ghlist);
                        }

                    }

//                    Set<Map.Entry<String, LineupModel>> lineset = lineupmap.entrySet();
//                    Iterator<Map.Entry<String, LineupModel>> lineit = lineset.iterator();
//                    while (lineit.hasNext()) {
//                        Map.Entry<String, LineupModel> lnmp = lineit.next();
//                        lnmp.getValue().save();
//                    }
                } catch (ScriptException ex) {
                    ex.printStackTrace();
                }

            }

        }

    }

    public void gatheringGameTeamhistory() {
        FileManager fileman = new FileManager();
        String maindir = "statistic";
        String gameinfopath = "en/gameinfo_en.js";
        String lnenpath = "en/gameteamhistory_en.js";
        String[] side = {"A", "B"};

        MatchesMapModel mmm = new MatchesMapModel();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        HashMap<String, LineupModel> lineupmap = new HashMap<>();

        List<MatchesMapModel> mmlist = mmm.getTodayWaitMatch();

        for (MatchesMapModel mm : mmlist) {
            fileman.setPath(maindir + "/" + mm.getO7m_mid());
            fileman.setFilename(gameinfopath);
            if (FileManager.isFile(fileman)) {
                String gameinfojs = fileman.read();
                engine = new ScriptEngineManager().getEngineByName("nashorn");

                try {
                    engine.eval(gameinfojs);
                    ScriptObjectMirror gameinfo = (ScriptObjectMirror) engine.get("gameInfo");
                    String taname = (String) gameinfo.get("taname");
                    String tbname = (String) gameinfo.get("tbname");
                    System.out.println(mm.getO7m_mid() + ":" + taname + " - " + tbname);

                    fileman.setFilename(lnenpath);
                    if (FileManager.isFile(fileman)) {
                        engine.eval(fileman.read());

                        ScriptObjectMirror gamehistory = (ScriptObjectMirror) engine.get("gameTeamHistory");
                        for (String sideselect : side) {

                            ScriptObjectMirror sidedata = (ScriptObjectMirror) gamehistory.get(sideselect);
                            ScriptObjectMirror sidedatatotal = (ScriptObjectMirror) sidedata.get("all");

                            ScriptObjectMirror gamehistoryjs = (ScriptObjectMirror) sidedatatotal.get("history");
                            ScriptObjectMirror midlist = (ScriptObjectMirror) gamehistoryjs.get("id");
                            ScriptObjectMirror lidlist = (ScriptObjectMirror) gamehistoryjs.get("mid");
                            ScriptObjectMirror hidlist = (ScriptObjectMirror) gamehistoryjs.get("aid");
                            ScriptObjectMirror gidlist = (ScriptObjectMirror) gamehistoryjs.get("bid");
                            ScriptObjectMirror datelist = (ScriptObjectMirror) gamehistoryjs.get("date");
                            ScriptObjectMirror homescorelist = (ScriptObjectMirror) gamehistoryjs.get("liveA");
                            ScriptObjectMirror awayscorelist = (ScriptObjectMirror) gamehistoryjs.get("liveB");
                            ScriptObjectMirror oddslist = (ScriptObjectMirror) gamehistoryjs.get("rq");
                            ScriptObjectMirror resultlist = (ScriptObjectMirror) gamehistoryjs.get("worl");
                            ScriptObjectMirror oddsresultlist = (ScriptObjectMirror) gamehistoryjs.get("oworl");
                            ScriptObjectMirror htlist = (ScriptObjectMirror) gamehistoryjs.get("bc");

                            if (midlist != null) {
                                ArrayList<GameHistoryModel> ghlist = new ArrayList<>();
                                for (int arrsize = 0; arrsize < midlist.size(); arrsize++) {
                                    Integer mid = (Integer) midlist.getSlot(arrsize);
                                    Integer lid = (Integer) lidlist.getSlot(arrsize);
                                    Integer hid = (Integer) hidlist.getSlot(arrsize);
                                    Integer gid = (Integer) gidlist.getSlot(arrsize);
                                    String date = (String) datelist.getSlot(arrsize);
                                    Integer hscore = (Integer) homescorelist.getSlot(arrsize);
                                    Integer ascore = (Integer) awayscorelist.getSlot(arrsize);
                                    String odds = (String) oddslist.getSlot(arrsize);
                                    Integer result = (Integer) resultlist.getSlot(arrsize);
                                    Integer oddsresult = (Integer) oddsresultlist.getSlot(arrsize);
                                    String ht = (String) htlist.getSlot(arrsize);

                                    SimpleDateFormat dFormat = new SimpleDateFormat("dd/MM/yy");
                                    SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date dt;
                                    try {
                                        dt = dFormat.parse(date);
                                        date = dbFormat.format(dt);
                                    } catch (java.text.ParseException ex) {
                                        ex.printStackTrace();
                                    }

                                    GameHistoryModel ghm = new GameHistoryModel();
                                    ghm.setMid(mm.getO7m_mid());
                                    ghm.setHistory_mid(mid);
                                    ghm.setLid(lid);
                                    ghm.setHid(hid);
                                    ghm.setGid(gid);
                                    ghm.setShow_date(date);
                                    ghm.setHome_score(hscore);
                                    ghm.setAway_score(ascore);
                                    ghm.setOdds(odds);
                                    ghm.setResult(result);
                                    ghm.setOdds_result(oddsresult);
                                    ghm.setHt_score(ht);
                                    if (sideselect == "A") {
                                        ghm.setType("home");
                                    } else {
                                        ghm.setType("away");
                                    }
//                                    ghm.save();
                                    ghlist.add(ghm);
                                }
                                GameHistoryModel ghm = new GameHistoryModel();
                                ghm.saveAll(ghlist);
                            }
                        }

                    }

                } catch (ScriptException ex) {
                    ex.printStackTrace();
                }

            }

        }

    }

    public void gatheringGameInfo() {
        FileManager fileman = new FileManager();
        String maindir = "statistic";
        String gameinfopath = "en/gameinfo_en.js";
        String homeformationpath = "en/formationa.txt";
        String awayformationpath = "en/formationb.txt";

        MatchesMapModel mmm = new MatchesMapModel();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        List<MatchesMapModel> mmlist = mmm.getTodayWaitMatch();

        for (MatchesMapModel mm : mmlist) {
            fileman.setPath(maindir + "/" + mm.getO7m_mid());
            fileman.setFilename(gameinfopath);
            if (FileManager.isFile(fileman)) {
                String gameinfojs = fileman.read();
                engine = new ScriptEngineManager().getEngineByName("nashorn");

                try {
                    engine.eval(gameinfojs);
                    ScriptObjectMirror gameinfo = (ScriptObjectMirror) engine.get("gameInfo");
                    String taname = (String) gameinfo.get("taname");
                    String tbname = (String) gameinfo.get("tbname");
                    String tarank = (String) gameinfo.get("tarank");
                    String tbrank = (String) gameinfo.get("tbrank");
                    String tadata = (String) gameinfo.get("tadata");
                    String tbdata = (String) gameinfo.get("tadata");
                    String channel = (String) gameinfo.get("channel");
                    String weather = (String) gameinfo.get("weather");
                    String temperature = (String) gameinfo.get("temperature");
                    String forma = "";
                    String formb = "";

                    fileman.setFilename(homeformationpath);
                    if (FileManager.isFile(fileman)) {
                        String content = fileman.read();
                        String[] contents = content.split(":");
                        if (contents.length == 2) {
                            forma = contents[1];
                        }
                    }
                    fileman.setFilename(awayformationpath);
                    if (FileManager.isFile(fileman)) {
                        String content = fileman.read();
                        String[] contents = content.split(":");
                        if (contents.length == 2) {
                            formb = contents[1];
                        }
                    }
                    System.out.println(mm.getO7m_mid() + ":" + taname + " - " + tbname);

                    GameInfoModel gim = new GameInfoModel();
                    gim.setMid(mm.getO7m_mid());
                    gim.setHomerank(tarank);
                    gim.setAwayrank(tbrank);
                    gim.setHomelastresult(tadata);
                    gim.setAwaylastresult(tbdata);
                    gim.setChannel(channel);
                    gim.setWeather(weather);
                    gim.setTemperature(temperature);
                    gim.setHomeformation(forma);
                    gim.setAwayformation(formb);
                    gim.save();

                } catch (ScriptException ex) {
                    ex.printStackTrace();
                }

            }

        }

    }

    public void gatheringGameEvent() {
        FileManager fileman = new FileManager();
        String maindir = "event";

        MatchesMapModel mmm = new MatchesMapModel();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        HashMap<String, LineupModel> lineupmap = new HashMap<>();

        List<MatchesMapModel> mmlist = mmm.getTodayLiveMatch();

        for (MatchesMapModel mm : mmlist) {
            fileman.setPath(maindir + "/" + mm.getO7m_mid() + "/en");
            fileman.setFilename(mm.getO7m_mid() + ".js");
            engine = new ScriptEngineManager().getEngineByName("nashorn");

            System.out.println(mm.getO7m_mid() + ":" + mm.getHn() + "-" + mm.getGn());
            if (FileManager.isFile(fileman)) {
                String gameinfojs = fileman.read();
                engine = new ScriptEngineManager().getEngineByName("nashorn");

                try {
                    engine.eval(gameinfojs);
                    ScriptObjectMirror event_players = (ScriptObjectMirror) engine.get("d_pi");
                    ScriptObjectMirror event_name = (ScriptObjectMirror) engine.get("d_pn");
                    ScriptObjectMirror event_type = (ScriptObjectMirror) engine.get("d_lx");
                    ScriptObjectMirror event_min = (ScriptObjectMirror) engine.get("d_tm");
                    ScriptObjectMirror event_score = (ScriptObjectMirror) engine.get("d_bf");
                    ScriptObjectMirror event_side = (ScriptObjectMirror) engine.get("d_sx");

                    if (event_min != null) {
                        ArrayList<Event7mModel> e7mlist = new ArrayList<>();
                        for (int arrsize = 0; arrsize < event_min.size(); arrsize++) {
                            Integer pid = (Integer) event_players.getSlot(arrsize);
                            Integer type = (Integer) event_type.getSlot(arrsize);
                            String name = (String) event_name.getSlot(arrsize);
                            String min = (String) event_min.getSlot(arrsize);
                            String score = (String) event_score.getSlot(arrsize);
                            Integer side = (Integer) event_side.getSlot(arrsize);

                            Event7mModel e7mm = new Event7mModel();
                            if (arrsize == 0) {
                                e7mm.clear(mm.getO7m_mid());
                            }
                            e7mm.setMid(mm.getO7m_mid());
                            e7mm.setPid(pid);
                            e7mm.setType(type);
                            e7mm.setMinute(min);
                            e7mm.setScore(score);
                            e7mm.setSide(side);
//                            e7mm.save();
                            e7mlist.add(e7mm);
//                            System.out.println(min + "->" + name);
                        }

                        Event7mModel e7m = new Event7mModel();
                        e7m.saveAll(e7mlist);

                    }

                    ScriptObjectMirror players_in = (ScriptObjectMirror) engine.get("d_upi");
                    ScriptObjectMirror players_out = (ScriptObjectMirror) engine.get("d_dpi");
                    ScriptObjectMirror substitutes_min = (ScriptObjectMirror) engine.get("d_stm");
                    ScriptObjectMirror substitutes_side = (ScriptObjectMirror) engine.get("d_ssx");

                    if (substitutes_min != null) {
                        ArrayList<SubstitutesModel> submlist = new ArrayList<>();
                        for (int arrsize = 0; arrsize < substitutes_min.size(); arrsize++) {
                            String min = (String) substitutes_min.getSlot(arrsize);
                            Integer pid_in = (Integer) players_in.getSlot(arrsize);
                            Integer pid_out = (Integer) players_out.getSlot(arrsize);
                            Integer side = (Integer) substitutes_side.getSlot(arrsize);

                            SubstitutesModel subm = new SubstitutesModel();
                            if (arrsize == 0) {
                                subm.clear(mm.getO7m_mid());
                            }
                            subm.setMid(mm.getO7m_mid());
                            subm.setPid_in(pid_in);
                            subm.setPid_out(pid_out);
                            subm.setMinute(min);
                            subm.setSide(side);
//                            subm.save();
                            submlist.add(subm);

//                            System.out.println(pid_in + "->" + pid_out);
                        }
                        SubstitutesModel submm = new SubstitutesModel();
                        submm.saveAll(submlist);
                    }

                    ScriptObjectMirror stats = (ScriptObjectMirror) engine.get("d_live_stat");
                    if (stats != null) {
                        ArrayList<LiveStatsModel> lsmlist = new ArrayList<>();
                        for (String key : stats.getOwnKeys(false)) {
                            String val = (String) stats.get(key);
//                            System.out.println(key + ":" + val);

                            LiveStatsModel lsm = new LiveStatsModel();
                            lsm.setMid(mm.getO7m_mid());
                            lsm.setType(Integer.parseInt(key));
                            lsm.setValue(val);
//                            lsm.save();
                            lsmlist.add(lsm);
                        }
                        LiveStatsModel lsm = new LiveStatsModel();
                        lsm.saveAll(lsmlist);
                    }

                } catch (ScriptException ex) {
                    ex.printStackTrace();
                }

            }

        }

    }

    public void gatheringTeamFixture() {
        FileManager fileman = new FileManager();
        String maindir = "statistic";
        String gameinfopath = "en/gameinfo_en.js";
        String lnenpath = "en/gameteamfixture_en.js";

        MatchesMapModel mmm = new MatchesMapModel();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");

        HashMap<String, LineupModel> lineupmap = new HashMap<>();

        List<MatchesMapModel> mmlist = mmm.getTodayWaitMatch();

        for (MatchesMapModel mm : mmlist) {
            fileman.setPath(maindir + "/" + mm.getO7m_mid());
            fileman.setFilename(gameinfopath);
            if (FileManager.isFile(fileman)) {
                String gameinfojs = fileman.read();
                engine = new ScriptEngineManager().getEngineByName("nashorn");

                try {
                    engine.eval(gameinfojs);

                    ScriptObjectMirror gameinfo = (ScriptObjectMirror) engine.get("gameInfo");
                    String taname = (String) gameinfo.get("taname");
                    String tbname = (String) gameinfo.get("tbname");
                    System.out.println(mm.getO7m_mid() + ":" + taname + " - " + tbname);

                    fileman.setFilename(lnenpath);
                    if (FileManager.isFile(fileman)) {
                        engine.eval(fileman.read());
                        ScriptObjectMirror teamfixture = (ScriptObjectMirror) engine.get("gameTeamFixture");
                        ScriptObjectMirror teamhomedata = (ScriptObjectMirror) teamfixture.get("A");
                        ScriptObjectMirror lidlist = (ScriptObjectMirror) teamhomedata.get("mid");
                        ScriptObjectMirror hidlist = (ScriptObjectMirror) teamhomedata.get("aid");
                        ScriptObjectMirror gidlist = (ScriptObjectMirror) teamhomedata.get("bid");
                        ScriptObjectMirror datelist = (ScriptObjectMirror) teamhomedata.get("time");

                        if (datelist != null) {
                            ArrayList<TeamFixtureModel> tflist = new ArrayList<>();
                            for (int arrsize = 0; arrsize < datelist.size(); arrsize++) {
                                Integer lid = (Integer) lidlist.getSlot(arrsize);
                                Integer hid = (Integer) hidlist.getSlot(arrsize);
                                Integer gid = (Integer) gidlist.getSlot(arrsize);
                                String date = (String) datelist.getSlot(arrsize);

                                TeamFixtureModel tfm = new TeamFixtureModel();
                                tfm.setMid(mm.getO7m_mid());
                                tfm.setLid(lid);
                                tfm.setHid(hid);
                                tfm.setGid(gid);
                                tfm.setDatetime(Long.valueOf(date));
                                tfm.setSide("home");
//                                tfm.save();
                                tflist.add(tfm);
                            }
                            TeamFixtureModel tfm = new TeamFixtureModel();
                            tfm.saveAll(tflist);
                        }

                        teamhomedata = (ScriptObjectMirror) teamfixture.get("B");
                        lidlist = (ScriptObjectMirror) teamhomedata.get("mid");
                        hidlist = (ScriptObjectMirror) teamhomedata.get("aid");
                        gidlist = (ScriptObjectMirror) teamhomedata.get("bid");
                        datelist = (ScriptObjectMirror) teamhomedata.get("time");

                        if (datelist != null) {
                            ArrayList<TeamFixtureModel> tflist = new ArrayList<>();
                            for (int arrsize = 0; arrsize < datelist.size(); arrsize++) {
                                Integer lid = (Integer) lidlist.getSlot(arrsize);
                                Integer hid = (Integer) hidlist.getSlot(arrsize);
                                Integer gid = (Integer) gidlist.getSlot(arrsize);
                                String date = (String) datelist.getSlot(arrsize);

                                TeamFixtureModel tfm = new TeamFixtureModel();
                                tfm.setMid(mm.getO7m_mid());
                                tfm.setLid(lid);
                                tfm.setHid(hid);
                                tfm.setGid(gid);
                                tfm.setDatetime(Long.valueOf(date));
                                tfm.setSide("away");
//                                tfm.save();
                                tflist.add(tfm);
                            }
                            TeamFixtureModel tfm = new TeamFixtureModel();
                            tfm.saveAll(tflist);

                        }

                    }

//                    Set<Map.Entry<String, LineupModel>> lineset = lineupmap.entrySet();
//                    Iterator<Map.Entry<String, LineupModel>> lineit = lineset.iterator();
//                    while (lineit.hasNext()) {
//                        Map.Entry<String, LineupModel> lnmp = lineit.next();
//                        lnmp.getValue().save();
//                    }
                } catch (ScriptException ex) {
                    ex.printStackTrace();
                }

            }

        }

    }

}
