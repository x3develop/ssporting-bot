/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author superx
 */
public class FileManager {

    private String path = "files";
    private String content = "";
    private String filename = "emptyfile.txt";
    private String fullpath = "";

    public void init() {
        File destination = new File(this.getPath());
        if (!destination.isDirectory()) {
            System.out.println("not dir");
            if (destination.mkdirs()) {
                System.out.println("But created");
            } else {
                System.out.println("And not created");
            }

        }
    }

    public static boolean move(FileManager source, FileManager target) {
        boolean success = false;
        target.setContent(source.read());
        target.write();
        if (isFile(target)) {
            source.delete();
            success = true;
        }
        return success;
    }

    public static boolean isFile(FileManager source) {
        boolean success = false;
        File tmp = new File(source.getFullpath());
        if (tmp.isFile()) {
            success = true;
        }
        return success;
    }

    public void setPath(String dir) {
        this.path = dir;
        File destination = new File(this.getPath());

        if (!destination.isDirectory()) {
            //System.out.println("not dir");
            //log.write("info", this.getPath() + " is not directory");
            if (destination.mkdirs()) {
                //System.out.println("But created");
                //log.write("info", "But created");
            } else {
                //System.out.println("And not created");
                //log.write("info", "But can't create.");
            }
        } else {
            //System.out.println("found Directory " + dir);
            //log.write("info", "found Directory " + dir);
        }
        this.fullpath = this.getPath() + "/" + this.getFilename();

    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setFilename(String filename) {
        this.filename = filename;
        this.fullpath = this.getPath() + "/" + this.getFilename();
    }

    public void write() {
        String file = this.getPath() + "/" + this.getFilename();
        this.setFullpath(file);
        File destination = new File(file);
        System.out.println(destination.getAbsolutePath());
        try {
            Files.write(this.getContent(), destination, Charset.forName("UTF-8"));
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public void append(String newline) {
        String file = this.getPath() + "/" + this.getFilename();
        this.setFullpath(file);
        File destination = new File(file);
        System.out.println(destination.getAbsolutePath());
        try {
            Files.append(newline, destination, Charsets.UTF_8);
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }

    public String read() {
        String file = this.getPath() + "/" + this.getFilename();
        File destination = new File(file);
        //System.out.println(destination.getAbsolutePath());

        if (destination.isFile()) {
            try {
                this.content = Files.toString(destination, Charsets.UTF_8);
            } catch (IOException ex) {
                Logger.getLogger(FileManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this.getContent();
    }

    public boolean delete() {
        boolean success = false;
        String file = this.getPath() + "/" + this.getFilename();
        File destination = new File(file);
        success = destination.delete();
        if (success) {
            //System.out.println("Deleted :" + file);

        }
        return success;
    }

    public static String getOldestFile(String dir) {

        File folder = new File(dir);
        File[] listOfFiles = folder.listFiles();
        int olderFileIndex = 0;
        long olderFileTime = 0;
        boolean bChk = false;

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()) {
                bChk = true;
                if (0 == i) {
                    olderFileTime = listOfFiles[i].lastModified();
                } else if (olderFileTime > listOfFiles[i].lastModified()) {
                    olderFileTime = listOfFiles[i].lastModified();
                    olderFileIndex = i;
                }
            }
        }

        if (true == bChk) {
            //System.out.print(" File " + listOfFiles[olderFileIndex].getName());
            //System.out.println(" Time " + listOfFiles[olderFileIndex].lastModified());
            return listOfFiles[olderFileIndex].getName();
        } else {
            //System.out.println("Has not found the file");
            return "";
        }
    }

    void setContent(int sumWait) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the fullpath
     */
    public String getFullpath() {
        return fullpath;
    }

    /**
     * @param fullpath the fullpath to set
     */
    public void setFullpath(String fullpath) {
        this.fullpath = fullpath;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

}
