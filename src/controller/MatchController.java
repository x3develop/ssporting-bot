/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.LiveMatchModel;
import Model.MatchesMapModel;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mrsyrop
 */
public class MatchController {
    
    public void clearDuplicatedMatch() {
        MatchesMapModel mmm = new MatchesMapModel();
        List<MatchesMapModel> mmlist = mmm.getTodayMatch();
        List<LiveMatchModel> lmlist = new ArrayList<>();
        for (MatchesMapModel mm : mmlist) {
            LiveMatchModel matchm = new LiveMatchModel();
            matchm.setMid(mm.getF24_mid());
            matchm.setHn(mm.getHn());
            matchm.setGn(mm.getGn());
            matchm.setShowDate(mm.getShowdate());
            lmlist.add(matchm);
//            System.out.println(matchm.getMid() + ":" + matchm.getHn() + "-" + matchm.getGn());
        }
        LiveMatchModel lm = new LiveMatchModel();
        lm.clearAll(lmlist);
        
    }
}
