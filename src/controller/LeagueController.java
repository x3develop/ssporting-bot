/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.LangleagueModel;
import Model.LeagueModel;
import java.util.List;

/**
 *
 * @author mrsyrop
 */
public class LeagueController {

    public void markOldLeague() {
        LangleagueModel llm = new LangleagueModel();
        LeagueModel lm = new LeagueModel();
        List<LangleagueModel> lllist = llm.getAll();
        for (LangleagueModel ll : lllist) {
            System.out.println(ll.getLid() + ":" + ll.getName());
            if (lm.hasLeague(ll.getLid())) {
                System.out.println("Found");
                ll.usedThisLeague();
            }
        }
    }
}
