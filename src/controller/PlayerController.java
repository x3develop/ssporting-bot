/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import Model.LeagueModel;
import Model.PlayerModel;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import ssporting.bot.SsportingBot;

/**
 *
 * @author mrsyrop
 */
public class PlayerController {

    public void updateTopPlayer() {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("nashorn");
        FileManager filemanen = new FileManager();
        FileManager filemanth = new FileManager();

        LeagueModel leaguem = new LeagueModel();
        PlayerModel playermodel = new PlayerModel();
        List<int[]> leaguelist = leaguem.getLive7m();
        String[] lang = {"en", "th"};
        filemanen.setPath("shooter");
        filemanth.setPath("shooter");

        try {

            for (int[] league : leaguelist) {
                System.out.println(league[0] + ":" + league[1]);
                filemanen.setFilename(league[0] + "_en.js");
                filemanth.setFilename(league[0] + "_th.js");
                if (FileManager.isFile(filemanen) && FileManager.isFile(filemanth)) {
                    boolean del = playermodel.removeOldTopplayer(league[1]);
//                    System.out.println(del);

                    System.out.println("have both file");
                    engine.eval(filemanen.read());
                    ScriptObjectMirror playerlisten = (ScriptObjectMirror) engine.get("Shooters");
                    engine.eval(filemanth.read());
                    ScriptObjectMirror playerlistth = (ScriptObjectMirror) engine.get("Shooters");

                    for (int i = 0; i < playerlisten.size(); i++) {

                        ScriptObjectMirror player = (ScriptObjectMirror) playerlisten.getSlot(i);
                        ScriptObjectMirror teamdata = (ScriptObjectMirror) player.getSlot(1);
                        ScriptObjectMirror playerdata = (ScriptObjectMirror) player.getSlot(2);

                        ScriptObjectMirror playerth = (ScriptObjectMirror) playerlistth.getSlot(i);
                        ScriptObjectMirror teamdatath = (ScriptObjectMirror) playerth.getSlot(1);
                        ScriptObjectMirror playerdatath = (ScriptObjectMirror) playerth.getSlot(2);

                        Integer playerid = (Integer) playerdata.getSlot(0);
                        String playername = (String) playerdata.getSlot(1);
                        String playernameth = (String) playerdatath.getSlot(1);

                        Integer teamid = (Integer) teamdata.getSlot(0);
                        String teamname = (String) teamdata.getSlot(1);
                        String teamnameth = (String) teamdatath.getSlot(1);

//                        System.out.println(player.getSlot(0));

                        PlayerModel playerm = new PlayerModel();
                        playerm.setId7m(league[0]);
                        playerm.setLeagueid(league[1]);
                        playerm.setPlayerid(playerid);

                        playerm.setNo((int) player.getSlot(0));
                        playerm.setTid(teamid);

                        playerm.setTeamname_en(teamname);
                        playerm.setPlayername_en(playername);

                        playerm.setTeamname_th(teamnameth);
                        playerm.setPlayername_th(playernameth);                  
      
                        String totalscore = (String) player.getSlot(3);
                        int penscore = (int) player.getSlot(4);
                        playerm.setTotalscore(totalscore);
                        playerm.setPenscoe(penscore+"");

                        if (playerm.save()) {
//                            System.out.println(playerm.getPlayername_th());
//                            fileman.delete();
                        }

                    }

                }

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
