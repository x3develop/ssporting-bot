/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssporting.bot;

import controller.FileDataController;
import controller.LeagueController;
import controller.MatchController;
import controller.PlayerController;
import controller.UrlController;
import controller.HttpReqController;
import controller.Ballcontroller;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author mrsyrop
 */
public class SsportingBot {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        PlayerController playercontroll = new PlayerController();
        UrlController urlc = new UrlController();
        FileDataController filedata = new FileDataController();
        MatchController mcon = new MatchController();
        LeagueController lcon = new LeagueController();
        Ballcontroller ball=new Ballcontroller();

        String mode = "getUEFAChampionsLeague";
//        System.out.println(args.length);
        if (args.length > 0) {
            mode = args[0];
        }
        
//        mode="getMatchDetail";
        System.out.println(mode);
        switch (mode) {
            case "getTvMatch":
                ball.getTvMatch();
                break;
            case "getMatchGamble":
                ball.getMatchGamble();
                break;
            case "getHistoryLeague":
                ball.getHistoryLeague();
                break;
            case "getUEFAChampionsLeague":
                ball.getUEFAChampionsLeague();
                break;
            case "getMatchByLeague":
                ball.getMatchByLeague();
                break;
            case "getLeagueAll":
                ball.getLeagueAll();
                break;
            case "getThaiLeague":
                ball.getThaiLeague();
                break;
            case "getMatchDetail":
                ball.getMatchDetail();
                break;
            case "getByLeague":
                ball.getByLeague();
                break;
            case "getLeague":
                ball.getLeague();
                break;
            case "bellGetPrediction":
                ball.getPrediction();
                break;
            case "getPredict":
                urlc.getPredict();
                break;
            case "getlivematch":
                urlc.getLiveMatch();
                break;
            case "liveMatchEventInfinity":
                urlc.getLiveMatch();
                filedata.runLiveMatchEventInfinity();
                break;
            case "getlivematchupdate":
                urlc.getLiveMatchEvent();
                break;
            case "processlivematchupdate":
                filedata.gatheringLiveUpdate();
                break;
            case "maplive7m":
                urlc.map7mLiveMatch();
                break;
            case "get7mstatistic":
                urlc.get7mStatistic();
                break;
            case "get7mliveevent":
                urlc.get7mLiveEvent();
                break;
            case "gathering7mliveevent":
                urlc.get7mLiveEvent();
                filedata.gatheringGameEvent();
                break;
            case "get7mteamdata":
                urlc.get7mTeamData();
                break;
            case "gatheringlineup":
                filedata.gatheringLineUp();
                break;
            case "gatheringteamfixture":
                filedata.gatheringTeamFixture();
                break;
            case "gatheringgamehistory":
                filedata.gatheringGamehistory();
                break;
            case "gatheringgameteamhistory":
                filedata.gatheringGameTeamhistory();
                break;
            case "gatheringgameinfo":
                filedata.gatheringGameInfo();
                break;
            case "getleaguepts":
                urlc.getLeaguePts();
                break;
            case "gatheringleaguepts":
                filedata.addLeaguePts();
                break;
            case "getodds":
                urlc.get7mOdds();
                break;
            case "cleardupmatch":
                mcon.clearDuplicatedMatch();
                break;
            case "markoldleague":
                lcon.markOldLeague();
                break;
            default:
                break;
        }
    }
}
