/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ssporting.bot.controller;

import java.sql.*;
import java.lang.System;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author Tingly
 */
public class DbConfig {

    protected Connection conn;
    protected Statement stmt;   
    protected ResultSet rs;

    String url = "jdbc:mysql://localhost/play?characterEncoding=UTF8";
    String driver = "org.gjt.mm.mysql.Driver";
    String username = "root";
    String passwd = "1234";
    String query = "";
    
//    String url = "jdbc:mysql://eeball.com/eeball.com?characterEncoding=UTF8";
//    String driver = "org.gjt.mm.mysql.Driver";
//    String username = "dev";
//    String passwd = "@x1234";
//    String query = "";

    public void connectDB() {
        try {
            Class.forName(driver);
            conn = DriverManager.getConnection(url, username, passwd);
            stmt = conn.createStatement();
            stmt.execute("SET profiling = 1;");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void closeDB() {
        try {
            if (null != rs) {
                rs.close();
            }
            if (null != stmt) {
                stmt.close();
            }
            if (null != conn) {
                conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
